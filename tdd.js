const MqttInterface = require('./lib/platform/core/interface').MqttInterface
const SimpleLogger = require('./lib/platform/core/logger').SimpleLogger
var conf = {
    username: 'ping',
    password: '123456',
    host: '127.0.0.1',
    port: 1883,
    loggers: [
        {
            script: SimpleLogger.path,
            name: 'ping'
        }
    ]
}
var iface = new MqttInterface(conf)
var letter = {
    message: 'Hello world'
}

iface.connect( (err) => {
    console.log('Connection with err = ', err)
    var handler = function(message){
        console.log('message to come', message);
        iface.dispose()
    }
    var done = () => {
        iface.sendSync('action/bonjour', letter)
        console.log('Hi there');
    }
    iface.listen('action/bonjour', handler, done)
})

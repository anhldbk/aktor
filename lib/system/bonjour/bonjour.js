'use strict'
const dgram = require('dgram')
const _ = require('lodash')
const Loggable = require('../../platform/core/logger').Loggable
const util = require('../../util')
const resources = require('../share/resources')
const numeral = require('../share/numeral')
const moment = require('moment')
const os = require('os')
const path = require('path')

const low = require('lowdb')
const fileSync = require('lowdb/lib/file-sync')
const registryPath = path.resolve( __dirname, '../registry.json')
const registry = low( registryPath, {
    storage: {
        read: fileSync.read // read only
    }
})

// BonjourService is a UDP server for serving service discovery.
// This service is not an aktor at all.
class BonjourService extends Loggable {
    /**
     * constructor
     */
    constructor() {
        var conf = {
            loggers: [
                Loggable.getDefaultLogger('service/bonjour')
            ]
        }
        super(conf)
        util.bindThis(this, BonjourService)

        var port = _.get(registry, 'configuration.bonjour.port', 8886)
        this._server = dgram.createSocket('udp4')
        this._port = port
    }

    /**
     * Get Bonjour messages
     * @param  {Function} callback Function `(err, result)` to process results.
     */
    getBonjourMessage(callback) {
        var self = this

        // read registry information
        registry.read()
        var message = _.cloneDeep(registry.getState())
        _.unset(message, 'configuration')
        _.set(message, 'platform.systemId', `system/${util.getMachineId()}`)

        // stuff with dynamic one
        var upTime = os.uptime()
        upTime = numeral(upTime).format('00:00:00')
        _.set(message, 'upTime', upTime)

        var currentTime = moment().format('HH:mm:ss DD/MM/Y')
        _.set(message, 'currentTime', currentTime)

        var getStateDone = function(err, state) {
            if (err) {
                self.logError(`Can NOT get information about the current state. Reason: ${err}`)
                callback && callback('status.failure.query_state')
            }

            _.set(message, 'state', state)
            callback && callback(null, message)
        }
        self.getState(getStateDone)
    }

    /**
     * Get information about system state
     * @param  {Function} callback Function `(err, result)` to process results.
     * NOTE: currently only information about system resources is available
     */
    getState(callback) {
        resources.getResourcesState(callback)
    }

    /**
     * Check if a message is a Bonjour message
     * @param  {Any}  message A message received by servers
     * @return {Boolean}  Returns true if the message is a Bonjour one. Otherwise, returns false.
     */
    isBonjourMessage(message) {
        try {
            message = JSON.parse(message.toString().toLowerCase())
            if (_.get(message, 'tell') === 'bonjour') {
                return true
            }
        } catch (e) {
            // do nothing
        }
        return false
    }

    /**
     * Handler to process incoming messages
     * @param  {Any} message Received messages
     * @param  {Object} client  Structure containing information about senders
     */
    onMessage(message, client) {
        if (!this.isBonjourMessage(message)) {
            return
        }
        var self = this
        var done = function(err, message) {
            if (err) {
                return // do nothing, already logged
            }
            message = JSON.stringify(message)
            self._server.send(message, 0, message.length, client.port, client.address)
        }
        self.getBonjourMessage(done)
    }

    /**
     * Start the service
     * @param  {Function} callback Function `(err, result)` to process results.
     */
    start(callback) {
        var self = this
        self.logInfo('Bonjour service is starting...')
        if (util.isPortOpenedSync(this._port)) {
            self.logError(`Another service is serving at port ${this._port}`)
            return callback('status.failure.invalid_port')
        }

        self._server.bind(this._port)
        var done = function() {
            var host = self._server.address();
            self.logInfo(`Bonjour service is started at ${host.address}:${host.port}...`)
            callback && callback()
        }
        self._server.on('listening', done);
        self._server.on('message', self.onMessage.bind(self))
    }

    /**
     * Stop the service
     * @param  {Function} callback Function `(err, result)` to process results.
     */
    stop(callback) {
        this.logWarn('Bonjour service is stopping...')
        this._server.close()
        this.dispose()
        callback && callback()
    }
}

module.exports = BonjourService

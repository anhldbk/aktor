'use strict'

var exec = require('child_process').exec

function issue(cmd, callback) {
    var options = {
        cwd: __dirname
    }
    exec(cmd, options, function(err, stdout, stderr) {
        callback && callback(err, stdout)
    })
}

function isUpdatable( callback ){
    issue('git remote update', function(err, result) {
        if (err) {
            return callback && callback('status.failure.git_remote_update')
        }
        issue('git status -uno', function(err, result) {
            if(err){
                return callback && callback('status.failure.git_status')
            }
            // -> Output for up-to-date branches:
            // Your branch is up-to-date with 'origin/master'.
            // nothing to commit (use -u to show untracked files)
            // -> Output for updable branches:
            // Your branch is behind 'origin/master' by 1 commit, and can be fast-forwarded.
            //   (use "git pull" to update your local branch)
            // nothing to commit (use -u to show untracked files)
            var lines = result.split('\n')
            result = ( lines[1].indexOf('commit') != -1 )
            callback && callback(null, result)
        })
    })
}

function update( callback ){
    issue('git merge', function(err, result){
        console.log(result)
        callback && callback(err)
    })
}

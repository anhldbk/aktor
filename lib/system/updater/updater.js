'use strict'
const pm2 = require('pm2')
const async = require('async')
const _ = require('lodash')
const util = require('../../util')
const RoundDistributor = require('./distributors').RoundDistributor
const CommonAktor = require('../core/aktor').CommonAktor
const MqttInterface = require('../core/interface').MqttInterface
const ParseStorage = require('../core/storage').ParseStorage
const Configurator = require('./configurator')
const configurator = new Configurator()
const path = require('path')
const PlatformConfiguration = require('lowdb')(
    path.resolve(__dirname, '../conf.json')
)

// A service for updating platform & its services
// It runs in a standalone services
class UpdaterService extends CommonAktor {
    // Constructor
    // No configuration is needed. Environ is a self-configuring aktor
    constructor() {
        var id = 'service/updater',
            result = configurator.getAktorConf(id),
            err = result[0],
            conf = result[1],
            path = 'interfaces.local.listen'

        if (! _.isNil(err) ){
            console.log('Fatal: Can NOT initialize UpdaterService. Reason: ' + err)
            console.log('Terminating now...')
            process.exit(-1)
        }

        // register handlers
        _.set(conf, path, {
            ':request/update': 'onUpdate'
        })

        super(conf)
        //TODO: Register an interval to check for update (can work without platform services)
        
    }

}

const exec = require('child_process').exec,
    _ = require('lodash')

/**
 * Enumerate all mounted devices
 * @param  {Function} callback Function `(err, result)` to process results
 */
function enumerate(callback){
    var done = function(err, result){
        if(err){
            return callback && callback(err)
        }
        var lines = result.split('\n')

        // drop the header and the blank line at the end
        _.pullAt(lines, lines.length-1, 0)

        var parse = function(input){
            var res = parseInt(input) * 1024
            if (isNaN(res)) {
                res = input // unparsable
            }
            return res
        }

        var mapOp = function(line){
            var parts = line.split(' ')
            _.pull( parts, ' ', '' )
            var res =  {
                filesystem: parts[0],
                used: parse( parts[2] ),
                available: parse( parts[3]),
                mount: parts[ parts.length -1 ] // mount points are always located at the end
            }
            res.total = res.used + res.available
            res.use = res.used / res.total
            return res
        }

        result =  _.map(lines, mapOp)
        callback && callback(null, result)
    }
    exec('df', done)
}

module.exports = {
    enumerate
}

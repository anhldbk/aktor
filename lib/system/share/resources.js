'use strict'
const disks = require('./disks')
const _ = require('lodash')
const numeral = require('./numeral')
const os = require('os')
const async = require('async')

/**
 * Convert fields of states into string representations
 * @param  {Object} state An object with fields of `total`, `used`, `available`, `use`
 * @return {Object} A new object with string fields
 * NOTE: This function is used privately by this module
 */
function represent(state){
    return {
        total: numeral(state.total).format('0.00b'),
        used: numeral(state.used).format('0.00b'),
        available: numeral(state.available).format('0.00b'),
        use: numeral(state.use).format('0.00%')
    }
}

/**
 * Get information about storage states
 * @param  {Function} callback Function `(err, result)` to process results.
        If every things are OK, `err` = null, `result` is an object of { total, used, use, available}
 */
function getStorageState(callback) {
    var enumerateDone = function(err, drives) {
        if(err){
            return callback && callback('status.failure.storage_enumerate')
        }

        // we just care about storage with filesystem of '/dev/*'
        var filterOp = (drive) => drive.filesystem.startsWith('/dev/')
        drives = _.filter(drives, filterOp)

        var used = _.sumBy( drives, 'used')
        var available = _.sumBy( drives, 'available')
        var total = _.sumBy( drives, 'total')
        var use = used / total
        var result = {
            total,
            used,
            use,
            available
        }
        result = represent(result)

        callback && callback(null, result)

    }
    disks.enumerate(enumerateDone)
}

/**
 * Get information about memory states
 * @param  {Function} callback Function `(err, result)` to process results.
        If every things are OK, `err` = null, `result` is an object of { total, used, use, available}
 */
function getMemoryState(callback) {
    var total = os.totalmem()
    var available = os.freemem()
    var used = total - available
    var use = used / total
    var result = {
        total,
        used,
        use,
        available
    }
    result = represent(result)
    callback && callback(null, result)
}

/**
 * Get state about resources (storage & memory)
 * @param  {Function} callback Function `(err, result)` to process results.
        If every things are OK, `err` = null, `result` is an object of { total, used, use, available}
 */
function getResourcesState(callback){
    var done = function(err, result){
        if(err){
            return callback && callback(err)
        }
        var memory = result[0]
        var storage = result[1]
        result = { memory, storage }
        callback && callback(null, result)
    }
    async.applyEach(
        [
            getMemoryState,
            getStorageState
        ],
        done
    )
}

module.exports = {
    getStorageState,
    getMemoryState,
    getResourcesState
}

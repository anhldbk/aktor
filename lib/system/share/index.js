var state = {
    upTime, // system up time
    currentTime, // current time of system, updated every minutes
    storage: {
        total, // in Gb
        used,
        free
    },
    memory: { // updated every 5min
        total, // in Gb
        used,
        free
    },
    power: {
        blackout, // true or false
        timeout: 60000
    },
    gsm: {
        state: "state.{connected, disconnected}",
        network: "viettel",
        signal: "signal.{poor, fair, good, excellent}",
        phoneNumber: "0987xyz",
        balance: "1000 VND"
    },
    wifi: {
        state: "state.{connected,disconnected,broadcasting}",
        network: "xyz.com",
        ip
    }
}

var a = {
    type: 'object',

    properties: {
        version: {
            type: 'string',
            description: 'Version of this descriptor',
            enum: ['0.1.0']
        },
        meta: { // descriptors about the entity itself
            type: 'object',
            description: 'Meta data about entities',
            properties: {
                id: {
                    constant: 'service/device-hub'
                },

                class: {
                    constant: 'class.service'
                }
            },
            required: [ 'class', 'id']
        },

        event: {
            type: 'object',
            description: 'A mapping between event names and their schemas ',
            eventDeviceChanged: {
                type: 'object',
                properties: {
                    type: {
                        type: 'string',
                        enum: ['event.device.online', 'event.device.offline', 'event.device.error', 'event.device.added', 'event.device.removed']
                    },
                    data: {
                        type: 'object',
                        properties: {
                            id : {
                                type: 'string',
                                description: 'A unique id for the device'
                            },
                            class: {
                                type: 'string',
                                description: 'Device class',
                                enum: ['class.device.sensor.motion', 'class.device.sensor.humidity', 'class.device.sensor.door', 'class.device.sensor.fire', 'class.device.keyfob.panic', 'class.device.keyfob.remote']
                            },
                            protocol: {
                                type: 'string',
                                description: 'Device procotol, for example: Zigbee, Z-Wave',
                                enum: ['zigbee']
                            },
                            link: {
                                type: 'string',
                                description: 'Link to the product page (if any)'
                            }
                        },
                        required: ['id', 'class', 'protocol']
                    }
                }
            },
            eventDeviceEvent: {
                type: 'object',
                properties: {
                    type: {
                        type: 'string',
                        enum: [
                            'event.device.sensor.motion',
                            'event.device.sensor.motion_detected',
                            'event.device.sensor.motion_undetected',
                            'event.device.sensor.door',
                            'event.device.sensor.door_opened',
                            'event.device.sensor.door_closed',
                            'event.device.sensor.temperature',
                            'event.device.sensor.humidity',
                            'event.device.sensor.fire',
                            'event.device.sensor.fire_detected',
                            'event.device.sensor.fire_undetected',
                            'event.device.sensor.panic',
                            'event.device.sensor.remote',
                            'event.device.sensor.remote_arm',
                            'event.device.sensor.remote_disarm',
                            'event.device.sensor.remote_indoor',
                            'event.device.sensor.remote_suppress'
                        ]
                    },
                    data: {
                        type: 'object',
                        properties: {
                            battery: {
                                type: 'number',
                                description: 'Battery level. 1 means ok, 0 means batter low',
                                enum: [0, 1]
                            }
                        }
                    },
                    origin: {
                        type: 'object',
                        properties: {
                            id: {
                                type: 'string',
                                description: 'A unique id for the device'
                            },
                            location: {
                                type: 'string'
                            },
                            name: {
                                type: 'string'
                            },
                            class: {
                                type: 'string',
                                description: 'Class of the entity. For example: class.device.motion...'
                            },
                            link: {
                                type: 'string',
                                description: 'URL of the entity on our product pages'
                            }
                        },
                        required: [ 'class', 'id' ]
                    }
                },
                required: [ 'origin', 'type' ],
                switch: [
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.motion'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        motion: {
                                            type: 'number',
                                            enum: [0, 1]
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.door'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        open: {
                                            type: 'number',
                                            enum: [0, 1]
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.temperature'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        temperature: {
                                            type: 'number'
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.humidity'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        humidity: {
                                            type: 'number'
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.fire'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        fire: {
                                            type: 'number'
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.panic'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        panic: {
                                            type: 'number'
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        if: {
                            properties: {
                                type: {
                                    constant: 'event.device.sensor.remote'
                                }
                            }
                        },
                        then: {
                            properties: {
                                data: {
                                    properties: {
                                        remote: {
                                            type: 'string',
                                            enum: ['arm', 'disarm', 'indoor', 'suppress']
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        then: {

                        }
                    }

                ]
            }
        }
    },



    required: ['meta']}

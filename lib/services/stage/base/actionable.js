'use strict'

const Loggable = require('../../../platform/core/logger').Loggable
const _ = require('lodash')
const JSON5 = require('json5')

/**
 * Base class for actionable entities
 */
class Actionable extends Loggable{
    constructor(conf){
        super(conf)
    }

    /**
     * Parse an input into a JSON object
     * @param  {String} input An input string
     * @return {Object} If the input contains a valid json string, we'll parse and return the parsed one. Otherwise, returns null
     */
    parseJson(input){
        try{
            return JSON5.parse(input)
        }
        catch(e){
            return null
        }
    }

    /**
     * Emit action events
     * @param  {Any} actions An action or an array of actions
     */
    emitAction(actions){
        if( !_.isArray(actions) ){
            actions = [actions]
        }
        for(var i =0 ; i < actions.length; i++){
            this.emit('action', actions[i])
        }
    }

    /**
     * Register handlers for processing actions emitted
     * @param  {Function} handler The function
     */
    onAction(handler){
        if( !_.isFunction(handler) ){
            throw new Error('status.failure.invalid_handler')
        }
        this.on('action', handler)
    }
}

module.exports = Actionable

'use strict'

class ExecutorBase {

    /**
     * Constructor
     */
    constructor(){

    }

    /**
     * Start a action
     * @param  {Object}   action   An object describing the action to start
     * @param  {Function} callback  A callback function to process results
     */
    start(action, callback){
        callback && callback()
    }
}

module.exports = ExecutorBase

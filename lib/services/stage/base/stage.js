'use strict'
const CommonAktor = require('../../../platform/core/aktor').CommonAktor

class StageBase extends CommonAktor{

    constructor(conf){
        super(conf)
    }

    addScene(scene, callback){
        callback && callback()
    }

    removeScene(scene, callback){
        callback && callback()
    }

    updateScene(scene, callback){
        callback && callback()
    }

    startScene(scene, callback){
        callback && callback()
    }

    stopScene(sceneName, callback){
        callback && callback()
    }

    addEvent(event, callback){
        callback && callback()
    }

    removeEvent(event, callback){
        callback && callback()
    }

    startAction(action, callback){

    }
}

module.exports = StageBase

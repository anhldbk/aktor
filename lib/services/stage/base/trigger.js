'use strict'

const Actionable = require('./actionable')

class TriggerBase extends Actionable{
    constructor(conf){
        super(conf)
    }

    /**
     * Start the trigger
     * @param {Function} callback Callback function
     */
    start(callback){
        callback && callback()
    }

    /**
     * Stop the trigger by destroying any allocated resources
     * @param {Function} callback Callback function
     */
    stop(callback){
        super.dispose(callback)
    }

}

module.exports = TriggerBase

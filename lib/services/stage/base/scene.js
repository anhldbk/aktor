'use strict'

const Actionable = require('./actionable')

class SceneBase extends Actionable{

    /**
     * Constructor
     */
    constructor(conf){
        super(conf)
    }

    /**
     * Start this scene
     * @param  {Function} callback A callback function `(err, result)`
     */
    start(callback){
        callback && callback()
    }

    /**
     * Stop this scene
     * @param  {Function} callback A callback function `(err, result)`
     */
    stop(callback){
        callback && callback()
    }

    /**
     * Configure this scene with a new configuration
     * @param  {Object}   conf     The configuration
     * @param  {Function} callback A callback function `(err, result)`
     */
    configure(conf, callback){
        callback && callback()
    }

    /**
     * Dispose the scene
     * @param  {Function} callback A callback function `(err, result)`
     */
    dispose(callback){
        super.dispose(callback)
    }
}

module.exports = SceneBase

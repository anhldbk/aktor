'use strict'

const TriggerBase = require('../base/trigger')
const _ = require('lodash')
const util = require('../../../util')
const vm = require('vm')
const sift = require('sift')
const Rx = require('rx')
const THROTTLE_DURATION = 300000 /* in ms <=> 5 min*/

class Trigger extends TriggerBase{
    /**
     * Constructor
     * @param {Object}   conf    A configuration object
     *
     {
         stream, // [Required] A Rx Observable object

         select: `{ // using json5 to use unquoted keys
             eventName: { sift-query-here }, // json string, using sift.js for query
             // eventName then can be used by "if" clause
             // for example
             temperatureEvent: { event.type : { $eq: 'event.sensor.fire.on'}}
         }`,

         if: `!eventName1 && temperatureEvent.data.temperature  > 30`,

         then: `[ // a json5 string
             {
                 id: 'service/speaker',
                 type: 'action.beep',
                 params: {
                     time: 3
                 }
             },
             {
                 id: 'service/sms',
                 type: 'action.send_sms',
                 params: {
                     to: '09xxx',
                     message: 'Hey Jude'
                 }
             },
             {
                 id: 'service/stage',
                 type: 'action.start_scene',
                 params: {
                     name: 'arm'
                 }
             }
         ]`,

         loggers: [ // optional
             {
                 script: 'Absolute path to SimpleLogger',
                 name
             },
             {
                 script: 'Absolute path to AdvancedLogger',
                 name,
                 host,
                 port
             }
         ]
     }
     */
    constructor(conf){
        super(conf)
        this._stopped = false
        util.bindThis(this, Trigger)
    }

    /**
     * Start the trigger
     * @param {Function} callback Callback function
     */
    start(callback){
        if( this._stopped ){
            return callback && callback('status.failure.already_stopped')
        }

        if( this._subscription ){
            return callback && callback('status.failure.already_started')
        }

        this._stream = null
        this._subscription = null


        if( util.isAnyNil(this._conf, ['select', 'if', 'then', 'stream']) ){
            return callback && callback('status.failure.invalid_descriptor')
        }

        var sourceStream = this._conf.stream
        var selectClause = this._conf.select
        var ifClause = _.get( this._conf, 'if', 'true') // if no condition provided, default is set to true.
        var thenClause = this._conf.then

        if(!(sourceStream instanceof Rx.Observable)){
            return callback && callback('status.failure.invalid_source_stream')
        }

        // all clauses are strings
        selectClause = this.parseJson(selectClause)
        if( !_.isObject(selectClause) ){ // must be an object
            return callback && callback('status.failure.invalid_select_clause')
        }

        if( !_.isString(ifClause) ){
            return callback && callback('status.failure.invalid_if_clause')
        }

        thenClause = this.parseJson(thenClause)
        if( !_.isArray(thenClause) ){ // must be an array of actions
            return callback && callback('status.failure.invalid_then_clause')
        }

        var self = this
        self._stream = sourceStream.select( (event) => {
            return self.selectEvent(event, selectClause)
        } )
        .filter( (event) => { // remove any nil event
            return !_.isNil(event)
        } )
        .scan( (acc, event) => { // because events are discrete, we must accumulate them
            return _.merge(acc, event)
        } )
        .filter( (event) =>{
            return self.checkCondition(ifClause, event)
        } )
        .throttle(THROTTLE_DURATION)
        .selectMany( (event) => {
            return Rx.Observable.fromArray( thenClause )
        })

        self._subscription = self._stream.subscribe(
            function (action) {
                self.emitAction(action)
            },
            function (err) {
                self.logError(`Something's wrong. Reason: ${err}`)
            }
        )

        callback && callback()
    }

    /**
     * Check a condition expression bounded with a context
     * @param  {String} condition A JS-based condition. For example: `fireEvent.fire && temperatureEvent.temperature  > 30`
     * @param  {Object} context   The context to bound to
     * @return {Boolean}           Returns true if the condition is satisfied. Otherwise, returns false.
     */
    checkCondition(condition, context){
        if( _.isNil(context) ){
            context = {}
        }

        // we don't want to modify the original context
        context = _.cloneDeep(context)
        var script = `
            output = false
            try{
                if(${condition}){
                    output = true
                }
            } catch(e){
            }
        `
        script = new vm.Script(script)
        script.runInNewContext(context)

        return context.output
    }

    /**
     * Transform a matched event according to a select query
     * @param  {Object} event  An event
     * @param  {String, Object} select The query. For example:
     *
     *`{ // using json5 to use unquoted keys
         eventName: { sift-query-here }, // json string, using sift.js for query
         // eventName then can be used by "if" clause
         // for example
         temperatureEvent: { event.type : { $eq: 'event.sensor.fire.on'}}
     }`
     *
     * @return {Object}  If the event doesn't match the query, returns null.
     *   Otherwise, returns the transformed event with key = `event name`. For example,
     *   if `temperatureEvent` is matched, the result will be:
     *   {
     *       temperatureEvent: <event object>
     *   }
     */
    selectEvent(event, select){
        if( !_.isObject(select) ){
            select = this.parseJson(select)
        }
        if( !_.isObject(select) ){
            return null
        }
        if( !_.isArray(event) ){
            event = [event]
        }
        var keys = _.keys(select), key

        for( var i = 0; i < keys.length; i++ ){
            key = keys[i]
            if( sift(select[key], event).length !== 0 ) {
                var result = {}
                result[key] = event[0].data
                return result
            }
        }

        return null
    }

    /**
     * Stop the trigger by destroying any allocated resources
     * @param {Function} callback Callback function
     */
    stop(callback){
        this._subscription.dispose()
        this._subscription = null
        this._stream = null
        this._stopped = true
        super.stop(callback)
    }

}

module.exports = Trigger

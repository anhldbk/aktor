    'use strict'

const StageBase  = require('../base/stage')
const Scheduler = require('../scheduler')
const Rx = require('rx')
const _ = require('lodash')

class Stage extends StageBase{

    constructor(conf){
        var path = 'interfaces.local.listen'
        _.set(conf, path, {
            'device/+/:event/update': 'onUpdate' // all update from devices
        })

        super(conf)

        this._scheduler = new Scheduler()
        this._executor = new Executor()
    }

    _init(callback){
        // load scenes from the local storage into `this._scenes`
        callback && callback()
    }

    addScene(scene, callback){
        callback && callback()
    }

    removeScene(scene, callback){
        callback && callback()
    }

    updateScene(scene, callback){
        callback && callback()
    }

    startScene(scene, callback){
        callback && callback()
    }

    stopScene(sceneName, callback){
        callback && callback()
    }

    addEvent(event, callback){
        callback && callback()
    }

    removeEvent(event, callback){
        callback && callback()
    }

    startAction(action, callback){

    }
}

module.exports = Stage

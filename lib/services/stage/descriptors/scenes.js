{
    scenes: [
        {
            id: "global.scene.arm",
            group: "global",

            schedule: {
                allowed: [
                    {
                        start,
                        stop
                    }
                ],
                disallowed: [
                    exception
                ]
            },

            actions: {
                onStart: [
                    {}
                ],

                onMain: [
                    {}
                ],

                onDone: [
                    {}
                ]
            },

            triggers: [
                {
                    select: `{

                    }`,
                    if: `{

                    }`,
                    then: `{

                    }`
                }
            ]
        }
    ]
}

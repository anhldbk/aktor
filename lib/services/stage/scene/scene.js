'use strict'

const SceneBase = require('../base/scene')
const _ = require('lodash')
const Rx = require('rx')
const async = require('async')
const util = require('../../../util')
const JSON5 = require('json5')

class Scene extends SceneBase {

    /**
     * Constructor
     * @param  {Object} conf    Configuration object. A sample configuration may look like
     *
         {
             stream, // [Required] A Rx Observable object

             actions: { // optional
                 start: [ // optional, registers actions to execute when starting this scene
                     `{
                         type: 'action',
                         endpoint: 'service/speaker/:request/beep',
                         params: {
                             beep: 3
                         }
                     }`,
                 ],

                 started: [ // optional, registers actions to execute when this scene started

                 ],

                 stop: [ // optional, registers actions to execute when stopping this scene

                 ],

                 stopped: [ // optional, registers actions to execute when this scene stopped

                 ]

             },

             triggers: [ // list of triggers
                 { // trigger
                     // using json5 to use unquoted keys
                     select: `{
                         eventName: { sift-query-here }, // json string, using sift.js for query
                         // eventName then can be used by "if" clause
                         // for example
                         temperatureEvent: { event.type : { $eq: 'event.sensor.fire.on'}}
                     }`,

                     if: `!eventName1 && temperatureEvent.data.temperature  > 30`,

                     // event can be a string which is a function to evaluate
                     // (event, callback) => { callback(null, true | false ) }. false to reject events
                     then: `[
                         {
                             action: 'service/speaker/:request/beep',
                             params: {
                                 time: 3
                             }
                         },
                         {
                             action: 'service/sms/:request/sms',
                             params: {
                                 to: '09xxx',
                                 message: 'Hey Jude'
                             }
                         },
                         {
                             action: 'service/stage/:request/start_scene',
                             params: {
                                 name: 'arm'
                             }
                         }
                     ]`

                 }
             ]

         }
     *
     */
    constructor(conf){
        var stream = _.get( conf, 'stream')
        if(!(stream instanceof Rx.Observable)){
            throw new Error('status.failure.invalid_stream')
        }
        super(conf)
    }

    /**
     * Start this scene
     * @param  {Function} callback A callback function `(err, result)`
     */
    start(callback){
        var self = this
        var execStartingActions = function(cb){
            var actions = _.get( self._conf, 'actions.start', [] )
            self.emit('actions', actions) // actions are strings, not object
            cb && cb()
        }

        var execStartedActions = function(cb){
            var actions = _.get( self._conf, 'actions.started', [] )
            self.emit('actions', actions)
            cb && cb()
        }

        var createTriggers = function(cb){
            var triggers = _.get( self._conf, 'triggers', [] )
            var stream = _.get( self._conf, 'stream', null)
            cb && cb()
        }

        async.waterfall(
            [
                execStartingActions,
                createTriggers,
                execStartedActions
            ],
            callback
        )
    }

    /**
     * Stop this scene
     * @param  {Function} callback A callback function `(err, result)`
     */
    stop(callback){
        callback && callback()
    }

    /**
     * Configure this scene with a new configuration
     * @param  {Object}   conf     The configuration
     * @param  {Function} callback A callback function `(err, result)`
     */
    configure(conf, callback){
        callback && callback()
    }

    /**
     * Dispose the scene
     * @param  {Function} callback A callback function `(err, result)`
     */
    dispose(callback){
        super.dispose(callback)
    }
}

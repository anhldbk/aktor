'use strict'

const SchedulerBase = require('../base/scheduler')
const later = require('later')

// Base class for schedulers
class Scheduler extends SchedulerBase{

    /**
     * Constructor
     */
    constructor(conf){
        super(conf)
    }

    /**
     * Enable the Scheduler
     */
    enable(callback){
        callback && callback()
    }

    /**
     * Disable the Scheduler
     */
    disable(callback){
        callback && callback()
    }

    /**
     * Schedule events
     * @param  {String} tag     An associated tag
     * @param  {Array, Object} time    An array of objects or an object having 2 fields:
     *     {String} `start`: cron string to define starting time
     *     {String} `stop`: [optional] cron string to define stopping time
     * @return: Returns true if everything is ok. Otherwise, returns false.
     * @note: Starting time should be lesser than stopping time
     *
     * @example:
     *
     * 1. Schedule arm mode to start at 10:15am every day
     *
     * ```js
     *  scheduler.schedule('arm', { start: '15 10 ? * *'})
     *
     *  // scheduler will emit event `start` with associated tag `arm`
     *
     *  scheduler.on('start', function(tag){
     *      // tag == `arm`
     *  })
     * ```
     *
     * 2. Schedule arm mode to start at 08:00am and stop at 06:00pm every day
     *
     * ```js
     *  scheduler.schedule('arm', {
     *      start: '00 08 ? * *',
     *      stop: '00 18 ? * *'
     *  })
     *
     *  // scheduler will emit event `start`, `stop` with associated tag `arm`
     *
     *  scheduler.on('start', function(tag){
     *      // tag == `arm`
     *  })
     *
     *  scheduler.on('stop', function(tag){
     *      // tag == `arm`
     *  })
     *
     * ```
     *
     */
    schedule(tag, time){
        return false
    }

    /**
     * Reschedule
     * Parameters are just like with `schedule`
     * @return: Returns true if everything is ok. Otherwise, returns false.
     * @note: Starting time should be lesser than stopping time
     */
    reschedule(tag, time){

    }

}

module.exports = SchedulerBase

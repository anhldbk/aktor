'use strict'

const async = require('async')
const _ = require('lodash')
const util = require('../../util')
const CommonAktor = require('../core/aktor').CommonAktor
const MqttInterface = require('../core/interface').MqttInterface
const ParseStorage = require('../core/storage').ParseStorage

// A service for updating platform & its services
// It runs in a standalone services
class WatsonService extends CommonAktor {
    constructor(conf){
        // register handlers
        var path = 'interfaces.local.listen'
        _.set(conf, path, {
            ':request/register': 'onRegister',
            ':request/unregister': 'onUnregister',
            ':request/update_event': 'onUpdateEvent',
            ':request/update_context': 'onUpdateContext',
            ':request/act': 'onAct',
            ':request/query': 'onQuery',
        })

        super(conf)
    }

    _init(callback){

    }

    onRegister(message){

    }

    onUnregister(message){

    }

    onUpdateState(message){
        // only parents can invoke this
    }

    onUpdateContext(message){

    }

    onUpdateEvent(message){

    }

    onAct(message){

    }

    onQuery(message){

    }

    forward(message, endpoint, callback){

    }
}

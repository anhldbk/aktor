'use strict'
const MongoClient = require('mongodb').MongoClient
const IdentityBase = require('./base')
const _ = require('lodash')
const async = require('async')
const util = require('../../util')

const DEFAULT_URL = "mongodb://localhost:27017/mqtt"

// Class for managing credentials to connect to eMQTT
// Be sure to have the broker properly configured
class MqttIdentity extends IdentityBase{

    // Constructor
    // @param conf: [Map, optional, default {}] A configuration object. At least, it must contain the following field:
    //  "url": [String, optional, default "mongodb://localhost:27017/mqtt"] Mongodb url
    constructor(conf){
        super(conf)
        this._db = null
    }

    // Initialize the service
    // @param callback: [Function] A function `(err, result)` to process results
    init(callback){
        var url = _.get(this._conf, 'url', DEFAULT_URL)
        var self = this
        MongoClient.connect(url, function(err, db) {
            if(err){
                return callback && callback('status.failure.cant_connect')
            }
            self._db = db
            callback && callback()
        })
    }

    // Dispose the service
    // @param callback: [Function] A function `(err, result)` to process results
    dispose(callback){
        var done = (err) => callback(err)
        this._db && ( this._db.close( done ) )
    }

    // Create a new user
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Object, optional, default {}] ACL options. May contains following fields:
    //  `pubsub`: [Array of String] Topics that can be published & subscribed
    //  `publish`: [Array of String] Topics that can be published
    //  `subscribe`: [Array of String] Topics that can be subscribed
    // @param callback: [Function] A function `(err, result)` to process results
    create(username, password, options, callback){
        if( _.isFunction(password) ){
            // invoked as `create(username, callback)`
            callback = password
            password = null
            // TODO: any ACL rule else.
            options = { pubsub: ['#'] }
        }

        if( _.isObject(password) ){
            // invoked as `create(username, options, callback)`
            callback = options
            options = password
            password = null
        }

        if( _.isFunction( options) ){
            callback = options
            // TODO: any ACL rule else.
            options = { pubsub: ['#'] }
        }


        var ifExistsThenError = (exists,cb) => {
            if(exists){
                return cb('status.failure.already_exists')
            }
            cb()
        }

        async.waterfall([
            async.apply( this.exist, username ),
            ifExistsThenError,
            async.apply( this.update, username, password, options)
        ], callback)
    }

    // Read information about users
    // @param username: [String] Username
    // @param options: [Map, optional, default {}] Any other options (Not used by this method)
    // @param callback: [Function] A function `(err, result)` to process results
    read(username, options, callback){
        if( _.isFunction(options)) {
            // options is omitted
            // invoked as `read(username, callback)`
            callback = options
            options = {}
        }
        var self = this
        var id = username

        var getEntity = function(cb){
            var collection = self._db.collection('mqtt_entity')
            var query = { id }
            var projection = { _id: 0 }

            var done = function(err, result){
                if (err || !result) {
                    return cb('status.failure.get_entity')
                }
                cb(null, result)
            }
            collection.findOne(query, projection, done)
        }

        var getPermissions = function(entity, cb){
            var collection = self._db.collection('mqtt_acl')
            var query = { id }
            var projection = { _id: 0, id: 0 }
            var done = function(err, result){
                if(err){
                    return cb('status.failure.get_permissions')
                }
                result && ( entity.permissions = result ) // if there's no permission, set to {}
                cb(null, entity)
            }
            collection.findOne(query, projection, done)
        }

        async.waterfall(
            [ getEntity, getPermissions ],
            callback
        )
    }

    // Delete a user
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    delete(username, callback){
        var self = this
        var id = username

        var deleteEntity = function(cb){
            var collection = self._db.collection('mqtt_entity')
            var query = { id }

            var done = function(err, result){
                if (err || !result) {
                    return cb('status.failure.delete_entity')
                }
                if ( result.result.n == 0 ) { // no entity deleted
                    return cb('status.failure.no_such_user')
                }

                cb(null, result)
            }
            collection.removeOne(query, done)
        }

        var deletePermissions = function(entity, cb){
            var collection = self._db.collection('mqtt_acl')
            var query = { id }

            var done = function(err, result){
                if (err || !result) {
                    return cb('status.failure.delete_permissions')
                }

                cb(null, result)
            }
            collection.removeOne(query, done)
        }

        var deleteDone = function(err, result){
            callback(err)
        }

        async.waterfall(
            [ deleteEntity, deletePermissions ],
            deleteDone
        )
    }

    // Check if a user exists
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    exist(username, callback){
        var collection = this._db.collection('mqtt_entity')
        var id = username
        var query = { id }

        var done = function(err, result){
            if (err) {
                return callback && callback('status.failure.get_entity')
            }

            callback && callback(null, ! _.isNil(result) )
        }
        collection.findOne(query, done)
    }

    // Update information about a user
    // @param username: [String] Username
    // @param password: [String, optional] New password to set
    // @param options: [Map, optional, default {}] Any other options to set
    // @param callback: [Function] A function `(err, result)` to process results
    update(username, password, options, callback){
        if( _.isObject(password)) {
            // password is omitted
            // invoked as `update(username, options, callback)`
            options = password
            password = null
        }
        if( _.isFunction(options) ){
            callback = options
            options = {}
        }
        if( _.isNil(options) ){
            options = {}
        }

        var self = this
        var id = username

        var setEntity = function(cb){
            if( _.isNil(password) ){
                return cb() // no need to do anything
            }
            var query = { id }
            var data = { id, token: util.getSha256Hash(password) }
            var update = { $set: data }
            var collection = self._db.collection('mqtt_entity')
            var done = function(err, result) {
                if (err) {
                    err = 'status.failure.set_entity'
                }
                cb(err)
            }
            collection.updateOne(query, update, { upsert: true }, done)
        }

        var setPermissions = function(cb) {
            if (_.isNil(options) || _.keys(options).length == 0) {
                return cb() // nothing to do
            }
            options.id = id
            var query = { id }
            var update = { $set: options }
            var collection = self._db.collection('mqtt_acl')

            var done = function(err, result) {
                if (err) {
                    err = 'status.failure.set_permissions'
                }
                cb(err)
            }

            collection.updateOne(query, update, { upsert: true }, done)
        }

        async.waterfall(
            [ setEntity, setPermissions ],
            callback
        )
    }

    // Get information about all users
    // @param options: [Map, optional, default {}] Any other options (Not used by this method)
    // @param callback: [Function] A function `(err, result)` to process results
    readAll(options, callback){
        if( _.isFunction(options) ){
            callback = options
            options = {}
        }
        var self = this

        var getEntities = function(cb){
            var collection = self._db.collection('mqtt_entity')
            var done = function(err, result) {
                if (err) {
                    err = 'status.failure.get_entity'
                }
                cb(err, result)
            }

            var projection = {_id: 0}
            collection.find().project(projection).toArray(done)
        }

        var getPermissions = function(entities, cb){
            var collection = self._db.collection('mqtt_acl')
            var done = function(err, result) {
                if (err) {
                    err = 'status.failure.get_permissions'
                }
                cb(err, entities, result)
            }

            var projection = {_id: 0}
            collection.find().project(projection).toArray(done)
        }

        var merge = function(entities, permissions, cb){
            permissions = _.keyBy( permissions, 'id' )
            var id, entity

            for( var i = 0; i < entities.length; i++){
                entity = entities[i]
                id = entity.id
                if( !permissions[id] ){
                    // no permissions
                    continue
                }
                _.unset( permissions[id], 'id' )
                _.set( entity, 'permissions', permissions[id])
            }
            cb(null, entities)
        }

        async.waterfall(
            [ getEntities, getPermissions, merge ],
            callback
        )
    }
}

module.exports = MqttIdentity

'use strict'
const IdentityBase = require('./base')
const _ = require('lodash')
const async = require('async')
const Parse = require('parse/node').Parse
const parse = require('../parse')

// Interface for Identity services
class ParseIdentity extends IdentityBase {

    // Constructor
    // @param conf: [Map, optional, default {}] A configuration object
    constructor(conf) {
        super(conf)
    }

    // Initialize the service
    // @param callback: [Function] A function `(err, result)` to process results
    init(callback) {
        var err = null
        if (!parse.initialize()) {
            err = 'status.failure.parse_not_ready'
        }
        callback && callback(err)
    }

    // Create a new user
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    create(username, password, options, callback) {
        if( _.isFunction(password) ){
            // invoked as `create(username, callback)`
            callback = password
            password = token
            options = {  }
        }

        if( _.isObject(password) ){
            // invoked as `create(username, options, callback)`
            callback = options
            options = password
            password = token
        }

        if (_.isFunction(options)) {
            // invoked as `create(username, password, callback)`
            callback = options
            options = {}
        }

        Parse.User.signUp(username, password, options, {
            success: function(user) {
                callback && callback()
            },
            error: function(user, error) {
                callback && callback('status.failure.can_not_create')
            }
        })
    }

    // Check if a user exists
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    exist(username, callback) {
        var query = new Parse.Query(Parse.User)
        query.equalTo("username", username)
        query.first({
            success: function(result) {
                callback && callback(null, !_.isNil(result))
            },
            error: function(error) {
                callback && callback('status.failure.can_not_query')
            }
        })
    }

    // Read information about users
    // @param username: [String] Username
    // @param options: [Map, optional, default {}] Any other options (no supported)
    // @param callback: [Function] A function `(err, result)` to process results
    read(username, options, callback) {
        if (_.isFunction(options)) {
            // invoked as read(username, callback)
            callback = options
            options = {}
        }

        var query = new Parse.Query(Parse.User)
        query.equalTo("username", username)
        query.first({
            success: function(result) {
                var err = null
                if (_.isNil(result)) {
                    err = 'status.failure.no_such_user'
                }
                callback && callback(err, result)
            },
            error: function(error) {
                callback && callback('status.failure.can_not_query')
            }
        })
    }

    // Delete a user
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    delete(username, callback) {
        var done = function(err, result) {
            if (err) {
                return callback && callback(err)
            }
            result.destroy({
                success: function(object) {
                    callback && callback()
                },
                error: function(object, error) {
                    callback && callback('status.failure.can_not_delete')
                }
            })
        }
        this.read(username, done)
    }

    // Update information about a user
    // @param username: [String] Username
    // @param password: [String, optional] New password to set
    // @param options: [Map, optional, default {}] Any other options to set
    // @param callback: [Function] A function `(err, result)` to process results
    update(username, password, options, callback) {
        if ( _.isObject(password) ) {
            // password is omitted
            // invoked as `update(username, options, callback)`
            options = password
            password = null
        }

        if (_.isFunction(options)) {
            callback = options
            options = {}
        }
        if( _.isNil( options )){
            options = {}
        }
        if (!_.isNil(password)) {
            options['password'] = password
        }

        var done = function(err, result) {
            if (err) {
                return callback && callback(err)
            }

            result.save(options, {
                success: function(object) {
                    callback && callback()
                },
                error: function(object, error) {
                    callback && callback('status.failure.can_not_update')
                }
            })
        }
        this.read(username, done)
    }

    // Get information about all users
    // @param options: [Map, optional, default {}] Any other options (No options supported)
    // @param callback: [Function] A function `(err, result)` to process results
    readAll(options, callback) {
        if (_.isFunction(options)) {
            // invoked as readAll(callback)
            callback = options
        }
        var query = new Parse.Query(Parse.User)
        query.find({
            success: function(results) {
                callback && callback(null, results)
            },
            error: function(error) {
                callback && callback('status.failure.can_not_query')
            }
        });
    }
}

module.exports = ParseIdentity

'use strict'

const platformConf = require('../conf')
const util = require('../../util')
const identity = require('../identity')
const interfake = require('../core/interface')
const storage = require('../core/storage')
const logger = require('../core/logger')
const async = require('async')
const _ = require('lodash')
const MqttInterface = interfake.MqttInterface
const ParseStorage = storage.ParseStorage
const TimeSeriesStorage = storage.TimeSeriesStorage
const SimpleLogger = logger.SimpleLogger
const AdvancedLogger = logger.AdvancedLogger
const MqttIdentity = identity.MqttIdentity
const InfluxIdentity = identity.InfluxIdentity
const ParseIdentity = identity.ParseIdentity

// Class for generating local configurations for aktors
class IdentityService {
    constructor() {
        this._mqttIdentity = new MqttIdentity(platformConf.emqtt)
        this._influxIdentity = new InfluxIdentity(platformConf.influx)
        this._parseIdentity = new ParseIdentity(platformConf.parse)
        this.exist = this.exist.bind(this)
        this._grant = this._grant.bind(this)
        this._generate = this._generate.bind(this)
    }

    init(callback) {
        async.waterfall(
            [
                this._mqttIdentity.init,
                this._influxIdentity.init,
                this._parseIdentity.init,
            ],
            calback
        )
    }

    dispose(callback) {
        callback && callback
    }

    // Grant an aktor to access platform services
    // @param id: The aktor's id
    // @param token: The aktors' token
    // @param permissions: [Object, optional, default { pubsub: ['#'] }] Permissions to pub/sub messages. May contains following fields:
    //  `pubsub`: [Array of String] Topics that can be published & subscribed
    //  `publish`: [Array of String] Topics that can be published
    //  `subscribe`: [Array of String] Topics that can be subscribed
    // @param callback: [Function] A function `(err, result)` to process results. If the aktor's already granted, `err` = `status.failure.already_exists`. Otherwise, `err` = null
    _grant(id, token, permissions, callback) {
        if (_.isFunction(permissions)) {
            // invoked as _grant(id, token, callback)
            callback = permissions
            permissions = {
                // TODO: apply any restrictions on pubsub?
                pubsub: ['#']
            }
        }
        var parseOptions = {},
            mqttOptions = permissions,
            influxOptions = {}

        var ifExist = function(exists, cb) {
            if (exists) {
                return cb('status.failure.already_exists')
            }
            cb()
        }

        async.waterfall(
            [
                async.apply(this.exist, id),
                ifExist, // if exists, then return immediately
                async.apply(this._mqttIdentity.create, id, token, mqttOptions),
                async.apply(this._parseIdentity.create, id, token, parseOptions),
                async.apply(this._influxIdentity.create, id, token, influxOptions),
            ],
            callback
        )
    }

    // Check if an ID exists
    // @param id: ID to check
    // @param callback: [Function] a function `(err, result)` to process results
    exist(id, callback) {
        return this._mqttIdentity.exist(id, callback) // just need to check this Identity service
    }

    // Grant an aktor to access platform services
    // @param id: Id of the aktor
    // @param callback: [Function] Function `(err, result)` to process results. If everything is OK, `err` is null and `result` contains configuration for the aktor .
    grant(id, callback) {
        var token = util.getRandomId()

        async.waterfall(
            [
                async.apply( this._grant, id, token ),
                async.apply( this._generate, id, token )
            ],
            callback
        )
    }

    // Revoke an aktor from access platform services
    // @param id: Id of the aktor
    // @param callback: [Function] Function `(err, result)` to process results. If everything is OK, `err` is null. Otherwise, `err` contains error information.
    revoke(id, callback){
        var ifExist = function(exists, cb) {
            if (!exists) {
                return cb('status.failure.no_such_id')
            }
            cb()
        }

        async.waterfall(
            [
                async.apply(this.exist, id),
                ifExist, // if not exists, then return immediately
            ],
            callback
        )
    }

    // Generate configurations for aktors
    // @param id: Aktor's id
    // @param token: Aktor's token
    // @param callback: [Function] Function `(err, result)` to process results. If everything is OK, `err` is null and `result` contains configuration for the aktor .
    _generate(id, token, callback) {
        var timeout = platformConf.environ.timeout,
            timeoutRatio = platformConf.environ.timeoutRatio,
            mqttConf = _.cloneDeep(platformConf.emqtt),
            parseConf = _.cloneDeep(platformConf.parse),
            timeseriesConf = _.cloneDeep(platformConf.influx),
            loggersConf = []

        // set configuration for MqttInterface
        _.merge(mqttConf, {
            script: MqttInterface.path,
            username: id,
            password: token
        })

        // set configuration for ParseStorage
        _.merge(parseConf, {
                script: ParseStorage.path,
                username: id,
                password: token
            })
            // unset unnecessary fields
        _.unset(parseConf, 'masterKey')
        _.unset(parseConf, 'fileKey')

        // set configuration for TimeSeriesStorage
        _.merge(timeseriesConf, {
            script: TimeSeriesStorage.path,
            username: id,
            password: token
        })

        loggersConf.push({
            script: SimpleLogger.path,
            name: id
        })

        var conf = {
            id,
            token,
            timeout,
            timeoutRatio,
            interfaces: {
                local: mqttConf
            },
            storages: {
                local: parseConf,
                timeseries: timeseriesConf
            },
            loggers: loggersConf
        }

        callback && callback( null, conf )
    }
}



module.exports = IdentityService

'use strict'

const IdentityBase = require('./base')
const _ = require('lodash')
const async = require('async')
const Parse = require('parse/node').Parse
const util = require('../util')
var influx = require('influx')

// Interface for Identity services
// Besure to have Influx configured with authentication & authorization
// Link: https://docs.influxdata.com/influxdb/v0.9/administration/authentication_and_authorization/#admin-user-management
// command:
// > create user root with password 'a33855ab-7ead-4e31-98e1-cdce3c588052' with all privileges
class InfluxIdentity extends IdentityBase{

    // Constructor
    // @param conf: [Map, optional, default {}] A configuration object
    constructor(conf){
        super(conf)
    }

    // Initialize the service
    // @param callback: [Function] A function `(err, result)` to process results
    init(callback){
        if( !util.isInfluxReady() ){
            return callback && callback('status.failure.influx_not_ready')
        }

        this._client = influx( this._conf )
        callback && callback()
    }

    // Dispose the service
    // @param callback: [Function] A function `(err, result)` to process results
    dispose(callback){
        callback && callback()
    }

    // Create a new user
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Map, optional, default {}] Any other options. Following fields are supported:
    //   `isAdmin`: [Boolean, default false] If the new user created is an admin, set this field to true
    // @param callback: [Function] A function `(err, result)` to process results
    create(username, password, options, callback){

        if( _.isFunction(password) ){
            // invoked as `create(username, callback)`
            callback = password
            password = token
            options = { }
        }

        if( _.isObject(password) ){
            // invoked as `create(username, options, callback)`
            callback = options
            options = password
            password = token
        }

        if (_.isFunction(options)) {
            // invoked as `create(username, password, callback)`
            callback = options
            options = {}
        }

        if( !this._client){
            return callback && callback('status.failure.not_initialized')
        }

        var isAdmin = _.get( options, 'isAdmin')
        if( !_.isBoolean( isAdmin )){
            isAdmin = false
        }

        var self = this
        var done = function(err){
            callback && callback(err)
        }
        var next = function(err, result){
            if(err){
                return callback && callback(err)
            }
            if(result === true){
                // Username already exists
                return callback && callback('status.failure.can_not_create')
            }
            self._client.createUser(username, password, isAdmin, done)
        }


        this.exist(username, next)
    }

    // Check if a user exists
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    exist(username, callback){
        if( !this._client){
            return callback && callback('status.failure.not_initialized')
        }
        var done = function (err, users) {
            if(err){
                return callback && callback('status.failure.can_not_query')
            }

            var result = _.findIndex(users, function(o) { return o.user == username })
            result = result != -1
            callback && callback(null, result)
         }
        this._client.getUsers(done)
    }

    // Read information about users
    // @param username: [String] Username
    // @param options: [Map, optional, default {}] Any other options (no supported)
    // @param callback: [Function] A function `(err, result)` to process results
    read(username, options, callback){
        if( !this._client){
            return callback && callback('status.failure.not_initialized')
        }
        if (_.isFunction(options)) {
            // invoked as read(username, callback)
            callback = options
            options = {}
        }

        var done = function (err, users) {
            if(err){
                return callback && callback('status.failure.can_not_query')
            }

            var index = _.findIndex(users, function(o) { return o.user == username })
            if( index == -1 ){
                return callback && callback('status.failure.no_such_user')
            }

            callback && callback(null, users[index])
         }
        this._client.getUsers(done)
    }

    // Delete a user
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    delete(username, callback){
        if( !this._client){
            return callback && callback('status.failure.not_initialized')
        }
        var done = function(err, result){
            if(err){
                err = 'status.failure.no_such_user'
            }
            callback && callback(err)
        }

        this._client.dropUser(username, done)
    }

    // Update information about a user
    // @param username: [String] Username
    // @param password: [String, required in this class] New password to set (In this derived class, password is required)
    // @param options: [Map, optional, default {}] Any other options to set (No option is supported)
    // @param callback: [Function] A function `(err, result)` to process results
    update(username, password, options, callback){
        if( _.isFunction(options) ){
            callback = options
            options = {}
        }
        if( !this._client){
            return callback && callback('status.failure.not_initialized')
        }
        if( !_.isString(password) ){
            return callback && callback('status.failure.invalid_password')
        }

        var self = this
        var done = function(err){
            callback && callback(err)
        }
        var next = function(err, result){
            if(err){
                return callback && callback(err)
            }
            if(result === false){
                return callback && callback('status.failure.no_such_user')
            }
            self._client.setPassword(username, password, done)
        }

        this.exist(username, next)
    }

    // Get information about all users
    // @param options: [Map, optional, default {}] Any other options to set (No option is supported)
    // @param callback: [Function] A function `(err, result)` to process results
    readAll(options, callback){
        if( !this._client){
            return callback && callback('status.failure.not_initialized')
        }
        if (_.isFunction(options)) {
            // invoked as read(username, callback)
            callback = options
            options = {}
        }

        var done = function (err, users) {
            if(err){
                return callback && callback('status.failure.can_not_query')
            }

            callback && callback(null, users)
         }
        this._client.getUsers(done)
    }
}

module.exports = InfluxIdentity

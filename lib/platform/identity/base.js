'use strict'
const util = require('../../util')

// Interface for Identity services
class IdentityBase{

    // Constructor
    // @param conf: [Map, optional, default {}] A configuration object
    constructor(conf){
        this._conf = conf

        // auto bind context for member functions
        this.init = this.init.bind(this)
        this.dispose = this.dispose.bind(this)
        this.create = this.create.bind(this)
        this.exist = this.exist.bind(this)
        this.read = this.read.bind(this)
        this.delete = this.delete.bind(this)
        this.update = this.update.bind(this)
        this.readAll = this.readAll.bind(this)
        this.upsert = this.upsert.bind(this)
    }

    // Initialize the service
    // @param callback: [Function] A function `(err, result)` to process results
    init(callback){
        callback && callback()
    }

    // Initialize the service synchronously
    // @param callback: [Function] A function `(err, result)` to process results
    initSync(callback){
        return util.callsync( this.init, this )
    }

    // Dispose the service
    // @param callback: [Function] A function `(err, result)` to process results
    dispose(callback){
        callback && callback()
    }

    // Dispose the service synchronously
    // @param callback: [Function] A function `(err, result)` to process results
    disposeSync(callback){
        return util.callsync( this.dispose, this )
    }

    // Create a new user
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    create(username, password, options, callback){
        callback && callback()
    }

    // Create a new user synchronously
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Map, optional, default {}] Any other options
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    createSync(username, password, options){
        return util.callsync( this.create, this, [ username, password, options ])
    }


    // Create or update a new user. If the user doesn't exist, we'll create it. Otherwise, we update its information.
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    upsert(username, password, options, callback){
        var self = this
        var done = (err, result) => {
            if(err){
                return callback && callback(err)
            }
            if( result === true ){
                // let's update it
                return self.update(username, password, options, callback)
            }
            // or not, we create it
            self.create(username, password, options, callback)
        }
        this.exist(username, done)
    }

    // Upsert synchronously
    // @param username: [String] Username
    // @param password: [String] Password
    // @param options: [Map, optional, default {}] Any other options
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    upsertSync(username, password, options){
        return util.callsync( this.upsert, this, [ username, password, options ])
    }

    // Check if a user exists
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    exist(username, callback){
        callback && callback(null, false)
    }


    // Check if a user exists synchronously
    // @param username: [String] Username
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    existSync(username){
        return util.callsync( this.exist, this, [ username ])
    }


    // Read information about users
    // @param username: [String] Username
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    read(username, options, callback){
        callback && callback()
    }

    // Read information about users synchronously
    // @param username: [String] Username
    // @param options: [Map, optional, default {}] Any other options
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    readSync(username, options){
        return util.callsync( this.read, this, [ username, options ])
    }

    // Delete a user
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    delete(username, callback){
        callback && callback()
    }

    // Delete a user synchronously
    // @param username: [String] Username
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    deleteSync(username){
        return util.callsync( this.delete, this, [ username ])
    }

    // Update information about a user
    // @param username: [String] Username
    // @param password: [String, optional] New password to set
    // @param options: [Map, optional, default {}] Any other options to set
    // @param callback: [Function] A function `(err, result)` to process results
    update(username, password, options, callback){
        callback && callback()
    }

    // Update information about a user synchronously
    // @param username: [String] Username
    // @param password: [String, optional] New password to set
    // @param options: [Map, optional, default {}] Any other options to set
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    updateSync(username, password, options){
        return util.callsync( this.update, this, [ username, password, options ])
    }

    // Get information about all users
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    readAll(options, callback){
        callback && callback()
    }

    // Get information about all users synchronously
    // @param options: [Map, optional, default {}] Any other options
    // @return: [Array] a pair of `(err, result)` which are passed to `callback`
    readAllSync(options){
        return util.callsync( this.readAll, this, [ options ])
    }
}

module.exports = IdentityBase

'use strict'

const IdentityBase = require('./base')
const MqttIdentity = require('./mqtt-entity')
const ParseIdentity = require('./parse-identity')
const InfluxIdentity = require('./influx-identity')
const PlatformIdentity = require('./platform-identity')

module.exports = {
    IdentityBase,
    MqttIdentity,
    ParseIdentity,
    InfluxIdentity,
    PlatformIdentity
}

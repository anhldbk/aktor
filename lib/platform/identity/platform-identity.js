'use strict'

const platformConf = require('../conf')
const util = require('../../util')
const interfake = require('../core/interface')
const storage = require('../core/storage')
const logger = require('../core/logger')
const async = require('async')
const _ = require('lodash')
const MqttIdentity = require('./mqtt-entity')
const InfluxIdentity = require('./influx-identity')
const ParseIdentity = require('./parse-identity')
const IdentityBase = require('./base')

const MqttInterface = interfake.MqttInterface
const ParseStorage = storage.ParseStorage
const TimeSeriesStorage = storage.TimeSeriesStorage
const SimpleLogger = logger.SimpleLogger
const Loggable = logger.Loggable
const AdvancedLogger = logger.AdvancedLogger

// Class for generating local configurations for aktors to access platform services
class PlatformIdentity extends IdentityBase{
    constructor(conf) {
        super(conf)

        this._mqttIdentity = new MqttIdentity(platformConf.emqtt)
        this._influxIdentity = new InfluxIdentity(platformConf.influx)
        this._parseIdentity = new ParseIdentity(platformConf.parse)
        this._grant = this._grant.bind(this)
        this._generate = this._generate.bind(this)
    }

    // Initialize the service
    // @param callback: [Function] A function `(err, result)` to process results
    init(callback) {
        async.waterfall(
            [
                this._mqttIdentity.init,
                this._influxIdentity.init,
                this._parseIdentity.init,
            ],
            callback
        )
    }


    // Dispose the service
    // @param callback: [Function] A function `(err, result)` to process results
    dispose(callback){
        async.waterfall(
            [
                this._mqttIdentity.dispose,
                this._influxIdentity.dispose,
                this._parseIdentity.dispose,
            ],
            callback
        )
    }

    // Check if an ID exists
    // @param username: ID to check
    // @param callback: [Function] a function `(err, result)` to process results
    exist(username, callback) {
        return this._mqttIdentity.exist(username, callback) // just need to check this Identity service
    }


    ////////////////////////////////////////////////////////////////////
    // Create a new user
    // @param username: [String, required] Username
    // @param password: [String, optional] Password. If none is provided, a random one will be used.
    // @param options:  [Object, optional, default { pubsub: ['#'] }] Permissions to pub/sub messages. May contains following fields:
    //  `pubsub`: [Array of String] Topics that can be published & subscribed
    //  `publish`: [Array of String] Topics that can be published
    //  `subscribe`: [Array of String] Topics that can be subscribed
    // @param callback: [Function, Optional] A function `(err, result)` to process results.
    create(username, password, options, callback){
        var id = username,
            token = util.getRandomId()

        if( _.isFunction(password) ){
            // invoked as `create(username, callback)`
            callback = password
            password = token
        }

        if( _.isObject(password) ){
            // invoked as `create(username, options, callback)`
            callback = options
            options = password
            password = token
        }

        if( _.isFunction( options) ){
            callback = options
        }

        if( !_.isString(password) ){
            password = token
        }

        if( _.isNil(options) ){
            // TODO: any ACL rule else.
            options = { pubsub: ['#'] }
        }

        async.waterfall(
            [
                async.apply( this._grant, id, password, options ),
                async.apply( this._generate, id, password )
            ],
            callback
        )
    }

    // Read information about users
    // @param username: [String] Username
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    read(username, options, callback){
        if( _.isFunction(options)) {
            // options is omitted
            // invoked as `read(username, callback)`
            callback = options
            options = {}
        }

        var parse, mqtt, influx, self = this

        var readParse = function(cb){
            var done = (err, result) => {
                parse = result
                cb() // dismiss any error
            }
            self._parseIdentity.read(username, options, done)
        }

        var readMqtt = function(cb){
            var done = (err, result) => {
                mqtt = result
                cb(err) // mqtt is so important
            }
            self._mqttIdentity.read(username, options, done)
        }

        var readInflux = function(cb){
            var done = (err, result) => {
                influx = result
                cb() // dismiss any error
            }
            self._influxIdentity.read(username, options, done)
        }

        var merge = function(cb){
            var result = { parse, influx, mqtt }
            cb(null, result)
        }

        var done = (err, result) =>{
            callback(err, result)
        }

        async.waterfall(
            [
                readMqtt,
                readParse,
                readInflux,
                merge
            ],
            done
        )
    }

    // Delete a user
    // @param username: [String] Username
    // @param callback: [Function] A function `(err, result)` to process results
    delete(username, callback){
        var self = this
        var deleteMqtt = (cb) => {
            var done = (err) => cb(err)
            self._mqttIdentity.delete( username, done)
        }
        var deleteInflux = (cb) => {
            var done = (err) => cb() // dismiss any error
            self._influxIdentity.delete( username, done)
        }
        var deleteParse = (cb) => {
            var done = (err) => cb()
            self._parseIdentity.delete( username, done)
        }
        async.waterfall(
            [
                deleteMqtt,
                deleteInflux,
                deleteParse
            ],
            callback
        )
    }

    // Update information about a user
    // @param username: [String] Username
    // @param password: [String, optional] New password to set
    // @param options: [Map, optional, default {}] Any other options to set
    // @param callback: [Function] A function `(err, result)` to process results
    update(username, password, options, callback){
        var id = username,
            token = util.getRandomId()

        if( _.isFunction(password) ){
            // invoked as `create(username, callback)`
            callback = password
            password = token
        }

        if( _.isObject(password) ){
            // invoked as `create(username, options, callback)`
            callback = options
            options = password
            password = token
        }

        if( _.isFunction( options) ){
            callback = options
            // TODO: any ACL rule else.
        }

        if(! _.isObject(options)) {
            // TODO: any ACL rule else.
            options = { pubsub: ['#'] }
        }

        if( !_.isString(password) ){
            password = token
        }

        async.waterfall(
            [
                async.apply( this._mqttIdentity.upsert, username, password, options ),
                async.apply( this._influxIdentity.upsert, username, password, {} ),
                async.apply( this._parseIdentity.upsert, username, password, {} ),
                async.apply( this._generate, id, password )
            ],
            callback
        )
    }

    // Get information about all users
    // @param options: [Map, optional, default {}] Any other options
    // @param callback: [Function] A function `(err, result)` to process results
    readAll(options, callback){
        if( _.isFunction(options) ){
            callback = options
            options = {}
        }
        var self = this

        var enumerateUsers = function(cb){
            var done = (err, result) => {
                if(err){
                    return cb(err)
                }

                var mapOp = (m) => m.id
                result = _.map( result, mapOp )
                cb(null, result)
            }
            self._mqttIdentity.readAll( options, done )
        }

        var getAll = function(usernames, cb){
            var res = []
            var getAllDone = (err, result) => {
                if(err){
                    return cb(err)
                }
                cb(null, res)
            }
            var get = (username, cb) => {
                var getDone = (err, result) => {
                    if(err){
                        return cb(err)
                    }
                    res.push( result )
                    cb()
                }
                self.read(username, getDone )
            }
            async.each( usernames, get, getAllDone )
        }

        async.waterfall(
            [ enumerateUsers, getAll ],
            callback
        )
    }

    // Grant an aktor to access platform services
    // @param id: The aktor's id
    // @param token: The aktors' token
    // @param permissions: [Object, optional, default { pubsub: ['#'] }] Permissions to pub/sub messages. May contains following fields:
    //  `pubsub`: [Array of String] Topics that can be published & subscribed
    //  `publish`: [Array of String] Topics that can be published
    //  `subscribe`: [Array of String] Topics that can be subscribed
    // @param callback: [Function] A function `(err, result)` to process results. If the aktor's already granted, `err` = `status.failure.already_exists`. Otherwise, `err` = null
    _grant(id, token, permissions, callback) {
        if (_.isFunction(permissions)) {
            // invoked as _grant(id, token, callback)
            callback = permissions
            permissions = {
                // TODO: apply any restrictions on pubsub?
                pubsub: ['#']
            }
        }

        var parseOptions = {},
            self = this,
            mqttOptions = permissions,
            influxOptions = {}

        async.waterfall(
            [
                async.apply(self._mqttIdentity.upsert, id, token, mqttOptions),
                async.apply(self._parseIdentity.upsert, id, token, parseOptions),
                async.apply(self._influxIdentity.upsert, id, token, influxOptions)
            ],
            callback
        )
    }

    // Generate configurations for aktors
    // @param id: Aktor's id
    // @param token: Aktor's token
    // @param callback: [Function] Function `(err, result)` to process results. If everything is OK, `err` is null and `result` contains configuration for the aktor .
    _generate(id, token, callback) {
        var timeout = platformConf.world.timeout,
            timeoutRatio = platformConf.world.timeoutRatio,
            clean = platformConf.world.clean,
            maxRestarts = platformConf.world.maxRestarts,
            mqttConf = _.cloneDeep(platformConf.emqtt),
            parseConf = _.cloneDeep(platformConf.parse),
            timeseriesConf = _.cloneDeep(platformConf.influx),
            loggersConf = []

        // set configuration for MqttInterface
        _.merge(mqttConf, {
            script: MqttInterface.path,
            username: id,
            password: token
        })

        // set configuration for ParseStorage
        _.merge(parseConf, {
                script: ParseStorage.path,
                username: id,
                password: token,
                name: 'local'
            })
            // unset unnecessary fields
        _.unset(parseConf, 'masterKey')
        _.unset(parseConf, 'fileKey')

        // set configuration for TimeSeriesStorage
        _.merge(timeseriesConf, {
            script: TimeSeriesStorage.path,
            username: id,
            password: token,
            name: 'timeseries'
        })

        loggersConf.push(
            Loggable.getDefaultLogger(id)
        )

        var conf = {
            id,
            name: id, // for displaying in PM2
            token,
            timeout,
            timeoutRatio,
            maxRestarts,
            clean, // discard previous sessions (esp. for containers, world.)
            interfaces: {
                local: mqttConf
            },
            storages: {
                local: parseConf,
                timeseries: timeseriesConf
            },
            loggers: loggersConf
        }

        callback && callback( null, conf )
    }
}



module.exports = PlatformIdentity

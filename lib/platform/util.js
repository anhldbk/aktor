'use strict'
const util = require('../util')
const conf = require('./conf')

// Check if mongodb is ready
// @return: Returns true if it's ready. Otherwise, returns false.
function isMongoDbReady(){
    return util.isPortOpenedSync( conf.mongodb.port )
}

// Check if parse is ready
// @return: Returns true if it's ready. Otherwise, returns false.
function isParseReady(){
    return util.isPortOpenedSync( conf.parse.port )
}

// Check if influx is ready
// @return: Returns true if it's ready. Otherwise, returns false.
function isInfluxReady(){
    return util.isPortOpenedSync( conf.influx.port )
}

// Check if fluentd is ready
// @return: Returns true if it's ready. Otherwise, returns false.
function isFluentdReady(){
    return util.isPortOpenedSync( conf.fluentd.port )
}

// Check if eMqtt is ready
// @return: Returns true if it's ready. Otherwise, returns false.
function isMqttReady(){
    return util.isPortOpenedSync( conf.emqtt.port )
}

// Check if all platform services are ready
// @return: Returns null if they are ready. Otherwise, returns error strings
function isPlatformReady(){
    if(!isMongoDbReady()){
        return 'status.failure.mongodb_not_ready'
    }
    if(!isParseReady()){
        return 'status.failure.parse_not_ready'
    }
    if(!isMqttReady()){
        return 'status.failure.mqtt_not_ready'
    }
    if(!isInfluxReady()){
        return 'status.failure.influx_not_ready'
    }
    return null
}

module.exports = {
    isMongoDbReady,
    isParseReady,
    isInfluxReady,
    isMqttReady,
    isFluentdReady,
    isPlatformReady
}

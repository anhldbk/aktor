'use strict'
const EventEmitter2 = require('../event-emitter')

// Class to represent network information of an interface
class NetworkInformation extends EventEmitter2{
    constructor(){
        super({
          wildcard: true,
          maxListeners: 0,
          delimiter: '/'
        })

        // flag to indicate if the interface's online
        this.isOnline = false

        // String to indicate the protocol
        this.protocol = null

        // The host to connect to
        this.host = null

        // The host's port
        this.port = null

        // Last-online & offline time (in Unix-time epoch)
        this.lastOffline = null
        this.lastOnline = null
    }

    // Get the current time (in Unix epoch)
    static getCurrentTime(){
        return (new Date).getTime()
    }

    // This method must be invoked when an interface is disconnected from a network
    disconnected(){
        this.isOnline = false
        this.lastOffline = NetworkInformation.getCurrentTime()
        this.emit('changed', this)
    }

    // This method must be invoked when an interface is connected to a network
    // @param host: The host. If it's null, the old value will be used
    // @param port: The host. If it's null, the old value will be used
    // @param protocol: The protocol. If it's null, the old value will be used
    connected(host, port, protocol){
        this.isOnline = true
        this.lastOnline = NetworkInformation.getCurrentTime()
        host && ( this.host = host )
        port && ( this.port = port )
        protocol && ( this.protocol = protocol )
        this.emit('changed', this)
    }
}

module.exports = NetworkInformation

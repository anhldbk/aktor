'use strict'
const _ = require('lodash')

class MessageParser{
  // Parse an input string into a JSON object
  // @param input: the string
  // @return: If the input is parsable, returns the associated JSON object. Otherwise, returns null
  static _parseJson(input){
    try{
      return JSON.parse(input)
    } catch(e){
      // malformed header detected
      return null
    }
  }

  // Parse messages received from our customized broker
  // messages are in format <JSON header>\n<JSON Payload>
  // header = {from, timestamp, ip, port}
  // @param message: The message
  // @return: if success, returns {header, ...payload}. Otherwise, returns null.
  static parse(message){
    if(message == null){
      return null
    }
    message = message.toString()

    var pos = message.indexOf('\n')
    if(pos == -1){ // not a message received from the broker
      return {payload: message}
    }

    var header = MessageParser._parseJson(
      message.substring(0, pos)
    )
    var origin =  MessageParser._parseJson(
      message.substring(pos+1, message.length)
    )
    if((header === null) || (origin == null)){
      // the header is always in JSON format
      // malformed header detected
      return null
    }

    origin.header = _.merge(origin.header, header)
    return origin
  }
}

module.exports = MessageParser

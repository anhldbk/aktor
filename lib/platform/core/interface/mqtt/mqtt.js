'use strict'
const mqtt = require('mqtt')
const InterfaceBase = require('../base')
const util = require('../../../../util')
const MessageParser = require('./message-parser')
const _ = require('lodash')
const async = require('async')

// Class represents an interface to an MQTT broker
// This implementation complies with Aktor Specification v2
class MqttInterface extends InterfaceBase{
    // Constructor
    // @param conf: A configuration object
    //
    // In addition to fields from InterfaceBase, including:
    //  "auto": [Boolean, default is true] If this's set to true, the interface
    //      will auto connect with the provided configuration
    //  "listen": [Object, optional ] A mapping betwween endpoints (mailboxes) & handlers
    //  "changed": [Function, optional] Function for handling network-changed events
    //
    // It can also contain following fields:
    //  "host": The broker's host, default is "127.0.0.1",
    //  "port": The broker's port, default is 1883,
    //  "username": the username required by your broker, if any
    //  "password": the password required by your broker, if any
    // More valid options can be found at: https://github.com/mqttjs/MQTT.js#mqttclientstreambuilder-options
    constructor(conf){
        if(util.isAnyNil(conf, ['username', 'password'])){
            throw new Error('Invalid configuration for MqttInterface')
        }
        var defaultConf = {
            host: '127.0.0.1',
            port: 1883,
            protocol: 'mqtt',
            rejectUnauthorized: false,
            clean: false
        }
        _.merge( defaultConf, conf )
        if( !defaultConf.clientId) {
            defaultConf.clientId = util.getRandomId()
        }
        super( defaultConf )
    }

    // Connect to the network
    // You should override this method in your derived classes
    // @param callback: [Optional] a function `(err, result)` to call when result is ready
    connect(callback){
        if(this._client){
            return callback && callback('status.failure.must_disconnect_first')
        }
        var self = this

        // install the listener first
        var done = function(err){
            if(err){
                return callback && callback(err)
            }
            var listeners = _.get( self._conf, 'listen', null )
            self.listenBundle( listeners, callback )
        }
        self.once( 'status', done)

        self.logDebug('Connecting with configuration = ' + JSON.stringify( self._conf ) + ' ...')
        self._state = MqttInterface.STATE_UNDEFINED // a flag
        self._client = mqtt.connect( self._conf )
        self._client.on('message', self._onMessage.bind(self) )
        self._client.on('connect', self._onOnline.bind(self) )
        self._client.on('offline', self._onOffline.bind(self) )
        self._client.on('error', self._onError.bind(self) )
    }

    // Disconnect from the currently connected network
    // You should override this method in your derived classes
    // @param callback: [Optional] a function `(err, result)` to call when result is ready
    disconnect(callback){
        if( _.isNil(this._client) ){
            return callback && callback()
        }
        var self =  this
        self._client.end(function(){
            self._client = null
            self._disconnected()
            callback && callback()
        })
    }

    _onOnline(){
        var self = this
        self._state = MqttInterface.STATE_ONLINE
        self.logInfo('Connected')

        // update state
        var host = self._conf.host
        var port = self._conf.port
        var protocol = self._conf.protocol
        self._connected( host, port, protocol )

        self.emit('status') // success
    }

    _onError(){
        this.emit('status', 'status.failure.invalid_username_or_password')
    }

    _onOffline(){
        var self = this
        if( self._state === MqttInterface.STATE_OFFLINE ){ // notify only once
            return
        }

        self._state = MqttInterface.STATE_OFFLINE
        self.logError('Disconnected')
        self._disconnected()
        self.emit('status', 'status.failure.offline')
    }

    // Handler for processing incoming Mqtt messages
    // @param endpoint: Topic
    // @param message: Received message
    _onMessage(endpoint, message){
        var self = this
        var msg = message
        message = MessageParser.parse(message)
        if ( !message ) {
            return self.logWarn('Invalid message')
        }
        _.set(message, 'header.interface', this.getName())
        _.set(message, 'header.endpoint', endpoint)

        self.logDebug('Receiving a message from endpoint '+ this._path(endpoint) + '...')
        self.logDebug(message)
        // typing the message
        var type = _.get(message, 'type')
        if( _.isNil(type) ){ // no type provided
            type = 'response'
            if( endpoint.startsWith('event/') || endpoint.startsWith('action/') ){
                type = endpoint
            }
            _.set(message, 'type', type)
        }

        if (type != 'response') {
            return self.emit(type, message)
        }
        var invalidResponse = util.isAnyNil(message, [ 'response', 'header.from', 'request.header.from'])

        if( invalidResponse ){
            // not a valid response
            return self.logWarn( 'Invalid response: ' + JSON.stringify(message) )
        }

        var messageId = _.get(message, 'request.header.id')
        if ( messageId != null ) {
            // emit the response associated with a request via its request Id
            // function to process responses will be `(err, message)`
            // `err` = null if the response can be received in timely manner
            // otherwise, `err` != null
            self.emit(messageId, null, message)
        }
    }

    // Send messages to an endpoint
    // @param endpoint: An endpoint (url, topic, whatever may be...)
    // @param message: [String or Object] A message to send
    // @param options: [Optional] options to send. May be one of following fields:
    //   + qos: <0,1,2>,
    //   + retain: <true, false>
    // @param callback: [Optional] a function `(err, result)` to call when the message's sent
    send(endpoint, message, options, callback){
        if( _.isNil(this._client) ) {
            return callback && callback('status.failure.disconnected_interface')
        }

        if ( _.isObject(message) ){
            message = JSON.stringify(message)
        }
        if ( ! _.isString(message) ){
            throw new Error('Invalid message')
        }
        if( _.isFunction(options) ){
            // send() is invoked as `send(endpoint, message, callback)`
            callback = options
            options = null
        }
        this.logDebug('Sending message to endpoint '+ this._path(endpoint) + '...')
        this.logDebug(message)

        if( _.isNil(options) ){
            options = {}
        }

        options.qos = 1
        this._client.once('packetsend', function(packet){
            callback && callback()
        })

        this._client.publish(endpoint, message, options)
    }

    // Register a handler to process incoming messages on a specific endpoint
    // @param endpoint: [String] The endpoint
    // @param handler: [Function, Nullable] A function `(message)` to process  messages. If there's no handler specified, we only send subscriptions to MQTT brokers. If handlers are null, only subscriptions are sent to servers.
    // @param callback: [Function, optional] A function `(err, result)` to invoke when results are returned
    // NOTE: You should override this function in your derived classess
    // and remember to call `super.listen()` in the end.
    listen(endpoint, handler, callback){
        if( ! _.isString(endpoint) ) {
            // invalid endpoint
            return callback && callback('status.failure.invalid_endpoint')
        }

        if (  _.isNil( this._client) ){
            return callback && callback('status.failure.not_initialized')
        }

        var self = this

        var done = function(err, granted){
            if (err){
                err = 'status.failure.subscription_failed'
            }
            callback && callback(err)
        }
        this.logDebug('Listening for endpoint ' + this._path(endpoint) + '...')

        self._client.subscribe(endpoint, done)

        if( _.isFunction(handler) ){
            // Compared to sending subscriptions, this operation is performed very fast
            // no callback is needed
            super.listen(endpoint, handler)
        }
    }

    // Get endpoints with interface names
    // @param endpoint: An endpoint
    // @return: The named endpoint
    _path(endpoint){
        if(endpoint.indexOf('@') != -1){
            return endpoint
        }
        return endpoint + '@' + this.getName()
    }

    // De-register any handler for a specific endpoint
    // @param endpoint: [String] An endpoint
    // @param callback: [Function] A function `(err, result)` to invoke when results are returned
    // NOTE:
    // You should override this function in your derived classess
    // and remember to call `super.unlisten()` in the end.
    unlisten(endpoint, callback){
        if( ! _.isString(endpoint) ) {
            // invalid endpoint
            return callback && callback('status.failure.invalid_endpoint')
        }

        if (  _.isNil( this._client) ){
            return callback && callback('status.failure.not_initialized')
        }

        var self = this
        var sendUnsubscription = function(cb){
            self._client.unsubscribe(endpoint, function(err){
                cb(err) // we just care about the error
            })
        }
        this.logDebug('Unlistening to endpoint ' + this._path(endpoint) + '...')
        async.waterfall([
            sendUnsubscription,
            async.apply( super.unlisten.bind(self), endpoint )
        ], callback)
    }

    // Dispose the interface
    // @param callback: a function `(err, result)` to call when result is ready
    dispose(callback){
        this.logInfo(`Disposing MqttInterface named ${this._name}...`)
        super.dispose(callback)
    }
}

MqttInterface.STATE_UNDEFINED = 0
MqttInterface.STATE_OFFLINE = 1
MqttInterface.STATE_ONLINE = 2

MqttInterface.path = __filename

module.exports = MqttInterface

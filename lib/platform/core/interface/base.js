'use strict'
const EventEmitter2 = require('../event-emitter')
const NetworkInformation = require('./network-info')
const _ = require('lodash')
const util = require('../../../util')
const Loggable = require('../logger').Loggable
const async = require('async')

// Class represents an interface to an MQTT broker
class InterfaceBase extends Loggable{
    // Constructor
    // @param conf: A configuration object. It can contain following fields:
    //  "listen": [Object, optional ] A mapping betwween endpoints (mailboxes) & handlers.
    //  "changed": [Function, optional] Function for handling network-changed events
    //  "name": [String, optional] Name of the interface. If no name's specified, a random one will be used.
    constructor(conf){
        super(conf)
        this._conf = conf
        this._name = _.get( conf, 'name', util.getRandomId() )
        this._networkInformation = new NetworkInformation()

        // some initialization
        // message handlers
        var endpointHandlers = _.get( this._conf, 'listen', null)
        if( _.isNil( endpointHandlers) ){
            this.listenBundle( endpointHandlers )
        }

        // handler for network-changed events
        var changedHandler = _.get( this._conf, 'changed', null )
        if( _.isFunction(changedHandler) ){
            this.onNetworkChanged( changedHandler )
        }

    }

    // Get the interface's name
    // @return: Name of the interface
    getName(){
        return this._name
    }

    // Connect to the network
    // You should override this method in your derived classes
    // @param callback: a function `(err, result)` to call when result is ready
    connect(callback){
        callback && callback()
    }


    // Disconnect from the currently connected network
    // You should override this method in your derived classes
    // @param callback: a function `(err, result)` to call when result is ready
    disconnect(callback){
        callback && callback()
    }


    // This method's called by derived classes to update its network information when it's connected.
    // @param host: The host. If it's null, the old value will be used
    // @param port: The host. If it's null, the old value will be used
    // @param protocol: The protocol. If it's null, the old value will be used
    _connected(host, port, protocol){
        this._networkInformation.connected(host, port, protocol)
    }

    // This method's called by derived classes to update its network information when it's disconnected
    _disconnected(){
        this._networkInformation.disconnected()
    }

    // Check if the interface is online or not
    // @return: Returns true if the interface is online. Otherwise, returns false.
    isOnline(){
        return this._networkInformation.isOnline
    }

    // Get the network state of the interface
    // @return: the state
    getNetworkInformation(){
        return this._networkInformation
    }

    // Handler to call when the network's changed
    // @param handler: a function `(networkInfo)` to process the change
    onNetworkChanged(handler){
        if( ! _.isFunction( handler ) ) {
            throw new Error('Invalid handler')
        }
        this._networkInformation.on( 'changed', handler )
    }

    // Register a handler to process incoming messages on a specific endpoint
    // @param endpoint: [String] The endpoint
    // @param handler: [Function] A function `(message)` to process  messages. If there's no handler specified, we only send subscriptions to MQTT brokers.
    // @param callback: [Function, optional] A function `(err, result)` to invoke when results are returned
    // NOTE: You should override this function in your derived classess
    // and remember to call `super.listen()` in the end.
    listen(endpoint, handler, callback){
        if( ! this.isValidEndpoint(endpoint) ){
            // invalid endpoint
            return callback && callback('status.failure.invalid_endpoint')
        }
        if( ! _.isFunction( handler ) ) {
            // invalid handler
            return callback && callback('status.failure.invalid_handler')
        }
        this.on( endpoint, handler )
        callback && callback()
    }

    // Register handlers to process incoming messages
    // @param endpointHandlers: A mapping betwween endpoints (mailboxes) & handlers
    // @param callback: [Function] A function `(err, result)` to invoke when results are returned
    listenBundle( endpointHandlers, callback ){
        if( _.isNil(endpointHandlers) ){
            return callback && callback() // nothing to do
        }
        if ( ! _.isObject( endpointHandlers ) ){
            // invalid param
            return callback && callback('status.failure.invalid_param')
        }
        var self = this
        var _listen = function( handler, endpoint, cb ){
            self.listen(endpoint, handler, cb)
        }
        async.forEachOf( endpointHandlers, _listen, callback )
    }

    // De-register any handler for a specific endpoint
    // @param endpoint: [String] An endpoint
    // @param callback: [Function] A function `(err, result)` to invoke when results are returned
    // NOTE:
    // You should override this function in your derived classess
    // and remember to call `super.unlisten()` in the end.
    unlisten(endpoint, callback){
        if( ! this.isValidEndpoint(endpoint) ){
            // invalid endpoint
            return callback && callback('status.failure.invalid_endpoint')
        }
        this.removeAllListeners(endpoint) // remove event listeners
        callback && callback()
    }

    // Check if an endpoint is valid
    // @param endpoint: [String] The endpoint
    // @return: Returns true if the endpoint is valid. Otherwise, returns false.
    isValidEndpoint(endpoint){
        if ( ! _.isString(endpoint) ){
            return false
        }
        var endpoints = [ 'log/debug', 'log/info', 'log/warn', 'log/error', 'log/fatal', 'log/#', 'changed']
        return _.indexOf( endpoints, endpoint ) == -1
    }

    // Send messages to an endpoint
    // @param endpoint: An endpoint (url, topic, whatever may be...)
    // @param message: message to send
    // @param options: [Optional] options to send
    // @param callback: a function `(err, result)` to call when result is ready
    send(endpoint, message, options, callback){
        callback()
    }

    sendSync(endpoint, message, options){
        return util.callsync( this.send, this, [endpoint, message, options] )        
    }

    // Dispose the interface
    // @param callback: a function `(err, result)` to call when result is ready
    dispose(callback){
        this.disconnect(callback)
        super.dispose()
    }
}


module.exports = InterfaceBase

'use strict'
const util = require('../../../util')
const getLogTime = util.getLogTime
const _ = require('lodash')
const LoggerBase = require('./base')

class SimpleLogger extends LoggerBase {
   // Constructor
   // Create a simple logger which logs messages to console
   // @param conf: A configuration object. It can contain following fields:
   //    name: [String, optional] Name of the logger. If now name's provided, a random one will be used
   constructor(conf){
      super(conf)
   }

   log(level, message){
      if ( ! this._isValidLevel(level) ){
         level = 'info'
      }
      if ( ! _.isString(message) ){
         message = JSON.stringify( message )
      }
      var current = getLogTime()
      console.log(util.stringFormat('%s\t%s:%s\t%s', current, this._name, level, message))
   }
}

SimpleLogger.path = __filename

module.exports = SimpleLogger

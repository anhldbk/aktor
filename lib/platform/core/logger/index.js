'use strict'

const Loggable = require('./loggable')
const AdvancedLogger = require('./advanced')
const SimpleLogger = require('./simple')
const LoggerBase = require('./base')

module.exports = {
    Loggable,
    LoggerBase,
    SimpleLogger,
    AdvancedLogger
}

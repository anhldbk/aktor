'use strict'
const _ = require('lodash')
const util = require('../../../util')

// A base class for logger
class LoggerBase {

    // Constructor
    // @param conf: A configuration object. It can contain following fields:
    //    name: [String, optional] Name of the logger. If now name's provided, a random one will be used
    constructor(conf){
        this._conf = conf
        this._name = _.get(conf, 'name', util.getRandomId())
        this.info = this.info.bind(this)
        this.warn = this.warn.bind(this)
        this.error = this.error.bind(this)
        this.debug = this.debug.bind(this)
        this.fatal = this.fatal.bind(this)
        this.log = this.log.bind(this)
    }

    // Get the logger's name
    // @return: The name
    getName(){
        return this._name
    }

    // Log INFO messages
    // @param message: Message to log
    info(message){
        this.log( 'info', message )
    }

    // Log WARN messages
    // @param message: Message to log
    warn(message){
        this.log( 'warn', message )
    }

    // Log ERROR messages
    // @param message: Message to log
    error(message){
        this.log( 'error', message )
    }

    // Log DEUBG messages
    // @param message: Message to log
    debug(message){
        this.log( 'debug', message )
    }

    // Log FATAL messages
    // @param message: Message to log
    fatal(message){
        this.log( 'fatal', message )
    }

    // Dispose the logger
    // You should override this function in your derived classes if you need
    // to release any allocated resources before the logger's terminated
    dispose(){}

    // Check if a log level is valid
    // @param level: The log level
    // @return: Returns true if the level is valid. Otherwise, returns false.
    _isValidLevel( level ){
        if( ! _.isString(level) ){
            return false
        }
        return [ 'info', 'warn', 'error', 'debug', 'fatal' ].indexOf(level) != -1
    }

    // Log messages
    // You MUST override this function in your derived classes
    // @param level: Log level, supposed to be one of followings: info, warn, error, debug, fatal
    // @param message: Message to log
    // @return: Returns nothing
    log(level, message){

    }
}

module.exports = LoggerBase

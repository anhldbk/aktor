'use strict'


const logger = require('fluent-logger')
const util = require('../../../util')
const getLogTime = util.getLogTime
const _ = require('lodash')
const LoggerBase = require('./base')

// An advanced logger based on Fluentd
class AdvancedLogger extends LoggerBase {

   // Constructor
   // Create a simple logger which logs messages to console
   // @param conf: A configuration object. It can contain following fields:
   //    name: [String, optional] Name of the logger. If now name's provided, a random one will be used
   //    host: [String, optional, default is 'localhost'] Fluentd's port
   //    port: [Integer, optional, default is 24224] Fluentd's port
   constructor(conf){
      super(conf)
      var host = _.get(conf, 'host', 'localhost')
      var port = _.get(conf, 'port', 24224)

      // Please make sure Influend is properly configured
      // Have a look at `./docs/installation/fluentd.md`
      logger.configure('mqtt', {
         host, port,
         timeout: 3.0,
         reconnectInterval: 600000 // 10 minutes
      })
      this._client = logger
   }

   log(level, message){
      if ( ! this._isValidLevel(level) ){
         level = 'info'
      }
      if ( ! _.isString(message) ){
         message = JSON.stringify( message )
      }
      level = 'log.' + level
      message = {
         from: this._name,
         message
      }
      this._client.emit( level, message )

      // and may be, we want to log messages to console
      message = JSON.stringify( message )
      var current = getLogTime()
      console.log(util.stringFormat('%s\t%s:%s\t%s', current, this._name, level, message))
   }
}

AdvancedLogger.path = __filename

module.exports = AdvancedLogger

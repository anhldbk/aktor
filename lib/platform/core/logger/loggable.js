'use strict'
const EventEmitter2 = require('../event-emitter')
const _ = require('lodash')
const LoggerBase = require('./base')
const util = require('../../../util')
const fs = require('fs')
const SimpleLogger = require('./simple')

// Loggable class
class Loggable extends EventEmitter2 {

    // Constructor
    // @param conf: A configuration object. It can contain following fields:
    //  'loggers': an array of loggers may have
    //  For example:
    // loggers: [
    //     {
    //         script: 'Absolute path to SimpleLogger',
    //         name
    //     },
    //     {
    //         script: 'Absolute path to AdvancedLogger',
    //         name,
    //         host,
    //         port
    //     },
    //     AnInitializedLogger
    // ]
    constructor(conf){
        super({
          wildcard: true,
          maxListeners: 0,
          delimiter: '/'
        })

        this._conf = conf
        this._loggers = []
        this._disposableLoggers = []

        // if there's any logger configured, we'll add it now
        var loggers = _.get( conf, 'loggers', [] )
        var err
        for(var i = 0; i < loggers.length; i++){
            err = this.addLoggerSync( loggers[i] ) [0]
            if( err ){
                throw new Error(err)
            }
        }

    }

    // Load a NodeJS-script via its path
    // @param script: absolute path to the script
    // @return: Returns null if the script is invalid or there's something wrong. Otherwise, returns the loaded module
    loadScript( script ){
        if( !_.isString(script) ){ // must be a string
            return null
        }
        if ( !fs.existsSync(script) ) {
            script += '.js'
            if ( !fs.existsSync(script) ) {
                return null
            }
        }
        var stats = fs.lstatSync(script)
        if ( stats.isDirectory() ) {
            script = util.pathJoin(script, 'index.js')
            if (!fs.existsSync(script)) {
                return null
            }
        }
        this.logInfo('Loading the script at ' + script + '...')

        try{
            return require(script)
        } catch(e){
            this.logInfo('Can NOT load the script at ' + script)
            return null
        }
    }


    // Add new loggers
    //
    // Usage #1:
    // @param logger: A LoggerBase-derived instance
    // @param callback: [Function, optional] a function `(err, result)` to call when result is ready
    //
    // Usage #2:
    // @param logger: A configuration object, for example:
    // conf = {
    //     script, // [String, required] absolute path to a logger's script
    //     name, // name of the logger, optional. If none is provided, a randome one will be used.
    //     // other key-values
    // }
    // @param callback: [Function, optional] a function `(err, result)` to call when result is ready
    //
    addLogger(logger, callback){
        var disposable = false
        if( _.isObject(logger) && ! (logger instanceof LoggerBase) ){
            // invoked as addLogger(conf, callback)
            var conf = logger
            var script = _.get(conf, 'script')
            var Logger = this.loadScript(script)
            if( _.isNil(Logger) ){
                return callback && callback('status.failure.invalid_script')
            }

            disposable = true // this is a logger owned by this instance of Loggable
            logger = new Logger(conf)
        }

        if( !(logger instanceof LoggerBase) ){
            // throw new Error('Invalid logger. Must be an instance of LoggerBase-derived classes or its definition')
            return callback && callback('status.failure.invalid_logger')
        }

        if(disposable){
            this._disposableLoggers.push(logger)
        }
        this._loggers.push(logger)

        this.onLogWarn( logger.warn )
        this.onLogDebug( logger.debug )
        this.onLogInfo( logger.info )
        this.onLogError( logger.error )
        this.onLogFatal( logger.fatal )

        this.emit( 'logger/added', logger )
        callback && callback( )
    }

    // Add new loggers synchronously
    // @param logger: LoggerBase-derived class or its instances
    // @return: Returns [err, result] which are parameters passed to the callback
    addLoggerSync( logger  ){
        return util.callsync( this.addLogger, this, [logger] )
    }

    // Register handlers for processing logger/added events
    // @param handler: a function `( logger )` to process
    // @return: Returns true on success. Otherwise, returns false.
    onLoggerAdded( handler ){
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( 'logger/added', handler )
        return true
    }

    // Register handlers for processing logs
    // @param level: [Optional] log levels, including 'log/info', 'log/warn', 'log/error', 'log/fatal' or 'log/#'. Default is 'log/#' which will be used to listen for all logs
    // @param handler: a function `( message )` to process  logs
    // @return: Returns true on success. Otherwise, returns false.
    onLog(level, handler){
        if( _.isFunction(level) ){
            // invoked as onLog(handler)
            handler = level
            level = 'log/#'
        }
        if( ! _.isString(level) ){
            // throw new Error('Invalid level')
            return false
        }
        var levels = [ 'log/debug', 'log/info', 'log/warn', 'log/error', 'log/fatal', 'log/#']
        if ( _.indexOf(levels, level) == -1 ){
            // throw new Error('Invalid level')
            return false
        }
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( level, handler )
        return true
    }

    // Register handlers for processing INFO logs
    // @param handler: a function `( message )` to process  logs
    // @return: Returns true on success. Otherwise, returns false.
    onLogInfo(handler){
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( 'log/info', handler )
        return true
    }

    // Register handlers for processing WARN logs
    // @param handler: a function `( message )` to process  logs
    // @return: Returns true on success. Otherwise, returns false.
    onLogWarn(handler){
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( 'log/warn', handler )
        return true
    }

    // Register handlers for processing DEBUG logs
    // @param handler: a function `( message )` to process  logs
    // @return: Returns true on success. Otherwise, returns false.
    onLogDebug(handler){
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( 'log/debug', handler )
        return true
    }

    // Register handlers for processing ERROR logs
    // @param handler: a function `( message )` to process logs
    // @return: Returns true on success. Otherwise, returns false.
    onLogError(handler){
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( 'log/error', handler )
        return true
    }

    // Register handlers for processing FATAL logs
    // @param handler: a function `( message )` to process  logs
    // @return: Returns true on success. Otherwise, returns false.
    onLogFatal(handler){
        if( ! _.isFunction( handler ) ) {
            // throw new Error('Invalid handler')
            return false
        }
        this.on( 'log/fatal', handler )
        return true
    }

    // Log info
    // @param message: [String] message to log
    logInfo(message){
        var level = 'log/info'
        this.emit( level, message )
    }

    // Log warning
    // @param message: [String] message to log
    logWarn(message){
        var level = 'log/warn'
        this.emit( level, message )
    }

    // Log debug
    // @param message: [String] message to log
    logDebug(message){
        var level = 'log/debug'
        this.emit( level, message )
    }

    // Log errors
    // @param message: [String] message to log
    logError(message){
        var level = 'log/error'
        this.emit( level, message )
    }

    // Log fatal errors
    // @param message: [String] message to log
    logFatal(message){
        var level = 'log/fatal'
        this.emit( level, message )
    }

    // Dispose the object, releasing any handlers registered
    // @param callback: a function `(err, result)` to call when result is ready
    dispose(callback){
        this.removeAllListeners('#')
        this.listenerTree = {}

        if( this._disposableLoggers){
            for(var i = 0; i < this._disposableLoggers.length; i++){
                this._disposableLoggers[i].dispose()
            }
            this._disposableLoggers = null
        }

        this._loggers = null
        callback && callback()
    }
}

Loggable.getDefaultLogger = function(name){
    return {
        script: SimpleLogger.path,
        name
    }
}

module.exports = Loggable

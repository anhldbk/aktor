'use strict'
const Loggable = require('../logger').Loggable
const util = require('../../../util')
const _ = require('lodash')
// Abstract class for Storage
class StorageBase extends Loggable{
    // Constructor
    // @param conf: A configuration object. At least, it must contain the following field:
    //  "name": [String, optional] Name of the storage. If no name's specified, a random one will be used.
    constructor(conf){
        super(conf)
        this._conf = conf
        this._name = _.get(conf, 'name', util.getRandomId())
        this._isInitialized = false
        util.Binder.bind(this, StorageBase)
    }

    // Used internally by derived classes
    _initialized(){
        this._isInitialized = true
    }

    // Check if the storage is initialized
    // @return: Returns true if the storage is initialized. Otherwise, returns false.
    isInitialized(){
        return this._isInitialized
    }

    // Get the storage's name
    // @return: Name of the storage
    getName(){
        return this._name
    }

    // Initialize the storage
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    // NOTE: You should override this function in your derived classes and call `_initialized()` at the end.
    init(callback){
        callback && callback()
    }

    // Initialize the storage synchronously
    // @return: Returns [err, result] which are parameters passed to the callback
    initSync(){
        return util.callsync( this.init, this )
    }

    // Set key-value data asynchronously
    // @param key: the key (or the path of the property to set)
    // @param value: The value
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    set(key, value, callback){
        callback && callback()
    }

    // Set key-value data synchronously
    // @param key: the key (or the path of the property to set)
    // @param value: The value
    // @return: Returns [err, result] which are parameters passed to the callback
    setSync(key, value){
        return util.callsync( this.set, this, [key, value] )
    }

    // Get the value associated with a key asynchronously
    // @param key: the key (or the path of the property to get)
    // @param options: [Object, optional] Options
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    get(key, options, callback){
        callback && callback()
    }

    // Get the value associated with a key synchronously
    // @param key: the key (or the path of the property to get)
    // @param options: [Object, optional] Options
    // @return: Returns [err, result] which are parameters passed to the callback
    getSync(key, options){
        return util.callsync( this.get, this, [key, options] )
    }

    // Unset a key asynchronously
    // @param key: the key (or the path of the property to unset)
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    unset(key, callback){
        callback && callback()
    }

    // Set key-value data synchronously
    // @param key: the key (or the path of the property to set)
    // @param value: The value
    // @return: Returns [err, result] which are parameters passed to the callback
    unsetSync(key){
        return util.callsync( this.unset , this, [key] )
    }

    // Destroy the storage asynchronously
    // @param callback: a function `(err, result)` to call when result is ready
    destroy(callback){
        callback && callback()
    }

    // Destroy the storage synchronously
    // @return: Returns true on success. Otherwise, returns false.
    destroySync(){
        return util.callsync( this.destroy, this )
    }

    // Close the storage asynchronously
    // You should override this method in your derived classes.
    // Remember to call this parent method at the end
    // @param callback: a function `(err, result)` to call when result is ready
    dispose(callback){
        super.dispose()
        callback && callback()
    }

    // Close the storage synchronously
    // @return: Returns [err, result] which are parameters passed to the callback
    disposeSync(){
        return util.callsync( this.close, this )
    }

    // Method inspired by Parse
    // Atomically add an object to the array associated with a given key, only if it is not already present in the array. The position of the insert is not guaranteed.
    // @param key: [String] The key
    // @param item: [Object] The object to remove.
    // @param callback: a function `(err, result)` to call when result is ready
    // NOTE: You should override this method in your derived classes (if supported)
    add(key, item, callback){
        throw new Error('Not supported')
    }

    // A synchronous version of `add`
    // @return: Returns [err, result] which are parameters passed to the callback
    addSync(key, item){
        return util.callsync( this.add, this, [key, item] )
    }

    // Method inspired by Parse
    // Atomically remove all instances of an object from the array associated with a given key.
    // @param key: [String] The key
    // @param item: [Object] The object to remove.
    // @param callback: a function `(err, result)` to call when result is ready
    // NOTE: You should override this method in your derived classes (if supported)
    remove(key, item, callback){
        throw new Error('Not supported')
    }

    // A synchronous version of `remove`
    // @return: Returns [err, result] which are parameters passed to the callback
    removeSync(key, item){
        return util.callsync( this.remove, this, [key, item] )
    }

    // Clean a value (if it is an object) from un-serializable fields
    // Currently, this will delete keys associated with functions
    // @param value: A value
    // @return: Returns the clean value
    _clean(value) {
        if (!_.isObject(value)) {
            return value
        }
        var val
        for (var key in value) {
            val = value[key]
            if( _.isNil(val) ){
                _.unset(value, key)
            }
            if (_.isObject(val)) {
                if ( _.isFunction(val) ) {
                    _.unset(value, key)
                } else {
                    this._clean(val);
                }
            }
        }

        _.unset(value, 'interfaces.local.listen')
        return value
    }
}

module.exports = StorageBase

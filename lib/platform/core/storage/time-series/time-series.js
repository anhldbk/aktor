'use strict'
const StorageBase = require('../base')
const util = require('../../../../util')
const getInfluxClient = require('./influx')
const _ = require('lodash')

// Storage for time-series data
class TimeSeriesStorage extends StorageBase{

    // Constructor
    // @param conf: A configuration object
    // Expected fields may be:
    //  "username": [String] username
    //  "password": [String] password
    //  "host": [String, optional, default is 'localhost'] Influx's host,
    //  "port": [integer, optional, default is 8086] Influx's port,
    //  "duration": [String, optional, default is "180d" ] Time to keep data. For more info: please visit: https://docs.influxdata.com/influxdb/v0.13/guides/downsampling_and_retention/
    //  "policyName": [String, optional, default is "rp_mqtt_data"] The policy name
    //  "database": [String, optional, default is "mqtt_data"] The database name
    constructor(conf){
        if( util.isAnyNil( conf, ['username', 'password']) ){
            throw new Error('Must provide username & password to access the storage')
        }
        var defaultConf = {
            host: 'localhost',
            port: 8086,
            duration: '180d', // 6 months
            policyName: 'rp_mqtt_data',
            database:  'mqtt_data'
        }
        _.merge(defaultConf, conf)
        super(defaultConf) // must call super() at the end to auto start (if enable)
    }

    // Initialize the storage
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    init(callback){
        var self = this
        self._client = null
        getInfluxClient.bind( self )( self.conf, function( err, result){
            if(err){
                return callback(err)
            }
            self._client = result
            callback()
        })
    }

    // Close the storage asynchronously
    // @param callback: a function `(err, result)` to call when result is ready
    dispose(callback) {
        this.logInfo(`Disposing TimeSeriesStorage named ${this.getName()} ...`)
        callback && callback()
    }


    // Set key-value data asynchronously
    // @param key: [String] The key (or the path of the property to set)
    // @param value: [Object] The value
    // @param callback: [Function] A function `(err, result)` to call when result is ready
    // @return: nothing
    set(key, value, callback){
        if( ! _.isFunction(callback) ){
            throw new Error('Invalid callback')
        }
        if( ! _.isString(key) ){
            return callback('status.failure.invalid_key')
        }
        value = this._clean(value)
        var id = _.get( this._conf, 'username' )
        var seriesName = util.pathJoin( id, key )
        var db = _.get( this._conf, 'database', 'mqtt_data' )
        var options = { db }

        this._client.writePoint(
          seriesName, {value}, null, options,
          function(err, response){
            callback(err)
          }
        )
    }

    // Get a query for a series data for a specific time range
    // @param series: Name of the series
    // @param start: [Optional] Beginning time
    // @param stop: [Optional] Ending time
    // @return: Returns the query string
    _getRangeQuery(series, start, stop){
      if(!series){
        throw new Error('Invalid series name')
      }

      var query = util.stringFormat('select * from "%s" ', series)

      if(!_.isNil(start)){
        query += util.stringFormat("where time >= %d ", start)
        !_.isNil(stop) && (query += util.stringFormat("and time <= %d ", stop))
      }

      return query
    }

    // Get the value associated with a key asynchronously
    // @param key: [String] the key (or the path of the property to get)
    // @param options: [Object, optional] Options. Following fields are supported:
    //  "start": [Integer, Unix time epoch] Beginning time
    //  "stop": [Integer, Unix time epoch] Ending time
    // @param callback: a function `(err, result)` to call when result is ready.
    //  result will be an array of data points
    // @return: nothing
    get(key, options, callback){
        if( _.isFunction(options) ){
            // invoked as get(key, callback){}
            callback = options
            options = {}
        }
        if( ! _.isFunction(callback) ){
            throw new Error('Invalid callback')
        }
        if ( ! _.isString(key) ){
            return callback('status.failure.invalid_key')
        }
        var id = _.get( this._conf, 'username' )
        var db = _.get( this._conf, 'database', 'mqtt_data' )
        var start = _.get( options, 'start', null )
        var stop = _.get( options, 'stop', null )
        var seriesName = util.pathJoin( id, key )
        var query = this._getRangeQuery( seriesName, start, stop )
        var self = this

        var done = function(err, result){
          if( err ){
              self.logError('Can NOT query data. Reason: ' + err)
              return callback('status.failure.can_not_query')
          }

          if( !result ){ // no data
              return callback(null, [])
          }

          callback(null, result[0])
        }

        this._client.query( db, query, done )
    }

    // Unset a key asynchronously
    // @param key: the key (or the path of the property to unset)
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    unset(key, callback){
        if( ! _.isFunction(callback) ){
            throw new Error('Invalid callback')
        }
        if ( ! _.isString(key) ){
            return callback('status.failure.invalid_key')
        }

        var id = _.get( this._conf, 'username' )
        var db = _.get( this._conf, 'database', 'mqtt_data' )
        var seriesName = util.pathJoin( id, key )
        var query = util.stringFormat('drop series from "%s"', seriesName)
        var self = this

        var done = function(err, result) {
            if (err) {
                self.logError('Can NOT query data. Reason: ' + err)
                err ='status.failure.remove_series'
            }
            callback(err)
        }

        this._client.query( db, query, done )
    }

}

TimeSeriesStorage.path = __filename

module.exports = TimeSeriesStorage

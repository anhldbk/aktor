'use strict'
const async = require('async')
const _ = require('lodash')
const influx = require('influx')

// Get an influx client from a configuration
// @param conf: A configuration object
// @param callback: A function `(err, result)` to process results
// NOTE: Remember to bind a context to this function
function getInfluxClient(conf, callback){
  var self = this
  var client = influx( self._conf )
  var policyName = _.get(conf, 'policyName', 'rp_mqtt_data')
  var database = _.get(conf, 'database', 'mqtt_data')
  var duration = _.get(conf, 'duration', '180d') // 6 months

  // check if database mqtt_data exists
  var checkDb = function(cb){
    self.logInfo('Checking if database ' + database +  ' exists...')
    client.createDatabase(database, function(err, result) {
        if(err){
            self.logError('Something goes wrong. Reason: ' + err)
            return cb('status.failure.check_database')
        }
        if(_.indexOf(result, database) != -1){
            return cb('status.success') // instruct the next function not to execute
        }
        cb(null)
    })
  }

  // check if the retention policy exists
  var checkPolicy = function(cb){
    self.logInfo('Checking if policy ' + policyName +' exists')
    client.getRetentionPolicies(database, function(err,result) {
      if(err){
        self.logError('Something goes wrong. Reason: ' + err)
        return cb('status.failure.check_policy')
      }
      result = _.get(result[0], 'series[0].values')
      result = _.map(result, _.head)
      if(_.indexOf(result, policyName) != -1){
        return cb('status.success') // instruct the next function not to execute
      }
      cb(null)
    })
  }

  var createPolicy = function(cb){
    self.logInfo('Creating policy '+ policyName + '...')
    client.createPolicy(
      policyName, database, duration, 1, true,
      function(err, result){
        if(err){
          self.logError('Something goes wrong. Reason: ' + err)
          return cb('status.failure.create_policy')
        }
        cb()
      }
    )
  }

  try{
    async.waterfall(
      [ checkDb, checkPolicy, createPolicy ],
      function(err, result){
        // there's no error supposed to be here
        callback(null, client)
      }
    )
  } catch(e){
    self.logError('Something goes wrong. Reason: ' + e)
    callback('status.failure.unknown_error')
  }

}

module.exports = getInfluxClient

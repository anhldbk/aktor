'use strict'
const util = require('../../../../util')
const _ = require('lodash')

// Get a query for a series data
// @param series: Name of the series
// @param start: [Optional] Beginning time
// @param stop: [Optional] Ending time
// @return: Returns the query string
function getQuery(series, start, stop){
  if(!series){
    throw new Error('Invalid series name')
  }

  var query = util.stringFormat('select * from "%s" ', series)

  if(!_.isNil(start)){
    query += util.stringFormat("where time >= %d ", start)
    !_.isNil(stop) && (query += util.stringFormat("and time <= %d ", stop))
  }

  return query
}

// Get the time range from an array
// @param time: Array of at most 2 integers
// @return {start, stop}
function getTimeRange(time){
  var start = null
  var stop = null
  // check time range
  if(time){
    // not null and at most with 2 items
    if(!_.isArray(time) || time.length > 2){
      return null
    }
    // all must be integers
    if( _.some(time, (t) => !_.isInteger(t)) ){
      return null
    }
    start = time[0]
    stop = time[1]

    if( start > stop ){
      start = time[1]
      stop = time[0]
    }
  }
  return {start, stop}
}

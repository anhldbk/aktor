'use strict'
const _ = require('lodash')
const StorageBase = require('../base')
const util = require('../../../../util')
const Parse = require('parse/node').Parse
const fs = require('fs')
const File = require('./file')
const async = require('async')

class ParseStorage extends StorageBase {
    // Constructor
    // @param conf: A configuration object
    // It should contain following fields:
    // - username: username ,
    // - password: password
    // - serverURL: [Optional] Url for Parse service, default 'http://localhost:1337/parse',
    // - appId: [Optional] the App ID, default 'aktor-parse-data'
    // - directory: [Optional]  Directory for storing offline files, default '../../../storage'
    constructor(conf) {
        if (util.isAnyNil(conf, ['username', 'password'])) {
            throw new Error('Must provide username & password to access the storage')
        }
        var defaultConf = {
            serverURL: 'http://localhost:1337/parse',
            directory: util.pathJoin(__dirname, '../../../../storage'),
            appId: 'aktor-parse-data'
        }

        _.merge(defaultConf, conf)
        super(defaultConf) // must call super() at the end to auto start (if enable)
    }

    // Initialize the storage
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    init(callback) {
        var self = this
        self.logInfo('Initializing ParseStorage...')
        // initialize offline storage
        var storageFilePath = util.pathJoin(
            self._conf.directory,
            util.getSha256Hash(self._conf.username)
        )
        self._offlineStorage = new File(storageFilePath)
        if (!Parse.applicationId) { // check if it's already initialized
            Parse.initialize(self._conf.appId)
            Parse.serverURL = self._conf.serverURL
        }
        var done = function(err) {
            if (err) {
                return callback(err)
            }
            self._initialized()
            callback()
        }

        async.waterfall(
            [
                self._connect.bind(self),
                self._getStorage.bind(self),
            ],
            done
        )
    }

    // Login with the provided username & password
    // @param callback: a function `(err, result)` to call when result is ready
    _connect(callback) {
        var self = this
        self.logInfo('Connecting with configuration = ' + JSON.stringify(self._conf) + ' ...')
        var username = self._conf.username
        var password = self._conf.password
        Parse.User.logIn(username, password, {
            success: (user) => {
                self._sessionToken = user.getSessionToken()
                self._userId = user.id
                callback && callback()
            },
            error: () => callback('status.failure.invalid_username_or_password')
        })
    }

    // Logout any session if have
    // @param callback: a function `(err, result)` to call when result is ready
    _disconnect(callback) {
        var done = (err) => {
            callback && callback() // dismiss any error
        }
        Parse.User.enableUnsafeCurrentUser()
        Parse.User.become(this._sessionToken).then(
            (success) => {
                Parse.User.logOut().then(
                    done,
                    done
                )
            },
            done
        )
    }

    // Get the storage object. If there's no such object previously defined,
    // we're creating a new one, setting exclusive rights for the current logged-in user
    // You may then access the storage via `this._onlineStorage`
    // @param callback: a function `(err, result)` to call when result is ready
    _getStorage(callback) {
        var self = this
        if (self._offlineStorage.exists()) {
            // oh, we've got an offline storage to restore
            self.logInfo('Restoring from previous offline storage...')
            return self._restoreFromOfflineStorage(callback)
        }

        var AktorStorage = Parse.Object.extend('AktorStorage')
        var query = new Parse.Query(AktorStorage)
        query.equalTo('name', self._conf.username)
        query.first({
            success: (result) => {
                if (_.isNil(result)) {
                    // no data previously defined
                    return self._createNewStorage(callback)
                }
                self._onlineStorage = result // store the result
                callback()
            },
            error: (err) => {
                callback('status.failure.parse_not_available')
            },
            sessionToken: self._sessionToken
        })
    }

    _restoreFromOfflineStorage(callback) {
        var self = this
        self.logInfo('Restoring from offline storage.....')
        this._offlineStorage.read(function(err, result) {
            if (err) {
                return callback(err)
            }
            self._onlineStorage = Parse.Object.fromJSON(result)
            callback()
        })
    }

    // Create a new storage object which is a Parse Object
    // @param callback: a function `(err, result)` to call when result is ready
    _createNewStorage(callback) {
        var self = this
        var AktorStorage = Parse.Object.extend('AktorStorage')
        var aktorStorage = new AktorStorage()

        // we identify each aktor via its name
        aktorStorage.set('name', self._conf.username)

        // Remember to login first
        // Restrict read/write permissions for the current user only
        var permission = new Parse.ACL()
        var userId = this._userId
        permission.setReadAccess(userId, true)
        permission.setWriteAccess(userId, true)
        aktorStorage.setACL(permission)

        aktorStorage.save(null, {
            success: (storage) => {
                self._onlineStorage = storage
                callback()
            },
            error: () => callback('status.failure.can_not_create_storage'),
            sessionToken: self._sessionToken
        })
    }

    // Set key-value data asynchronously
    // @param key: the key (or the path of the property to set). Parse schema is really rigid,
    //  so we're using `lodash` to make using `key` more fun.
    // @param value: The value
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    set(key, value, callback) {
        var self = this
        var data = self._onlineStorage.get('data')
        if (_.isNil(data)) {
            data = {}
        }
        value = self._clean(value)
        _.set(data, key, value)

        if (!self._onlineStorage.set('data', data)) {
            return callback('status.failure.invalid_key_or_value')
        }
        // try to save
        self._save(callback)
    }

    // Try to save the storage to Parse server
    _save(callback) {
        var self = this
        if (this._onlineStorage.dirtyKeys().length == 0) {
            return callback() // no need to save
        }
        this._onlineStorage.save(null, {
            success: (storage) => {
                if (self._notSaved) {
                    self._notSaved = null
                    // clean up the associated file
                    return self._offlineStorage.remove(callback)
                }
                self._onlineStorage = storage // update
                callback()
            },
            error: (storage, error) => {
                // there's something wrong
                self.logWarn(`Can NOT save data for aktor ${self._conf.username}. Use offline storage instead`)
                self._notSaved = true
                self._offlineStorage.write(
                    self._onlineStorage._toFullJSON(),
                    callback
                )
            },
            sessionToken: self._sessionToken
        })
    }

    // Get the value associated with a key asynchronously
    // @param key: [String, optional] the key (or the path of the property to get). If there's no key provided, the entire data will be returned
    // @param options: [Object, optional] Options. In this derived class, no option is supported
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    get(key, options, callback) {
        if( _.isFunction(key) ){
            callback = key
            key = null
        }
        if (_.isFunction(options)) {
            // invoked as get(key, callback){}
            callback = options
            options = {}
        }

        var data = this._onlineStorage.get('data')
        if( !_.isNil(key) ){
            data = _.get(data, key, null)
        }
        callback && callback(null, data)
    }

    // Unset a key asynchronously
    // @param key: the key (or the path of the property to unset)
    // @param callback: a function `(err, result)` to call when result is ready
    // @return: nothing
    unset(key, callback) {
        var data = this._onlineStorage.get('data')
        if (_.isNil(data)) {
            data = {}
        }
        _.unset(data, key)

        if (!this._onlineStorage.set('data', data)) {
            return callback && callback('status.failure.invalid_key_or_value')
        }
        // try to save
        this._save(callback)
    }

    // Destroy the storage asynchronously
    // @param callback: a function `(err, result)` to call when result is ready
    destroy(callback) {
        // just clear field `data`
        if (!this._onlineStorage.set('data', {})) {
            return callback && callback('status.failure.can_not_destroy')
        }
        // try to save
        this._save(callback)
    }

    // Close the storage asynchronously
    // @param callback: a function `(err, result)` to call when result is ready
    dispose(callback) {
        this.logInfo(`Disposing ParseStorage named ${this.getName()} ...`)
        async.waterfall(
            [
                this._disconnect.bind(this),
                super.dispose.bind(this)
            ],
            callback
        )
    }

    // Atomically add an object to the array associated with a given key, only if it is not already present in the array. The position of the insert is not guaranteed.
    // @param key: [String] The key
    // @param item: [Object] The object to remove.
    // @param callback: a function `(err, result)` to call when result is ready
    add(key, item, callback) {
        var data = this._onlineStorage.get('data')
        if (_.isNil(data)) {
            data = {}
        }
        var collection = _.get(data, key)
        if (_.isNil(collection)) {
            _.set(data, key, [])
            collection = _.get(data, key)
        }
        if (_.indexOf(collection, item) != -1) {
            // duplicated item
            return callback && callback() // do nothing
        }
        collection.push(item)

        if (!this._onlineStorage.set('data', data)) {
            return callback('status.failure.invalid_key_or_value')
        }
        // try to save
        this._save(callback)
    }

    // Atomically remove all instances of an object from the array associated with a given key.
    // @param key: [String] The key
    // @param item: [Object] The object to remove.
    // @param callback: a function `(err, result)` to call when result is ready
    remove(key, item, callback) {
        var data = this._onlineStorage.get('data')
        if (_.isNil(data)) {
            data = {}
        }
        var collection = _.get(data, key, [])
        if (!_.isArray(collection)) {
            return callback && callback('status.failure.invalid_key')
        }

        if (_.indexOf(collection, item) == -1) {
            return callback && callback() // no need to do anything
        }
        _.pull(collection, item)

        if (!this._onlineStorage.set('data', data)) {
            return callback('status.failure.invalid_key_or_value')
        }
        // try to save
        this._save(callback)
    }

}

ParseStorage.path = __filename

module.exports = ParseStorage

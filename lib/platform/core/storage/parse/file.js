'use strict'
const fs = require('fs')
const _ = require('lodash')
const util = require('../../../../util')

// Simple class to work with files
class File {
    constructor(filePath){
        this._filePath = filePath
    }

    write(content, callback){
        var self = this

        // auto convert to string
        if( _.isObject(content) ){
            content = JSON.stringify(content)
        }

        fs.writeFile(this._filePath, content, (err) => {
            if(err){
                err = 'status.failure.write_storage_file'
            }
            callback && callback(err)
        });
    }

    remove(callback){
        if( ! fs.existsSync(this._filePath) ) {
            return callback && callback('status.failure.invalid_file_path')
        }

        try {
            fs.unlinkSync(this._filePath)
        } catch (e){
            // dont care about errors
        }

        callback && callback()
    }

    // Check if the file exists
    exists(){
        return fs.existsSync( this._filePath )
    }

    read(callback){
        var readDone = (err, result) => {
            if(err){
                err = 'status.failure.read_storage_file'
                return callback && callback(err)
            }

            var obj = util.parseJson( result ) // auto convert to objects
            if( !_.isNil(obj) ){
                result = obj
            }
            callback && callback(null, result)
        }
        fs.readFile( this._filePath, readDone)
    }
}

module.exports = File

'use strict'

const ParseStorage = require('./parse')
const StorageBase = require('./base')
const TimeSeriesStorage = require('./time-series')

module.exports = {
    ParseStorage,
    StorageBase,
    TimeSeriesStorage
}

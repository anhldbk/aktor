'use strict'
const AktorBase = require('./base')
const util = require('../../../util')
const _ = require('lodash')
const async = require('async')
const Interface = require('../interface')
const MqttInterface = Interface.MqttInterface
const InterfaceBase = Interface.InterfaceBase
const SimpleLogger = require('../logger').SimpleLogger
const DEFAULT_TIMEOUT = 10 * 1000 // 10s
const DEFAULT_TIMEOUT_RATIO = 0.8

class CommonAktor extends AktorBase {
    // Constructor
    // @param conf: A configuration object.
    // A valid configuration may look like:
    // {
    // 	id, // required
    // 	token, // optional
    // 	timeout, // optional, Integer, default is DEFAULT_TIMEOUT, time (in ms) to emit status events
    //  timeoutRatio, // optional, default 0.8. The usage of this parameter can be described in the following scenario: If aktor `A` asks aktor `B` a request with timeout 1000ms. To fullfill the request, aktor `B` have to ask aktor `C` to get the result with timeout of timeoutRatio * 1000ms.
    //     We may then call the quantity of `timeout` * `timeoutRatio` as `forwardTimeout` which can be achieved by function `getForwardTimeout()`
    // 	interfaces : {
    // 		local: {
    // 			script, // Absolute path to a script of an InterfaceBase-derived class
    // 			username, // Optional, will be overriden with `id`
    // 			password, // Optional, if token's provided, this will be overriden with `token`
    // 			host,
    // 			port,
    // 			listen: {
    // 				'action/service/zigbee/data': 'onData' // name of the handler in the Aktor
    // 			}
    // 		}
    // 	},
    //
    // 	storages : {
    // 		local : {
    // 			script, // StorageBase-derived classes
    // 			username, // will be overriden with `id`
    // 			password, // if token's provided, this will be overriden with `token`
    // 			serverURL,
    // 			appId,
    // 			directory
    // 		}
    // 	},
    //
    // 	loggers: [
    // 		 {
    // 			script,
    // 			name, // will be overriden with `id`
    // 			host, // for AdvancedLogger, we must use `host` & `port`
    // 			port
    // 		}
    //
    //  ]
    // }
    constructor(conf) {
        // CommonAktors require configurations to have field `id` & `token`
        if (util.isAnyNil(conf, ['id', 'token'])) {
            throw new Error('Must provide field id and token in configurations')
        }

        if( !_.isString(conf.id) || conf.id.indexOf('.') != -1 ){
            // must be a string and not contain dots
            throw new Error('Invalid ID')
        }

        // by default, CommonAktors have an interface named `local`
        var path = 'interfaces.local'
        if (!_.has(conf, path)) {
            _.set(conf, path, {
                script: MqttInterface.path,
                name: 'local'
            })
        }

        // by default, CommonAktors must listen for messages delivered to primary endpoints
        path = `interfaces.local.listen.${conf.id}`
        if (!_.has(conf, path)) {
            _.set(conf, path, '')
        }

        // by default, CommonAktors use a simple logger if there's no logger configured
        if (!_.has(conf, 'loggers')) {
            _.set(conf, 'loggers', [{
                script: SimpleLogger.path
            }])
        }

        if (!_.has(conf, 'timeout')) {
            _.set(conf, 'timeout', DEFAULT_TIMEOUT)
        }

        if (!_.has(conf, 'timeoutRatio')) {
            _.set(conf, 'timeoutRatio', DEFAULT_TIMEOUT_RATIO)
        }

        super(conf)
        this._started = false
        this._status = 'status.offline'
        this.start = this.start.bind(this)
        this.stop = this.stop.bind(this)

    }

    // Get the forward timeout for a specific message
    // @param message: [Object] A message received to calculate the next forward timeout to `ask`. If not specified, we'll use one provided in this aktor's configuration
    // @return: Returns the forward value (in ms)
    getForwardTimeout(message) {
        var result = 1
        if (!_.isObject(message)) {
            result *= _.get(this._conf, 'timeout', DEFAULT_TIMEOUT)
        } else {
            result *= _.get(message, 'header.timeout', DEFAULT_TIMEOUT)
        }
        result *= _.get(this._conf, 'timeoutRatio', DEFAULT_TIMEOUT_RATIO)
        return result
    }

    getTimeout() {
        return _.get(this._conf, 'timeout', DEFAULT_TIMEOUT)
    }

    // Activate this actor
    // @param callback: a function `(err, result)` to call when result is ready
    start(callback) {
        // if initialized then returns immediately
        if (this._started) {
            return callback && callback('status.failure.already_started')
        }
        this.logInfo('Starting...')
        var self = this
        var done = function(err, result) {
            if (err) {
                return callback && callback(err)
            }
            self._started = true
            callback && callback()
        }

        async.waterfall(
            [
                this._preInit.bind(this),
                this._init.bind(this),
                this._postInit.bind(this)
            ],
            done
        )
    }

    // Initialize the CommonAktor
    // @param callback: a function `(err, result)` to call when result is ready
    // NOTE: This method is called whenever all interfaces, storages & loggers are initialized
    _init(callback) {
        callback && callback()
    }

    // Pre-initialize by adding configured interfaces, storages, loggers...
    // @param callback: a function `(err, result)` to call when result is ready
    _preInit(callback) {
        async.waterfall(
            [
                this._addInterfaces.bind(this), // add interfaces set in the configuration
                this._addStorages.bind(this), // add storages
            ],
            callback
        )
    }

    // Post-initialize
    // Used by class CommonAktor exclusively (do NOT override)
    // @param callback: a function `(err, result)` to call when result is ready
    _postInit(callback) {
        // periodically emit :event/status with status = 'status.online'
        this.logInfo('Started with configuration ' + JSON.stringify(this._conf))
        this._status = 'status.online'
        this._updateStatus(true)

        // messages of `action/stop` are delivered directly to the primary endpoints
        // no need to send subscriptions
        this.getInterface('local').on('action/stop', this.onStop.bind(this))

        callback && callback()
    }

    // Update status by sending messages to 'event/service/world/manifest/<id>'
    _updateStatus() {
        var self = this,
            params = { status : this._status },
            interfaces = this.getInterfaces(),
            endpoint = util.pathJoin('event/service/world/manifest', self.getId()),
            iface,
            options = {}

        // loop through all interfaces
        self.logInfo('Manifesting with status = ' + this._status)

        for (var i = 0; i < interfaces.length; i++) {
            iface = interfaces[i].getName()
            this.tell(endpoint + '@' + iface, {
                params
            }, options)
        }
    }

    // Override the parent's implementation
    addInterface(interfake, callback) {
        var self = this
        if (interfake instanceof InterfaceBase) {
            return super.addInterface(interfake, callback)
        }

        // invoked as addInterface(conf, callback)
        // then `interfake` MUST be a configuration object
        if (!_.isObject(interfake)) {
            return callback && callback('status.failure.invalid_configuration')
        }

        var token = _.get(this._conf, 'token', null)
        var conf = interfake

        // inspect configurations
        if (_.isNil(conf.name)) { // if no name is specified
            // we'll use a random one
            _.set(conf, 'name', util.getRandomId())
        }

        if (_.isNil(conf.username)) { // no username's provided, we'll use the id
            _.set(conf, 'username', self.getId()) // will be overriden with `id`
                // if token's provided, this will be overriden with `token`
        }

        if (_.isNil(conf.password)) { // if there's no password provided, we'll use the token
            if (!_.isNil(token)) {
                _.set(conf, 'password', token)
            }
        }

        if( _.isNil(conf.will) ){  // set the last will
            _.set(conf, 'will', {
                topic: util.pathJoin('event/service/world/manifest', self.getId()),
                payload: JSON.stringify({status: 'status.offline'})
            })
        }

        // validate listen endpoints
        var listen = _.get(conf, 'listen', {})
        // silently listen for primary endpoints
        var endpoint = this.getId()
        if (!_.has(listen, endpoint)) {
            listen[endpoint] = ''
        }

        super.addInterface(conf, callback)
    }

    // Add interfaces configured
    // @param callback: a function `(err, result)` to call when result is ready
    _addInterfaces(callback) {
        var interfaces = _.get(this._conf, 'interfaces', null)
        var token = _.get(this._conf, 'token', null)
        if (_.isNil(interfaces)) {
            return callback && callback()
        }

        var self = this
        var _addInterface = function(conf, name, cb) {
            _.set(conf, 'name', name)
            var done = function(err, result) {
                if (!err) {
                    return cb && cb()
                }

                var message = util.stringFormat(
                    'Failed to add interface named %s. \nReason: %s\nConfiguration: %s',
                    name, err, JSON.stringify(conf)
                )
                self.logError(message)
                cb && cb(err)
            }

            self.addInterface(conf, done)
        }

        async.forEachOf(interfaces, _addInterface, callback)
    }

    // Add storages configured
    // @param callback: a function `(err, result)` to call when result is ready
    _addStorages(callback) {
        var storages = _.get(this._conf, 'storages', [])
        var token = _.get(this._conf, 'token', null)
        var names = _.keys(storages)
        var self = this

        var _addStorage = function(name, cb) {
            var conf = storages[name]
            if (!_.isObject(conf)) {
                self.logError('Invalid configuration for storage named ' + name)
                return cb && cb('status.failure.invalid_configuration')
            }

            if (_.isNil(conf.username)) {
                _.set(conf, 'username', self.getId()) // will be overriden with `id`
            }

            if (_.isNil(conf.password)) {
                // if token's provided, this will be overriden with `token`
                if (_.isNil(token)) {
                    return cb && cb('status.failure.no_password')
                }
                _.set(conf, 'password', token)
            }

            var done = function(err, result) {
                if (err) {
                    conf = JSON.stringify(conf)
                    var message = `Failed to add storage named ${name}. \nReason: ${err}\nConfiguration: ${conf}`
                    self.logError(message)
                } else {
                    self.logInfo(`Successfully added storage named ${name}`)
                }
                cb && cb(err)
            }

            self.addStorage(conf, done)
        }
        self.logInfo(`Adding ${names.length} storages...`)
        async.each(names, _addStorage, callback)
    }

    // Publish message to an endpoint, asking for a response
    //
    // @param endpoint: [String] An endpoint. See the documentation at `_parseEndpoint()`
    // @param message: [optional] a key-value object, default {}
    // @param options: [optional] an object of 4 optional fields
    //    qos: <0,1,2>,
    //    retain: <true, false>
    //    timeout: <int, time out to wait for response, in ms>, default is DEFAULT_TIMEOUT
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    //
    // NOTE: if you publish messages to an unauthorized topic, there's no way
    // for us to check if the messages are published
    ask(endpoint, message, options, callback) {
        var validation = this._parseEndpoint(endpoint)
        if (_.isNil(validation)) {
            return this._safeCallback(callback, 'status.failure.invalid_endpoint')
        }
        endpoint = validation[0]
        var iface = validation[1]

        if (_.isFunction(message)) {
            // invoked as ask('action/anything', function(err, msg){})
            callback = message
            options = {}
            message = {}
        }

        if (_.isFunction(options)) {
            // invoked as ask('action/anything', {data: 3}, function(err, msg){})
            callback = options
            options = {}
        }

        if (!_.isObject(message)) {
            message = {} // by default
        }

        if (_.isNil(callback)) {
            // no callback for processing responses may have
            return iface.sendSync(endpoint, message, options)
        }

        // the header will be overriden with a generated ID
        var timeout = _.get(options, 'timeout', DEFAULT_TIMEOUT) // 1 min timeout
        var id = util.getRandomId()
        message.header = {
            id,
            endpoint,
            timeout
        }
        var self = this

        // timeout function to remove the listener below
        var _callback = function(err, message) {
            self.removeAllListeners(id)
            _.unset(self.listenerTree, id) // remove the trailing key

            // if there's no physical error
            // we silently check for any logical error
            if (_.isNil(err)) {
                var status = _.get(message, 'response.status', null)
                    // silently set the new error
                if (status !== 'status.success') {
                    err = status
                }
            }
            callback && callback(err, message)
        }

        // InterfaceBase-derived instances will emit the response with the id
        iface.once(id, util.callout(_callback, timeout))

        return iface.sendSync(endpoint, message, options)
    }

    // Invoke callbacks in a safe manner
    // @param callback: [Optional, Function] a callback function of `(err, result)`
    // @param err: [String] An error
    // NOTE: If no callback is specified, we will throw Exceptions associated with `err`
    _safeCallback(callback, err) {
        if (_.isFunction(callback)) {
            return callback(err)
        }

        if (err) {
            throw new Error(err)
        }
    }

    // Publish message to a endpoint without waiting for a response
    //
    // @param endpoint: [String] A endpoint which is described by a URI.
    // @param message: a key-value object
    // @param options: [optional] an object of 3 optinal fields
    //    qos: <0,1,2>,
    //    retain: <true, false>
    // NOTE: if you publish messages to an unauthorized topic, there's no way
    // for us to check if the messages are published
    tell(endpoint, message, options) {
        this.ask(endpoint, message, options)
    }

    // Register handlers for processing messages
    //
    // @param endpoint: [String] A endpoint which is described by a URI.
    // @param handler: [Function, or String, Nullable] The handler. If this handler is a string, then it must be
    //  the path to a member function of `this`. For example: handler = `api.onGet`, `onGet`...
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    // NOTE: if you publish messages to an unauthorized topic, there's no way
    // for us to check if the messages are published
    listen(endpoint, handler, callback) {
        var self = this
        var validation = self._parseEndpoint(endpoint)
        if (_.isNil(validation)) {
            // invalid endpoint
            return callback && callback('status.failure.invalid_endpoint')
        }

        if (_.isString(handler)) {
            handler = _.get(this, handler, null)
            if (_.isFunction(handler)) {
                handler = handler.bind(this) // auto bind the context
            }
        }

        if (!_.isNil(handler) && !_.isFunction(handler)) {
            return callback && callback('status.failure.invalid_handler')
        }

        endpoint = validation[0]
        var iface = validation[1]
        iface.listen(endpoint, handler, callback)
    }

    // Remove any listening registration for a endpoint
    // @param endpoint: Endpoint to unlisten
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    // @return: Returns true if success. Otherwise, returns false.
    unlisten(endpoint, callback) {
        var self = this
        var validation = self._parseEndpoint(endpoint)
        if (_.isNil(validation)) {
            // invalid endpoint
            return callback && callback('status.failure.invalid_endpoint')
        }

        endpoint = validation[0]
        var iface = validation[1]
        iface.unlisten(endpoint, callback)
    }

    // Parse endpoints
    // An endpoint used by CommonAktor has the schema of:
    // <endpoint>[@<interface>], in which:
    // + <endpoint>: The endpoint
    // + <interface>: [Optional] name of the interface associated with the endpoint.
    // For example:
    // + action/service/zigbee/add_device
    // + action/service/auth/login@cloud
    // @return: Returns the interface if have any. Otherwise, if there's no interface
    //  specified, the default one will be used
    // @return: An array of [endpoint, interface]. If there's something wrong, returns null
    _parseEndpoint(endpoint) {
        if (!_.isString(endpoint)) {
            // throw new Error('Invalid endpoint')
            return null
        }
        var parts = endpoint.split('@')
        if (parts.length > 2) {
            // an endpoint may have at most 2 parts
            // throw new Error('Invalid endpoint')
            return null
        }
        endpoint = parts[0]

        var interfake = this.getDefaultInterfaceName() // it may be null
        if (parts.length != 1) {
            // no interface specifed
            interfake = parts[1]
        }

        interfake = this.getInterface(interfake)
        if (_.isNil(interfake)) {
            this.logWarn('No interface registered')
            return null
        }
        return [endpoint, interfake]
    }

    // Reply to a request
    // request: The original request
    // response: your response
    reply(request, response) {
        var id = _.get(request, 'id')
        if (_.isNil(id)) {
            id = _.get(request, 'header.id')
        }
        if (_.isNil(id)) {
            return // no need to reply, coz there's no callback awaiting
        }
        var from = _.get(request, 'header.from', null)
        if (from == null) {
            // the request is INVALID
            return
        }
        var iface = _.get(request, 'header.interface', null)
        if (iface == null) {
            iface = this.getDefaultInterfaceName()
        }

        this.tell(
            from + '@' + iface, {
                request,
                response
            }
        )
    }

    replyUnauthorized(request) {
        this.reply(request, {
            status: 'status.failure.unauthorized'
        })
    }

    replyInvalidParams(request) {
        this.reply(request, {
            status: 'status.failure.invalid_params'
        })
    }

    replyInvalidId(request) {
        this.reply(request, {
            status: 'status.failure.invalid_id'
        })
    }

    replySuccess(request, response) {
        response = _.merge({
                status: 'status.success'
            },
            response
        )
        this.reply(request, response)
    }

    replyFailure(request, failure) {
        this.reply(request, {
            status: failure
        })
    }

    // Check if this aktor is started
    // @return: Returns true if the aktor's started. Otherwise, returns false.
    isStarted() {
        return this._started
    }

    // Deactivate this actor
    // @param callback: a function `(err, result)` to call when result is ready
    stop(callback) {
        if (!this._started) {
            return callback && callback('status.failure.not_started')
        }
        this.logInfo('Stopping...')
        if (this._status === 'status.online') {
            // you can set `_status` or it will set to `status.offline` by default
            this._status = 'status.offline'
        }
        this._updateStatus() // emit the last status
        this._started = false

        // call the callback first so we can dispose all the interfaces latter on.
        callback && callback()
        super.stop()
    }

    // Handler to process `action/stop`
    // @param message: MQTT messages received from brokers
    // NOTE: You should not override this function. Override `stop()` and `_isAuthorized()` instead.
    onStop(message) {
        if (!this._isAuthorized(message)) {
            return this.replyUnauthorized(message)
        }

        var self = this
        this.stop( (err) => {
            if(err){
                return self.replyFailure(message, err)
            }
            self.replySuccess(message)
        })
    }

    // Check if a message is authorized (that means: it's from `service/world`, its parent or itself)
    // @return Returns true if it's authorized. Otherwise, returns false.
    _isAuthorized(message) {
        var from = _.get( message, 'header.from')
        if( !_.isString(from) ){
            return false
        }

        if( from.startsWith('service/container')){
            return true
        }

        var parent  = _.get( this._conf, '_parent', null)
        var allowed = [ parent, 'service/world', this.getId() ]
        return allowed.indexOf( from ) != -1
    }
}

module.exports = CommonAktor

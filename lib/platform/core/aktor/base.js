'use strict'
const _ = require('lodash')
const Logger = require('../logger')
const Loggable = Logger.Loggable
const LoggerBase = Logger.LoggerBase
const util = require('../../../util')
const InterfaceBase = require('../interface').InterfaceBase
const StorageBase = require('../storage').StorageBase
const async = require('async')

class AktorBase extends Loggable {
    // Constructor
    // @param conf: A configuration object.  It can contain following fields:
    //  "id": [String, optional] Id of the aktor. If no id is specified, a
    //      random one will be generated
    // "loggers": [Array] Array of loggers configured. For more information about this field,
    //      see documention of Loggable's constructor
    constructor(conf) {
        if (!_.isObject(conf)) {
            throw new Error('Invalid configuration')
        }

        if (_.isNil(conf.id)) {
            conf.id = util.getRandomId()
        }

        // update loggers' names if needed
        var loggers = _.get(conf, 'loggers', [])
        for (var i = 0; i < loggers.length; i++) {
            if (!loggers[i].name) {
                loggers[i].name = conf.id
            }
        }

        super(conf)
        this._id = conf.id

        // Aktor has named interfaces & storages
        this._storages = {}
        this._interfaces = {}

        this._defaultInterfaceName = null
        this._defaultStorageName = null

        this.onLoggerAdded(this._loggerAdded) // we may not care about parameters passed
    }

    // Override the function from Loggable to provide a default name to the configuration
    addLogger(logger, callback) {
        if (_.isObject(logger) && !(logger instanceof LoggerBase)) {
            // invoked as addLogger(conf, callback)
            // add names if needed
            if (!logger.name) {
                logger.name = this.getId()
            }
        }

        super.addLogger(logger, callback)
    }

    _loggerAdded(logger) {
        var interfaces = _.values(this._interfaces)
        var storages = _.values(this._storages)
        var err
        for (var i = 0; i < interfaces.length; i++) {
            err = interfaces[i].addLoggerSync(logger)[0]
            if (err) {
                this.logError('Can NOT update loggers for interface named ' + interfaces[i].getName())
            }
        }
        for (var i = 0; i < storages.length; i++) {
            err = storages[i].addLoggerSync(logger)[0]
            if (err) {
                this.logError('Can NOT update loggers for storage named ' + storages[i].getName())
            }
        }
    }

    // Set the default interface
    // @param name: Name of the existing interface to set default
    // @return: Returns true if success. Otherwise, returns false.
    // NOTE:
    // 1. If an aktor has only one interface, the interface will be marked as default automatically
    // 2. If an aktor has multiple interfaces, the first one will be the default one automatically
    setDefaultInterface(name) {
        if (this.interfaces[name]) {
            this._defaultInterfaceName = name
            return true
        }
        return false
    }

    // Get the default interface (which is an instance of an InterfaceBase-derived class)
    // @return: Returns the default interface if have any. Otherwise, returns null
    getDefaultInterfaceName() {
        return this._defaultInterfaceName
    }

    // Add new interface for aktors
    //
    // Usage #1:
    // @param interfake: An InterfaceBase-derived instance
    // @param callback: [Function, optional] a function `(err, result)` to call when result is ready
    //
    // Usage #2:
    // @param interfake: [Object] Configuration object with following fields:
    // {
    //     script, // absolute path to an interface's script
    //     name, // [Required] the interface name. It must not contain characters '@'
    //     ... // other fields
    // }
    // @param callback: [Function, optional] a function `(err, result)` to call when result is ready
    //
    // NOTE:
    // 1. The `interfake` instance will be instructed to connect with the configuration
    // 2. Handlers in `listen` MUST be paths to functions. So we can serialize/deserialize it.
    // 3. Example usage:
    // addInterface(
    //     {
    //         script: MqttInterface.path,
    //         name: 'local',
    //         username: 'ping',
    //         password: '123456',
    //         auto: false,
    //         listen: {
    //             'action/ping/get': '', // null handler
    //             'action/ping/post': 'onPost' //--> will be resolved to `this.onPost`
    //         },
    //         will : {
    //             topic,
    //             payload
    //         }
    //     },
    //     callback
    // )
    addInterface(interfake, callback) {
        if( _.isObject(interfake) && ! (interfake instanceof LoggerBase) ){
            // invoked by usage #2 addInterface(conf, callback)
            var conf = interfake
            var result = this._createInterface( conf )
            if( result[0] ){ // err
                return callback && callback( result[0] )
            }
            interfake = result[1]
        }

        if (!(interfake instanceof InterfaceBase)) {
            // throw new Error('Invalid interface. Must be an instance of InterfaceBase-derived classes or its definition')
            return callback('status.failure.invalid_interface')
        }

        var name = interfake.getName()
        this.logInfo('Adding a new interface named ' + name + ' ...')
        this._interfaces[name] = interfake

        // Mark the first interface as the default one
        if (_.isNil(this._defaultInterfaceName)) {
            this._defaultInterfaceName = name
        }

        if( interfake.isOnline() ){
            return callback && callback()
        }
        interfake.connect(callback) // auto connect
    }

    // Create an interface based on a configuration
    // For more information about the configuration, see function `addInterface()`
    // @param conf: [Object] Configuration
    // @return: A pair of [err, result]. If everything is ok, `err` = null, `result` = instance of the Interface created
    //      Otherwise, `err` is not null and contains error information
    // NOTE:
    // 1. (side effect) `conf` may be changed
    // 2. You have to call method `connect` of the created instance after invoking this method
    _createInterface(conf){
        if( !_.isObject(conf) ){
            return ['status.failure.invalid_configuration', null]
        }
        var name = _.get(conf, 'name')
        if (!_.isString(name) || name.indexOf('@') != -1) {
            // must be a string, not contain characters '@'
            return [ 'status.failure.invalid_name', null ]
        }
        if( !_.isNil(this._interfaces[name]) ){
            // must not be registered before
            return [ 'status.failure.duplicate_interface_name', null ]
        }

        var script = _.get(conf, 'script')
        var Interface = this.loadScript(script)
        if( _.isNil(Interface) ){
            return [ 'status.failure.invalid_script', null ]
        }

        // clone the configuration for Aktor's loggers
        _.set(conf, 'loggers', this._loggers)

        var defaultListen = {}
        _.set(defaultListen, this.getId(), '')

        // resolve listeners
        var listen = null, _listen = null
        if( _.isNil(conf._listen) ){
            listen = _.get(conf, 'listen', defaultListen)
            _listen = _.cloneDeep( listen )
        } else { // used when descrialization
            listen = _.cloneDeep( conf._listen )
        }

        var keys = _.keys(listen), key
        var handler = null
        for (var i = 0; i < keys.length; i++) {
            key = keys[i]

            handler = listen[key]
            if ( !_.isString(handler) ) {
                // resolve the handler by its name
                return [ 'status.failure.invalid_listen_handler', null ]
            }

            if( handler.length == 0 ){ // a null handler
                listen[key] = null
                continue
            }
            handler = _.get(this, handler)
            if ( !_.isFunction(handler) ) {
                return [ 'status.failure.invalid_listen_handler', null ]
            }
            listen[key] = handler.bind(this) // autobind the context
        }
        _.set(conf, 'listen', listen)


        if( !_.isNil(_listen) ){
            _.set(conf, '_listen', _listen) // secretly use this field for serialization/deserialization
        }

        try{
            return [null, new Interface(conf)]
        } catch(e){
            return ['status.failure.load_interface', null]
        }
    }

    // Get the interface described by a name
    // @param name: Name of the interface
    // @return The interface if have. Otherwise, if there's no such name, returns null
    getInterface(name) {
        return _.get(this._interfaces, name, null)
    }

    // Get all interfaces associated with this aktor
    // @return List of interfaces
    getInterfaces(){
        return  _.values(this._interfaces)
    }

    // Add new interface for aktors
    //
    // Usage #1:
    // @param storage: StorageBase-derived class or its instances
    // @param callback: a function `(err, result)` to call when result is ready
    //
    // Usage #2:
    // @param storage: [Object] Configuration object with following fields:
    // {
    // 	   script, // [String, required] Absolute path to a script of an StorageBase-derived class
    //     name,  // [String, optional] Name of the storage. If no name's specified, a random one will be used.
    //     ... // other fields
    // }
    // @param callback: [Function, optional] a function `(err, result)` to call when result is ready
    addStorage( storage, callback ) {
        var conf = null
        if( _.isObject(storage) && ! (storage instanceof StorageBase) ){
            // invoked by usage #2 addStorage(conf, callback)
            conf = storage

            if( !_.has(conf, 'name') ){
                _.set(conf, 'name', util.getRandomId() )
            }
            var name = _.get(conf, 'name')
            if (!_.isString(name) || !_.isNil(this._storages[name])) {
                return callback && callback('status.failure.invalid_name')
            }

            this.logInfo(`Adding a new storage named ${name}...`)

            var script = _.get(conf, 'script')
            var Storage = this.loadScript(script)
            if( _.isNil(Storage) ){
                return callback && callback('status.failure.invalid_script')
            }

            // clone the configuration for loggers of Aktor
            var loggers = _.get(this._conf, 'loggers', null)
            if (loggers) {
                _.set(conf, 'loggers', loggers)
            }

            storage = new Storage(conf)
        }

        if ( !(storage instanceof StorageBase) ) {
            return callback && callback('status.failure.invalid_storage')
        }

        this._storages[ storage.getName() ] = storage
        if( storage.isInitialized() ){
            return callback && callback()
        }

        storage.init(callback) // auto initialize
    }

    // Get the storage described by a name
    // @param name: Name of the storage
    // @return The storage if have. Otherwise, if there's no such name, returns null
    getStorage(name) {
        return _.get(this._storages, name, null)
    }


    // Activate this actor
    // @param callback: a function `(err, result)` to call when result is ready
    start(callback) {
        callback && callback()
    }

    // Restart the actor
    // @param callback: a function `(err, result)` to call when result is ready
    restart(callback) {
        callback && callback()
    }

    // Deactivate this actor
    // @param callback: a function `(err, result)` to call when result is ready
    stop(callback) {
        var self = this
        var disposable = []
        
        // Pay attention to the order: storages must be disposed first
        disposable = _.concat(disposable, _.values(self._storages))
        disposable = _.concat(disposable, _.values(self._interfaces))
        disposable = _.concat(disposable, self)

        var dispose = function(resource, cb) {
            if( _.isNil(resource.dispose) ){
                return cb()
            }
            resource.dispose(cb)
        }

        async.each(disposable, dispose, callback)
    }

    // Publish message to an endpoint, asking for a response
    //
    // @param endpoint: [String] an endpoint which is described by a URI.
    // @param message: [optional] a key-value object, default {}
    // @param options: An option which specified by derived classes
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    //
    // NOTE: if you publish messages to an unauthorized topic, there's no way
    // for us to check if the messages are published
    ask(endpoint, message, options, callback) {
        callback && callback()
    }

    // Publish message to a endpoint without waiting for a response
    //
    // @param endpoint: [String] A endpoint which is described by a URI.
    // @param message: a key-value object
    // @param options: An option which specified by derived classes
    // NOTE: if you publish messages to an unauthorized topic, there's no way
    // for us to check if the messages are published
    tell(endpoint, message, options) {
        this.ask(endpoint, message, options)
    }

    // Register a pair of endpoint & handler for processing messages
    //
    // @param endpoint: [String] A endpoint which is described by a URI.
    // @param handler: [Function] Function `(message)` to call if there's any message to come.
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    // NOTE: if you publish messages to an unauthorized topic, there's no way
    // for us to check if the messages are published
    listen(endpoint, handler, callback) {
        callback && callback()
    }

    // Register multiple pairs of endpoints and handlers for processing messages
    //
    // @param endpointHandlers: [Object] A mapping between endpoints and handlers
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    listenBundle( endpointHandlers, callback){
        if( !_.isObject(endpointHandlers) ){
            return callback && callback('status.failure.invalid_endpoint_handler')
        }
        var self = this
        var listen = function(value, key, cb){
            self.listen(key, value, cb)
        }
        async.forEachOf( endpointHandlers, listen, callback )
    }

    // Register handlers for processing messages once
    //
    // Be sure to have the endpoint configured with an appropriate ACL.
    // @param endpoint: [String] A endpoint which is described by a URI.
    // @param handler: [Function] Function `(message)` to call if there's any message to come. If time runs out, `message = null`
    // @param timeout: [Integer, optional] Time to wait in ms
    // @param callback: [Function, Optional] Callback to call when there's a response from server about the subscription `function(err, message)`
    listenOnce(endpoint, handler, timeout, callback) {
        var self = this
        const defaultTimeout = _.get( this._conf, 'timeout', 30000) // 30 seconds
        if ( _.isFunction(timeout) ) {
            // invoked as `listenOnce(endpoint, handler, callback)`
            callback = timeout
            timeout = defaultTimeout
        }
        if ( !_.isInteger(timeout) ) {
            timeout = defaultTimeout
        }
        if( timeout < 0 ){
            timeout = defaultTimeout
        }

        var _handler = function(message, result) {
            if (message === 'status.failure.timeout') {
                message = null
            }

            handler && handler(message)
            self.unlisten(endpoint) // dont care about callback functins
        }
        _handler = util.callout(_handler, timeout)

        self.listen(endpoint, _handler, callback)
    }

    // Remove any listening registration for a endpoint
    // @param endpoint: Endpoint to unlisten
    // @param callback: [Function, optional] function(err, message) to process responses (if any)
    // NOTE: You should override this function in your derived classes
    unlisten(endpoint, callback) {
        callback && callback()
    }

    // Get the actor's id
    // @return The Id
    getId() {
        return this._id
    }

    // Get reference to other aktor
    // @param actor: [String, AktorBase-derived classes] the aktor to reference
    // @return An instance of AktorRef
    getRef(actor) {
        var actorId = actor
        if (actor instanceof AktorBase) {
            actorId = actor.getId()
        }
        if (!_.isString(actorId)) {
            throw new Error('Invalid actor')
        }

        return new AktorRef(actorId, this)
    }
}

// Class for interact with other aktor
class AktorRef {
    // constructor
    // @param to: [String or AktorBase-derived classes] aktor to interact with. It may be a string or an instance of Aktor
    // @param from: [AktorBase-derived classes] Aktor who wants to interact with the aktor above
    constructor(to, from) {
        if (to instanceof AktorBase) {
            to = this.getId()
        }
        var valid = from instanceof AktorBase
        valid &= _.isString(to)
        if (!valid) {
            throw 'Invalid params'
        }
        this._from = from
        this._to = to
    }

    // the same as request()
    // just another name to make it looks like ...Akka
    ask(endpoint, message, options, callback) {
        this._from.ask(endpoint, message, options, callback)
    }

    // request without wait for any response
    tell(endpoint, message, options) {
        this.ask(endpoint, message, options)
    }

    listenOnce(endpoint, handler, timeout, callback) {
        this._from.listenOnce(endpoint, handler, timeout, callback)
    }

    listen(endpoint, handler, callback) {
        if (endpoint.startsWith(':')) {
            endpoint = path.join(this._to, endpoint)
        }
        this._from.listen(endpoint, handler, callback)
    }
}

module.exports = AktorBase

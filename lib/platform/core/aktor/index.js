'use strict'

const AktorBase = require('./base')
const CommonAktor = require('./common')

module.exports = {
    AktorBase,
    CommonAktor
}

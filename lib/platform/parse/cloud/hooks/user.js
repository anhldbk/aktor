'use strict'

// This hook is used to prohibit external users from directly sign up
Parse.Cloud.beforeSave(Parse.User, function(request, response) {
    if (request.master) {
        Parse.Cloud.useMasterKey()
    } else {
        // Need master key
        return response.error('Unauthorized')
    }

    if (!request.object.existed()) {

        var acl = new Parse.ACL();

        acl.setPublicReadAccess(false);

        acl.setPublicWriteAccess(false);

        request.object.setACL(acl);

    }

    response.success()
})

// Set appropriate ACLs for new users
// Parse.Cloud.afterSave(Parse.User, function(req) {
//   if (!req.object.existed()) {
//     var user = req.object
//     if ( ( user === null ) || ( user === undefined) ){
//       return
//     }
//     var acl = new Parse.ACL(user)
//     acl.setPublicReadAccess(false)
//     acl.setPublicWriteAccess(false)
//     user.setACL(acl)
//     user.save()
//   }
// })

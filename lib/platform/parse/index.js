const path = require('path')
const express = require('express')
const Parse = require('parse/node').Parse
const ParseServer = require('parse-server').ParseServer
const parseConf = require('../conf').parse
const util = require('../util')

// import ParseDashboard from 'parse-dashboard'

const SERVER_PORT = process.env.PORT || parseConf.port
const SERVER_HOST = process.env.HOST || parseConf.host
const SERVER_URL = process.env.SERVER_URL || parseConf.serverURL
const APP_ID = process.env.APP_ID || parseConf.appId
const MASTER_KEY = process.env.MASTER_KEY || parseConf.masterKey
const FILE_KEY = process.env.FILE_KEY || parseConf.fileKey
const DATABASE_URI = process.env.DATABASE_URI || parseConf.databaseURI

// Initialize Parse
// @returns: Returns true if Parse server is ready. Otherwise, returns false.
function initialize() {
    if (!Parse.applicationId) { // check if it's already initialized
        Parse.initialize(APP_ID)
        Parse.serverURL = SERVER_URL
        Parse.masterKey = MASTER_KEY
        Parse.Cloud.useMasterKey()
    }

    return util.isParseReady()
}

// Start ParseServer
// @param callback: [Function, optional] function `(err, result)` to process results
function start(callback) {
    if (initialize()) {
        console.log('Fatal: May be another instance of ParseServer is running at port ' + SERVER_PORT)
        return callback && callback('status.failure.already_running')
    }

    if (!util.isMongoDbReady()) {
        console.log('Fatal: Mongodb is not ready')
        return callback && callback('status.failure.mongodb_not_ready')
    }

    const server = express()
    const parseServer = new ParseServer({
        databaseURI: DATABASE_URI,
        cloud: path.resolve(__dirname, 'cloud.js'),
        appId: APP_ID,
        masterKey: MASTER_KEY,
        fileKey: FILE_KEY,
        serverURL: SERVER_URL,
        enableAnonymousUsers: false,
        allowClientClassCreation: false
    })

    server.use('/parse', parseServer, function(req, res, next) {
        // This will get called after every parse request
        // and stops the request propagation by doing nothing
    })

    // TODO: Improve the error page
    server.use(function(err, req, res, next) {
        res.status(404).end('error')
    });

    var done = function() {
        console.log(`ParseServer is now running on http://localhost:${SERVER_PORT}`)
        onStarted(callback)
    }
    server.listen(SERVER_PORT, done)
}

function onStarted(callback){
    // Re-create class AktorStorage if needed
    var AktorStorage = Parse.Object.extend('AktorStorage')
    var storage = new AktorStorage()
    storage.save(null, {
        success: function(storage){
            storage.destroy()
            callback && callback()
        },
        error: function(storage, error){
            callback && callback(err)
        }
    })
}

module.exports = {
    initialize,
    start
}

'use strict'
const ContainerService = require('./container')
const _ = require('lodash')
const util = require('../../util')
const fs = require('fs')
const SimpleLogger = require('../core/logger').SimpleLogger
var logger = null

var argv = require('optimist').argv
    // expected to executed with --conf <JSON configuration string>
var conf = argv.conf

if (_.isNil(conf)) {
    process.exit(-1)
}

try {
    conf = JSON.parse(conf)
} catch (e) {
    process.exit(-1)
}

var service = new ContainerService(conf)

function terminate(){
    // clean up and then call the following
    if(logger){
        logger.warn(`Received a signal to terminate the container named ${conf.id}`)
    }
    var exit = () => {
        process.exit(0) // or 1
    }
    service.stop(function(err) {
        setTimeout(exit, 5000) // 5 seconds to wait for fully stops
    })
}

process.on('SIGINT', terminate)
process.on('SIGTERM', terminate)

process.on('uncaughtException', function(err) {
    // catch all uncaught exceptions
    var script = util.getFaultyModule(err)[0]
    var message = `***********************************************
    Uncaught exception: ${err.stack}
    ***********************************************`
    if(logger){
        logger.warn(message)
    }
    service.onUncaughtException(script, function(err) {
        process.exit(-1)
    })
})

service.start(function(err) {
    if (err) {
        process.exit(-1)
    }
    logger = new SimpleLogger( { name: conf.id } )
    logger.info(`Container named ${conf.id} is started successfully.`)
})

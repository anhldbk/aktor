'use strict'

// This is a collection of service executors
const exec = require('./exec')
const load = require('./load')

// Remember to bind Actor instances to these functions before invoking them
module.exports = {
    exec, // (programPath, conf, callback)
    load  // (modulePath, conf, callback)
}

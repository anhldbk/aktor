'use strict'
const common = require('./common')
const _ = require('lodash')
const util = require('../../../util')

// Executing a nodejs service, using the same process space
// @param script: Absolute path to the module (so we can `require`)
// @param conf: A configuration
// @param callback: A function `(err, result)` to call when the service's ready
// NOTE: Remember to bind this function with an instance of CommonAktor
function load(script, conf, callback){
    var error = common.valid(this, script, conf, callback)
    if(!_.isNil(error)){
        return callback && callback(error)
    }

    var self = this
    var id = _.get(conf, 'id')
    var Service = require( script )
    var service = new Service( conf )

    // register a handler first
    var listenDone = function( message ){
        // err will always be null
        var result = _.get( message, 'params.status', 'status.offline' )
        if( result === 'status.online' ){
            // for `load` services, we may pass an instance back
            return callback && callback(null, service)
        }

        // kill the child :(
        service.stop()
        callback && callback(result)
    }
    self.listenOnce(util.pathJoin('event/service/world/manifest', id), listenDone)

    // Then start the service
    var startDone = function(err){
        var message = null
        if(err){
            message = util.stringFormat('Can NOT activate service %s. Reason: %s', id, err)
            self.logError( message )
        } else {
            message = util.stringFormat('Service %s is activated', id)
            self.logInfo( message )
        }
    }
    service.start( startDone )
}

module.exports = function(script, conf, callback){
    try{
        load.bind(this)(script, conf, callback)
    } catch(e){
        callback && callback('status.failure.load', e.toString())
    }
}

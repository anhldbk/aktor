'use strict'
const _ = require('lodash')
const fs = require('fs')
const CommonAktor = require('../../core/aktor').CommonAktor
const util = require('../../../util')

// Translate an object into a list
// For example: {a: 2, b: 3} -> '--a 2 --b 3'
// @param object: The object
// @return The list
function objectToString(object){
    var keys = _.keys(object)
    var mapOp = function(key) {
        return ['--' + key, object[key].toString()]
    }
    var res = _.map(keys, mapOp)
    res = _.flatten(res)
    return res.join(' ')
}

// Check if params passed to exectors are valid
// @param context: Context of this function
// @param modulePath: Absolute path to the associated nodejs module
// @param conf: A configuration
// @param callback: A callback function of `(err, result)`
// @return Returns null if they are valid. Otherwise, returns the error code.
function valid(context, modulePath, conf, callback){
    if( !(context instanceof CommonAktor) ){
        return 'status.failure.invalid_binding'
    }

    if(!_.isFunction(callback)){
        // no callback provided
        return 'status.failure.invalid_callback'
    }

    if(!fs.existsSync(modulePath)){
        if(!fs.existsSync(modulePath + '.js')){ // check for js extension
            return 'status.failure.no_such_module'
        }
    }
    if( util.isAnyNil(conf, [ 'id', 'token' ])){
        return 'status.failure.invalid_conf'
    }
    return null
}

module.exports = {
    objectToString,
    valid
}

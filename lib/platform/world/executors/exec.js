'use strict'
const common = require('./common')
const pm2 = require('pm2')
const _ = require('lodash')
const async = require('async')
const path = require('path')

// Execute a command (which is a service program) in a dedicated process
// @param script: Absolute path (including parameters) to the executable program
// @param conf: A configuration
// @param callback: A function `(err, result)` to call when the service's ready
// NOTE:
// 1. This function's suitable for services implemented in non-Nodejs languages
// 2. Configuration will be translated into parameters to pass to the program.
// For example:
//  --id <granted id>
//  --token <granted token>
//  --update <ttl period>
//  --host <broker's host>
//  --port <broker's port>
//  --maxRestarts <number of  maximum number of times in a row a script will be restarted, optional, default is 3>
// 3. Remember to bind this function with an instance of CommonAktor
function exec(script, conf, callback) {
    var error = common.valid(this, script, conf, callback)
    if (!_.isNil(error)) {
        return callback && callback(error)
    }

    var args = getArgs(conf)
    var id = _.get(conf, 'id')

    var name = id
    var maxRestarts = _.get(conf, 'maxRestarts', 3)
    var self = this

    var done = (err) => {
        pm2.disconnect()
        callback && callback(err)
    }

    var connectDone = function(error) {
        if (error) {
            return done('status.failure.pm2_not_ready')
        }

        var options = {
            script,
            args,
            name,
            maxRestarts
        }

        var startError = null

        var listenDone = function(message) {
            if ( !_.isNil(startError) ) {
                return done(startError)
            }

            // err will always be null
            var result = _.get(message, 'params.status', 'status.offline')
            if (result === 'status.online') {
                return done()
            }

            // kill the child :(
            pm2.stop(id, done)
        }
        self.listenOnce(path.join('event/service/world/manifest', id), listenDone)

        var startDone = function(err) {
            if ( _.isNil(err) ) {
                return
            }
            startError = 'status.failure.pm2_start'
        }

        pm2.start(options, startDone)
    }

    pm2.connect(connectDone)

}

function getArgs(conf){
    // cloning the configuration
    var args = _.cloneDeep(conf)
    // remove unnecessary fields
    var keys = _.keys(args), i, key
    // for zigbee service, we use `update` instead of `timeout`
    var allowedKeys = ['id', 'token', 'update', 'host', 'port']
    for( i = 0; i < keys.length; i++){
        key = keys[i]
        if( allowedKeys.indexOf(key) == -1 ) {
            _.unset( args, key)
        }
    }
    args = common.objectToString(args)
    return args
}

module.exports = function(script, conf, callback) {
    try {
        exec.bind(this)(script, conf, callback)
    } catch (e) {
        if (_.isFunction(callback)) {
            callback('status.failure.unknown', e.toString())
        }
    }
}

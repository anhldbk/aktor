'use strict'
const DistributorBase = require('./base')
const _ = require('lodash')

// A distributor in round-robin fashions
class RoundDistributor extends DistributorBase {

    // Constructor
    // @param containers: [Array of strings] Array of containers IDs to assign aktors to.
    constructor(containers) {
        super(containers)
        this._count = 0
        this._aktorContainer = {}
    }

    // Assign an aktor to a container
    // @param id: Id of the aktor
    // @return: Id of the worker to assign
    get( id ){
        var assignedContainer =  this._aktorContainer[id]
        if( !_.isNil( assignedContainer ) ){
            return assignedContainer
        }

        if( this._containers.length  == 0){
            throw new Error('No container to assign to')
        }

        if( this._count >= this._containers.length) {
            this._count = 0 // to cope with dynamically adding/removing containers
        }

        assignedContainer = this._containers[ this._count ]
        this._count = (this._count + 1) % this._containers.length
        this._aktorContainer[id] = assignedContainer
        return assignedContainer
    }

    // Check if a aktor is already assigned to a container
    // @param id: Id of the aktor
    // @return: Returns true if an assignment is made. Otherwise, returns false
    exist(id){
        return _.has( this._aktorContainer, id )
    }

    // Remove an assigned aktor
    // @param id: Id of the aktor
    // @return: Returns true if the removal's successful. Otherwise, returns false
    remove( id ){
        if( !this.exist(id) ){
            return false // not assigned yet
        }
        _.unset( this._aktorContainer, id )
        return true
    }

    // This method is called whenever a container is removed
    // We removed any aktor added
    _onContainerRemoved(container){
        var omitOp = (value) => value == container
        this._aktorContainer =_.omitBy( this._aktorContainer , omitOp)
    }

    // Add a distribution
    // @param aktor: [String] An aktor's id
    // @param container: [String] A container's id
    // @return: Returns false if the aktor is already assigned. Otherwise, returns true.
    // NOTE: If the container doesn't exist, we'll add it
    addDistribution(aktor, container){
        if(this.exist(aktor)){
            return false
        }
        var index = this._containers.indexOf( container )
        if( index === -1 ){ // no such container already exists
            this._containers.push(container)
        }

        this._aktorContainer[aktor] = container
        return true
    }
}

module.exports = RoundDistributor

'use strict'
const _ = require('lodash')

// Base class for container distributors
class DistributorBase {

    // Constructor
    // @param containers: [Array of strings] Array of containers IDs to assign aktors to.
    constructor(containers){
        if( _.isNil(containers) ){
            containers = []
        }
        this._containers = containers
        if( !_.isArray( this._containers ) ){
            throw new Error('Invalid containers.')
        }
    }

    // Assign an aktor to a container
    // @param id: Id of the aktor
    // @return: Id of the container to assign
    get( id ){
        throw new Eror('Not implemented')
    }

    // Remove an assigned aktor
    // @param id: Id of the aktor
    // @return: Returns true if the removal's successful. Otherwise, returns false
    remove( id ){
        throw new Eror('Not implemented')
    }

    // Check if a aktor is already assigned to a container
    // @param id: Id of the aktor
    // @return: Returns true if an assignment is made. Otherwise, returns false
    exist(id){
        throw new Eror('Not implemented')
    }

    // Get available containers
    // @returns: Array of available containers
    getContainers(){
        return _.cloneDeep(this._containers)
    }

    // Add a new container into the distributor
    // @param container: [String] Name of the container
    // @return: If the name's not unique, returns false. Otherwise, returns true
    addContainer(container){
        var index = this._containers.indexOf( container )
        if( index !== -1 ){ // already exists
            return false
        }
        this._containers.push(container)
        this._onContainerAdded(container)
        return true
    }

    // This method is called whenever a container is added
    // You can optionally override it in your method
    _onContainerAdded(container){

    }

    // Remove a container
    // @param container: [String] Name of the container
    // @return: If there's no such container, returns false. Otherwise, returns true.
    removeContainer(container){
        var index = this._containers.indexOf( container )
        if( index === -1 ){ // no such container
            return false
        }
        _.pull( this._containers, container )
        this._onContainerRemoved(container)
        return true
    }

    // This method is called whenever a container is removed
    // You can optionally override it in your method
    _onContainerRemoved(container){

    }

    // Add a distribution
    // @param aktor: [String] An aktor's id
    // @param container: [String] A container's id
    // @return: Returns false if the aktor is already assigned. Otherwise, returns true.
    // NOTE: If the container doesn't exist, we'll add it
    addDistribution(aktor, container){
        throw new Eror('Not implemented')
    }
}

module.exports = DistributorBase

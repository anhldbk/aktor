'use strict'
const util = require('../../util')
const _ = require('lodash')
const WorldService = require('./world')
const SimpleLogger = require('../core/logger').SimpleLogger
var service = null
var logger = null

function terminate(){
    // clean up and then call the following
    logWarn('Received a signal to terminate World')
    var exit = () => {
        process.exit(0) // or 1
    }
    if( !service ){
        return exit()
    }
    service.stop(function(err) {
        setTimeout(exit, 5000) // 5 seconds to wait for fully stops
    })
}

function logInfo(message){
    logger && logger.info(message)
}

function logWarn(message){
    logger && logger.warn(message)
}

function logError(message){
    logger && logger.error(message)
}

function onUncaughtException(err) {
    // catch all uncaught exceptions
    var script = util.getFaultyModule(err)[0]
    var message = `Uncaught exception in module ${script}
    World is shutting down  ...`
    logError(message)

    service.stop(script, function(err) {
        logInfo('Restarting World...')
        process.exit(-1)
    })
}

function run() {
    // register handlers for various signals
    process.on('SIGINT', terminate)
    process.on('SIGTERM', terminate)
    process.on('uncaughtException', onUncaughtException)

    service = new WorldService() // no configuration is needed. World is a self-configuring service
    service.start(function(err) {
        if (err) {
            console.log('Failed to start World. Shutting down now...')
            process.exit(-1)
        }
        logger = new SimpleLogger( { name: conf.id } )
        logger.info('World is started successfully.')
    })
}

function boot(){
    // check if platform services are ready
    var error = util.isPlatformReady()
    if (error) {
        console.log(`Platform services are not ready. Error: ${error}`)
        console.log(`Waiting for another attempt`)
        return setTimeout(boot, 10000) // wait for 10 seconds to attempt another check
    }
    // ok, services are ready.
    run()
}

boot()

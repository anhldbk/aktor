'use strict'

const CommonAktor = require('../../core/aktor').CommonAktor
const MqttInterface = require('../../core/interface').MqttInterface
const ParseStorage = require('../../core/storage').ParseStorage
const util = require('../../../util')
const _ = require('lodash')
const fs = require('fs')
const executors = require('../executors')
const async = require('async')
const KEY_RUNNING_AKTORS = '_actors_set_'
const KEY_UNCAUGHT_EXCEPTIONS_IN_PREVIOUS_SESSION = '_uncaught_exception_'
const Configurator = require('../configurator')
const configurator = new Configurator()

const pm2 = require('pm2')
const MAX_RESTARTS = 2
const AktorStates = require('./aktor-states')

// ContainerService is the service for hosting aktors (node and non-nodejs)
// It runs in a standalone service
class ContainerService extends CommonAktor {
    // Constructor
    // @param conf: [Object] A configuration object
    //      It's required to have `interfaces.local` & `storage.local` defined
    constructor(conf) {
        // register handlers
        var prefix = util.pathJoin('action', conf.id) + '/'
        var handlers = {
            [ prefix + 'aktor_start' ] : 'onAktorStart',
            [ prefix + 'aktor_stop' ] : 'onAktorStop',
            [ prefix + 'aktor_destroy' ] : 'onAktorDestroy',
            [ prefix + 'aktor_list' ] : 'onAktorList'
        }

        var path = 'interfaces.local.listen'
        _.set(conf, path, handlers)

        super(conf)
        this._aktorStates = new AktorStates()
    }

    // Initialize the Container by starting previously started aktors
    // @param callback: a function `(err, result)` to call when result is ready
    _init(callback) {
        this.logInfo('Initializing container ...')
        var localStorage = this.getLocalStorage()
        var res = localStorage.getSync(KEY_UNCAUGHT_EXCEPTIONS_IN_PREVIOUS_SESSION)

        if (!_.isNil(res[0])) {
            return callback && callback('status.failure.get_storage')
        }
        var uncaughtException = res[1]

        var clean = _.get( this._conf, 'clean', false )
        if( clean ){ // discard the storage
            if(!uncaughtException){
                this.logInfo('Cleaning the containers....')
                return localStorage.destroy(callback)
            }
            this.logInfo('Previous session has uncaught exceptions. `clean` flag will be deprecated')
            localStorage.unsetSync(KEY_UNCAUGHT_EXCEPTIONS_IN_PREVIOUS_SESSION) // clear the flag
        }

        var aktors = localStorage.getSync(KEY_RUNNING_AKTORS)[1] // array of running aktors
        if (_.isNil(aktors)) {
            this.logInfo('No aktor to start when initializing')
            return callback && callback() // nothing to do
        }

        this.logInfo('Aktors to start at initialization: ' + aktors)
        var self = this
        var startAktor = function(id, cb) {
            var conf = localStorage.getSync(id)[1]
            var faulty = _.get(conf, 'faulty', 0)
            var prevState = _.get(conf, '_state', AktorStates.STATE_STARTED)
            var maxRestarts = _.get(self._conf, 'maxRestarts', MAX_RESTARTS)

            if (faulty >= maxRestarts) {
                self.logWarn(`Faulty aktor with id = ${conf.id} will not be started.`)
                self._aktorStates.createStopped(conf.id)
                return cb()
            }

            if( prevState !== AktorStates.STATE_STARTED ){
                self.logWarn(`Aktor with id = ${conf.id} was NOT previously started ( last state = ${prevState} ). It will NOT be started automatically in this session`)
                return cb()
            }

            self._startAktor(conf, cb)
        }
        async.eachSeries(aktors, startAktor, callback)
    }

    _postInit(callback){
        var self = this
        var params = { id: self.getId() }
        self.tell('event/service/container/container_started', { params })
        super._postInit(callback)
    }

    getLocalStorage() {
        return this.getStorage('local')
    }

    onAktorList(message) {
        // restrict this request to only 'service/world'
        if( !this._isAuthorized(message) ){
            return this.replyUnauthorized(message)
        }
        var localStorage = this.getLocalStorage()
        var id = _.get( message, 'params.id')
        var aktors = [], result

        result = localStorage.getSync(KEY_RUNNING_AKTORS)
        if (result[0]) {
            return this.replyFailure(message, result[0])
        }

        var runnings = result[1]
        if( _.isNil(runnings)){
            runnings = []
        }

        if( !_.isNil(id) ){ // if the id is provided
            runnings = [id]
        }

        for (var i = 0; i < runnings.length; i++) {
            result = localStorage.getSync(runnings[i])[1] // error-free here
            result = _.cloneDeep(result) // We don't want to modify the object stored in ParseStorage
            _.unset(result, 'token') // remove the token
            aktors.push(result)
        }

        this.replySuccess(message, { aktors })
    }

    // Check if a message is authorized. This is an overriden version of `CommonAktor`
    // @return Returns true if it's authorized. Otherwise, returns false.
    _isAuthorized(message) {
        var from = _.get( message, 'header.from')
        if( _.isNil(from) ){
            return false
        }

        var allowed = [ this.getId(), 'service/world', 'system' ]
        return allowed.indexOf( from ) !== -1
    }

    onAktorStart(message) {
        // restrict this request to only `service/world`
        if( !this._isAuthorized(message) ){
            return this.replyUnauthorized(message)
        }

        var params = _.get(message, 'params')
        var id = _.get(params, 'id')
        if( _.isNil(id) ){
            return this.replyInvalidId(message)
        }

        var self = this
        var done = function(err, result) {
            if (err) {
                self.logError(`Can NOT start the aktor with id = ${id} . Reason: ${err}`)
                return self.replyFailure(message, err)
            }
            self.logInfo('Aktor is started successfully')
            self.replySuccess(message)
        }
        this._startAktor(params, done)
    }

    // Validate a script's path
    // @param script: Absolute path to a script
    // @return: Returns a pair of [err, result]. If everything is ok, `err` is null, `result` contains the validated path
    _validateScriptPath(script) {
        // script must be an absolute path to nodejs or non-nodejs aktors
        // Nodejs script can be paths to `.js` files or directories which contain `index.js` files
        if (_.isNil(script)) {
            return ['status.failure.invalid_script', null]
        }

        if (!fs.existsSync(script)) {
            script += '.js'
            if (!fs.existsSync(script)) {
                return ['status.failure.invalid_script', null]
            }
        }
        var stats = fs.lstatSync(script)
        if (stats.isDirectory()) {
            // currently we only support for Nodejs module
            // TODO: support for other scripts like Python, Ruby...
            script = util.pathJoin(script, 'index.js')
            if (!fs.existsSync(script)) {
                return ['status.failure.invalid_script', null]
            }
        }
        return [null, script]
    }

    // Start an aktor with a provided configuration
    // @param conf: [Object] A configuration object. Fields `id` & `token` are required.
    // @param callback: [Function] A function of `(err, result)` to process results
    _startAktor(conf, callback) {
        var self = this

        var script = _.get(conf, 'script')
        var result = this._validateScriptPath(script)
        if (!_.isNil(result[0])) {
            self.logError(`Invalid script detected with script = ${script}. Reason: ${result[0]}.`)
            return callback && callback(result[0])
        }
        script = result[1]

        if (util.isAnyNil(conf, ['id', 'token'])) {
            self.logError(`Configuration must provide field 'id' and 'token'`)
            return callback && callback('status.failure.invalid_configuration')
        }
        var maxRestarts = _.get(conf, 'maxRestarts', 3)
        var id = _.get(conf, 'id')
        var token = _.get(conf, 'token')

        conf._container = this.getId()

        // Check its state first
        if( self._aktorStates.createStart(id) == null ){
            self.logError(`Can NOT start the aktor named ${id}. Reason: invalid state.`)
            self.logError(`Current state of aktor ${id} is: ${self._aktorStates.get(id)}` )

            return callback && callback('status.failure.invalid_state')
        }

        var tuple = configurator.getAktorConf(id, token),
            err = tuple[0],
            activeConf = tuple[1]

        if(err){
            self.logError(`Can NOT use generate configuration for aktor named ${id}. Reason: ${err}`)
            return callback && callback('status.failure.configuration_failed')
        }

        // merge it
        conf = _.merge( activeConf, conf )

        self.logInfo(`Starting an aktor with script located at ${script} ...`)
        self.logInfo(`Configuration: ${JSON.stringify(conf)}`)

        // ok launch it now
        var exec = executors.load.bind(this)
        conf._isProcess = false
        if (!script.endsWith('.js')) {
            // ok then we're launching a non-Nodejs aktor
            exec = executors.exec.bind(this)
            conf._isProcess = true // it will be hosted in another process monitored by PM2
        }

        var execDone = function(err, result) {
            if (err) {
                self._aktorStates.error(id) // error state
                return callback(err)
            }
            var params = { id }
            self.tell('event/service/container/aktor_started', { params })

            err = self._aktorStates.started(id)
            conf._state = AktorStates.STATE_STARTED
            var localStorage = self.getLocalStorage()

            // check if we're activating aktors in previous sessions
            var prevSession = localStorage.getSync(id)
            if (!_.isNil(prevSession[0])) {
                return callback && callback('status.failure.can_not_get')
            }
            if (!_.isNil(prevSession[1])) {
                self.logInfo(`Previous session is detected for aktor named ${id}`)
                return localStorage.set(id, conf, callback)
            }

            conf.faulty = 0 // set the faulty count
            var done = function(err) {
                if (err) {
                    err = 'status.failure.can_not_update'
                }
                callback && callback(err)
            }
            async.waterfall(
                [
                    // TODO: Auto-bind for member functions in StorageBase-derived classes
                    async.apply(localStorage.set.bind(localStorage), id, conf),
                    async.apply(localStorage.add.bind(localStorage), util.getSha256Hash(script), id),
                    async.apply(localStorage.add.bind(localStorage), KEY_RUNNING_AKTORS, id),
                ],
                done
            )
        }
        exec(script, conf, execDone)
    }

    onAktorStop(message) {
        // restrict this request to only 'service/world'
        if( !this._isAuthorized(message) ){
            return this.replyUnauthorized(message)
        }

        var conf = _.get(message, 'params')

        var self = this
        var stopDone = function(err) {
            if (err) {
                return self.replyFailure(message, err)
            }
            self.replySuccess(message)
        }
        conf.timeout = this.getForwardTimeout(message) // calculate the forward timeout

        self._stopAktor(conf, stopDone)
    }

    onAktorDestroy(message){
        // restrict this request to only 'service/world'
        if( !this._isAuthorized(message) ){
            return this.replyUnauthorized(message)
        }

        var conf = _.get(message, 'params')

        var self = this
        var destroyDone = function(err) {
            if (err) {
                return self.replyFailure(message, err)
            }
            self.replySuccess(message)
        }
        conf.timeout = this.getForwardTimeout(message) // calculate the forward timeout

        self._destroyAktor(conf, destroyDone)
    }

    // Using PM2 to stop an aktor running as a standalone process
    _pm2StopProcess(id, callback) {
        var connected = false
        var pm2connect = function(cb) {
            pm2.connect((err) => cb(err))
        }
        var pm2delete = function(cb) {
            connected = true
            pm2.stop(id, (err) => cb(err))
        }
        var done = function(err) {
            if (connected) {
                pm2.disconnect()
            }
            callback && callback() // dismiss any error
        }
        async.waterfall([pm2connect, pm2delete], done)
    }

    // Stop an aktor via its Id
    // @param conf: [Object] The configuration which accepts following fields:
    //  - `id`: The aktor's id
    //  - `timeout: [Integer, optional] Timeout to ask. If not set, the timeout configured in this aktor's configuration will be used.
    //  - `active` [Boolean, optional, default false]. If this is an active Stop request, set this field to true.
    //  - `_from`: [String, optional, default 'service/world'] Id of invoker
    // @param callback: callback function
    _stopAktor(conf, callback) {
        if( _.isNil(conf) ){
            return callback && callback('status.failure.invalid_configuration')
        }
        var id = _.get(conf, 'id'),
            timeout = _.get(conf, 'timeout', this.getForwardTimeout()),
            from = _.get(conf, '_from', 'service/world'),
            state = this._aktorStates.get(id)

        if (_.isNil(id)) { // no such id
            return callback && callback('status.failure.invalid_id')
        }

        if(state === AktorStates.STATE_STOPPED){
            this.logInfo(`The aktor named ${id} is already stopped.`)
            return callback && callback()
        }
        if( !this._aktorStates.stop(id) ){
            this.logWarn('Can NOT stop the aktor named ' + id + '. Reason: invalid state.')
            this.logInfo('Current state is: ' + state )
            return callback && callback('status.failure.invalid_state')
        }

        this.logInfo('Stopping aktor with id = ' + id + '...')

        var self = this
        var localStorage = self.getLocalStorage()
        // check if the aktor's running
        var record = localStorage.getSync(id)
        // record is a tuple of err, result
        if (record[0]) { // err, can not query
            self._aktorStates.error(id)
            return callback && callback('status.failure.can_not_get')
        }
        var aktor = record[1]
        if (_.isNil(aktor)) {
            self._aktorStates.error(id)
            return callback && callback('status.failure.no_such_aktor_running')
        }
        if( (from !== 'service/world') && ( from !== aktor._parent) && ( from !== id ) ) {
            // only World or its parent or itself can invoke this method
            return callback && callback('status.failure.unauthorized')
        }

        if( self._stopping !== true){ // if the container is NOT shutting down
            // we'll update the state associated
            aktor._state = AktorStates.STATE_STOPPED
            localStorage.setSync(id, aktor) // no matter what, we update it in the next tick
        }

        var killProcess = () => {
            self.logInfo(`Trying to use PM2 to stop the process named = ${id}...`)
            var killDone = (err) => {
                if(err){
                    self.logError(`Can NOT use PM2 to kill the process named ${id}`)
                    self._aktorStates.error(id)
                } else {
                    self.logInfo(`Successfully use PM2 to kill the process named ${id}`)
                    self._aktorStates.stopped(id)
                }
                return callback && callback(err)
            }
            self._pm2StopProcess(id, killDone)
        }

        // we politely ask the aktor to stop via `action/stop`
        var askDone = function(err, result) {
            // dont care about err may have
            if( aktor._isProcess){
                killProcess()
            }
            self._aktorStates.stopped(id)
            callback && callback()
        }

        var message = {
            type: 'action/stop'
        }
        self.ask(id, message, { timeout }, askDone)
    }

    // Stop all aktors
    _stopAktors(callback) {
        var res = this.getLocalStorage().getSync(KEY_RUNNING_AKTORS)
        if (!_.isNil(res[0])) {
            return callback && callback('status.failure.get_storage')
        }
        var aktors = res[1] // array of running aktors
        if (!_.isArray(aktors)) {
            return callback && callback()
        }

        var mapOp = (id) => {
            return { id }
        }
        aktors = _.map( aktors, mapOp )
        async.each(aktors, this._stopAktor.bind(this), callback)
    }

    // Deactivate this actor
    // @param callback: a function `(err, result)` to call when result is ready
    stop(callback) {
        var self = this
        self._stopping = true

        var emitContainerStopped = (cb) => {
            var params = { id: self.getId() }
            self.tell('event/service/container/aktor_stopped', { params })
            cb()
        }

        configurator.dispose()

        async.waterfall(
            [
                emitContainerStopped,
                this._stopAktors.bind(this),
                super.stop.bind(this)
            ],
            callback
        )
    }

    // Destroy an aktor by removing its data
    // Remember to stop aktors first before invoking this method.
    // @param conf: A configuration object. Expected fields are:
    //  `id`: Id of the already-stopped aktor to destory
    // @param callback: a function `(err, result)` to call when result is ready
    _destroyAktor(conf, callback){
        if( _.isNil(conf) ){
            return callback && callback('status.failure.invalid_configuration')
        }
        var id = _.get(conf, 'id')
        var self = this

        if (_.isNil(id)) { // no such id
            return callback && callback('status.failure.invalid_id')
        }

        // check if the aktor's running
        var localStorage = self.getLocalStorage()
        var record = localStorage.getSync(id)
        // record is a tuple of err, result
        if (record[0]) { // err, can not query
            return callback && callback('status.failure.can_not_get')
        }
        var aktor = record[1]
        if (_.isNil(aktor)) {
            return callback && callback('status.failure.no_such_aktor_running')
        }

        var script = aktor.script
        script = self._validateScriptPath(script)[1] // must be successful

        if( !this._aktorStates.destroy(id) ){
            self.logWarn('Can NOT destroy the aktor named ' + id + '. Reason: invalid state.')
            self.logInfo('Current state is: ' + this._aktorStates.get(id) )
            return callback && callback('status.failure.invalid_state')
        }

        self.logInfo('Destroying data associated with aktor id = ' + id + '...')
        localStorage.unsetSync(id)
        localStorage.removeSync(util.getSha256Hash(script), id)
        localStorage.removeSync(KEY_RUNNING_AKTORS, id)
        self._aktorStates.destroyed(id)
        configurator.delete(id)

        return callback && callback()
    }

    _destroyAktorSync(conf){
        return util.callsync( this._destroyAktor, this, [conf] )
    }

    // Handler to process uncaught exceptions
    // @param script: [String] Absolute path to the faulty module
    // @param callback: [Function, Optional] A function of `(err, result)` to process responses
    onUncaughtException(script, callback) {
        // we want to know what aktors caused the exceptions
        var self = this
        this.logWarn('Processing uncaught exceptions at script ' + script + '...')
        var localStorage = this.getLocalStorage()
        var stopDone = function() {
            self.logInfo('Restarting container...')
            callback && callback()
        }
        this._uncaughtException = true

        var res = localStorage.getSync(util.getSha256Hash(script))
        if (res[0]) { // error
            return this.logError('Can NOT access the local storage. Reason: ' + res[0])
        }
        var ids = res[1] // array of aktors running the same script
        if (_.isNil(ids)) {
            this.logWarn('Can NOT detect faulty aktors')
            return this.stop(stopDone) // nothing to do
        }

        this.logWarn('Detected faulty aktors are ' + ids)
        var aktor, id
        for (var i = 0; i < ids.length; i++) {
            id = ids[i]
                // increase field `faulty`
            aktor = localStorage.getSync(id)[1]
            if (_.isNil(aktor)) {
                continue
            }
            if (_.isNil(aktor.faulty)) {
                aktor.faulty = 0
            }
            aktor.faulty += 1

            this._aktorStates.error(id)

            if (aktor.faulty > aktor.maxRestarts) {
                this.logWarn('A faulty aktor is detected with id = ' + id)
                var params = { id }
                this.tell('event/service/container/aktor_faulty', { params })
            }
            localStorage.setSync(KEY_UNCAUGHT_EXCEPTIONS_IN_PREVIOUS_SESSION, true)
            localStorage.setSync(id, aktor)
        }

        this.stop(stopDone)
    }
}

module.exports = ContainerService

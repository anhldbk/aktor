'use strict'
const StateMachine = require('../../share').StateMachine
const _ = require('lodash')

// A class for managing runtime information about aktors & their states
class AktorStates {

    // Constructors
    constructor() {
        // get IDs of containers
        this._states = { }
    }

    // Check if an aktor exists in AktorStates
    // @return: Returns true if it exists. Otherwise, returns false.
    exist(id){
        return _.has( this._states, id )
    }

    // Get information about an aktor via its id
    // @param id: The id
    // @return: If there's such Id in our AktorStates, returns the current state. Otherwise, returns null
    get(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return null
        }
        return state.current
    }

    // Create a new aktor via its id
    // @param id: The id
    // @return: If the aktor does NOT exist in AktorStates, we'll create associated data and return the current state. Otherwise, returns null
    create(id){
        var state = this._states[id]
        if( !_.isNil(state) ){
            return null
        }
        var state = StateMachine.create({
            initial: AktorStates.STATE_CREATED,
            events: [{
                name: AktorStates.ACTION_START,
                from: AktorStates.STATE_CREATED,
                to: AktorStates.STATE_STARTING
            }, {
                name: AktorStates.ACTION_START,
                from: AktorStates.STATE_STOPPED,
                to: AktorStates.STATE_STARTING
            }, {
                name: AktorStates.ACTION_STARTED,
                from: AktorStates.STATE_STARTING,
                to: AktorStates.STATE_STARTED
            }, {
                name: AktorStates.ACTION_RESTART,
                from: AktorStates.STATE_STARTED,
                to: AktorStates.STATE_STARTING
            }, {
                name: AktorStates.ACTION_STOP,
                from: AktorStates.STATE_STARTED,
                to: AktorStates.STATE_STOPPING
            }, {
                name: AktorStates.ACTION_STOP,
                from: AktorStates.STATE_STARTING_ERROR,
                to: AktorStates.STATE_STOPPING
            }, {
                name: AktorStates.ACTION_STOP,
                from: AktorStates.STATE_STARTED_ERROR,
                to: AktorStates.STATE_STOPPING
            }, {
                name: AktorStates.ACTION_STOPPED,
                from: AktorStates.STATE_STOPPING,
                to: AktorStates.STATE_STOPPED
            },  {
                name: AktorStates.ACTION_ERROR,
                from: AktorStates.STATE_STARTING,
                to: AktorStates.STATE_STARTING_ERROR
            },  {
                name: AktorStates.ACTION_ERROR,
                from: AktorStates.STATE_STARTED,
                to: AktorStates.STATE_STARTED_ERROR
            }, {
                name: AktorStates.ACTION_ERROR,
                from: AktorStates.STATE_STOPPING,
                to: AktorStates.STATE_STOPPING_ERROR
            }, {
                name: AktorStates.ACTION_DESTROY,
                from: AktorStates.STATE_STOPPED,
                to: AktorStates.STATE_DESTROYING
            }, {
                name: AktorStates.ACTION_DESTROY,
                from: AktorStates.STATE_STOPPING_ERROR,
                to: AktorStates.STATE_DESTROYING
            }, {
                name: AktorStates.ACTION_DESTROYED,
                from: AktorStates.STATE_DESTROYING,
                to: AktorStates.STATE_DESTROYED
            }]
        })
        this._states[id] = state

        return state.current
    }

    // Restart an aktor
    // @param id: The id
    // @return: Returns true if the aktor exists and in re-startable states. Otherwise, returns false
    restart(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }
        if( !state.can(AktorStates.ACTION_RESTART) ){
            return false
        }
        state[AktorStates.ACTION_RESTART]()
        return true
    }

    // Starts an aktor
    // @param id: The id
    // @return: Returns true if the aktor exists and in startable states. Otherwise, returns false
    start(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }
        if( !state.can(AktorStates.ACTION_START) ){
            return false
        }
        state[AktorStates.ACTION_START]()
        return true
    }

    // Call this method whenever an aktor is started
    // @param id: The id
    // @return: Returns true if the aktor exists and in startable states. Otherwise, returns false
    started(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }
        if( !state.can(AktorStates.ACTION_STARTED) ){
            return false
        }
        state[AktorStates.ACTION_STARTED]()
        return true
    }

    // Starts an aktor. If it does NOT exist, create it first.
    // @param id: The id
    // @return: If the aktor does NOT exist in AktorStates, we'll create associated data and return the current state. Otherwise, returns null
    createStart(id){
        this.create(id) // create it first, no matter what
        var state = this._states[id]
        if( !state.can(AktorStates.ACTION_START) ){
            return null
        }
        state[AktorStates.ACTION_START]()
        return state.current
    }

    // Stop an aktor initially
    // @param id: The id
    // @return: If the aktor does NOT exist in Registry, we'll create associated data and return an object of {state, container}. Otherwise, returns null
    createStopped(id){
        this.create(id) // create it first, no matter what
        var state = this._states[id]
        state.current = AktorStates.STATE_STOPPED
        return state.current
    }


    // Make an aktor transition to error state
    // @param id: The id
    // @return: Returns true if the aktor exists and in error-able states. Otherwise, returns false
    error(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }
        if( !state.can(AktorStates.ACTION_ERROR) ){
            return false
        }
        state[AktorStates.ACTION_ERROR]()
        return true
    }


    // Stops an aktor
    // @param id: The id
    // @return: Returns true if the aktor exists and in stoppable states. Otherwise, returns false
    stop(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }

        if( !state.can(AktorStates.ACTION_STOP) ){
            return false
        }
        state[AktorStates.ACTION_STOP]()
        return true
    }

    // Call this method whenever an aktor's stopped
    // @param id: The id
    // @return: Returns true if the aktor exists and in stoppable states. Otherwise, returns false
    stopped(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }
        if( !state.can(AktorStates.ACTION_STOPPED) ){
            return false
        }
        state[AktorStates.ACTION_STOPPED]()
        return true
    }

    // Destroy an aktor
    // @param id: The id
    // @return: Returns true if the aktor exists and in removable states. Otherwise, returns false
    destroy(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }

        if( !state.can(AktorStates.ACTION_DESTROY) ){
            return false
        }
        state[AktorStates.ACTION_DESTROY]()
        return true
    }

    // After an aktor is detroyed, invoke this method
    // No requirement for its pre-state
    destroyed(id){
        var state = this._states[id]
        if( _.isNil(state) ){
            return false
        }

        // all states can be removed
        _.unset( this._states, id )
        return true
    }

}
AktorStates.STATE_CREATED = 'created'
AktorStates.STATE_STARTING = 'starting'
AktorStates.STATE_STARTED = 'started'
AktorStates.STATE_STARTING_ERROR = 'starting-error'
AktorStates.STATE_STARTED_ERROR = 'started-error'
AktorStates.STATE_STOPPED = 'stopped'
AktorStates.STATE_STOPPING = 'stopping'
AktorStates.STATE_STOPPING_ERROR = 'stopping-error'
AktorStates.STATE_DESTROYING = 'destroying'
AktorStates.STATE_DESTROYED = 'destroyed'
AktorStates.ACTION_START = 'start'
AktorStates.ACTION_STARTED = 'started'
AktorStates.ACTION_STOP = 'stop'
AktorStates.ACTION_ERROR = 'fault'
AktorStates.ACTION_STOPPED = 'stopped'
AktorStates.ACTION_RESTART = 'restart'
AktorStates.ACTION_DESTROY = 'destroy'
AktorStates.ACTION_DESTROYED = 'destroyed'

module.exports = AktorStates

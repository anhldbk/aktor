'use strict'

const PlatformIdentity = require('../identity').PlatformIdentity
const _ = require('lodash')
const platformConf = require('../conf')

class Configurator {
    constructor(){
        this._platformIdentity = new PlatformIdentity()
        var err = this._platformIdentity.initSync()[0]
        if( !_.isNil(err) ){
            console.log('Fatal: Can NOT initialize WorldService. Reason: ' + err)
            console.log('Terminating now...')
            process.exit(-1)
        }
    }

    dispose(callback){
        this._platformIdentity.dispose(callback)
    }

    // Get configuration for World
    // @return: [Array] A pair of `(err, result)`. If everything is OK, `err` is null and `result` contains the configuration.
    //  Otherwise, `err` contains error information
    getWorldConf(){
        var id = 'service/world',
            instances = platformConf.world.container.instances,
            containers = [],
            conf,
            counter,
            result = this._platformIdentity.upsertSync(id)

        if( !_.isNil(result[0]) ){
            // something goes wrong
            return [ result[0], null ]
        }

        conf = result[1]

        // add configuration for containers
        for( counter = 0; counter < instances; counter++){
            id = `service/container-${counter}`
            result = this._platformIdentity.upsertSync(id)[1]
            // we may guarantee the above operation will be successful
            containers.push( result )
        }
        _.set( conf, 'containers', containers )

        return [ null, conf ]
    }

    // Get configuration for Aktor
    // @param id: [String, required] Id of the aktor
    // @param token: [String, optional] Token of the aktor. If none is provided, an random one will be used
    // @return: [Array] A pair of `(err, result)`. If everything is OK, `err` is null and `result` contains the configuration.
    //  Otherwise, `err` contains error information
    getAktorConf(id, token){
        var conf,
            result = this._platformIdentity.upsertSync(id, token)

        if( !_.isNil(result[0]) ){
            // something goes wrong
            return [ result[0], null ]
        }

        conf = result[1]
        return [ null, conf ]
    }

    // Remove all identities associated with an id
    // @param id: [String] Id of an aktor
    delete(id, callback){
        this._platformIdentity.delete(id, callback)
    }
}

module.exports = Configurator

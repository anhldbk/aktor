'use strict'
const pm2 = require('pm2')
const async = require('async')
const _ = require('lodash')
const util = require('../../util')
const RoundDistributor = require('./distributors').RoundDistributor
const CommonAktor = require('../core/aktor').CommonAktor
const MqttInterface = require('../core/interface').MqttInterface
const ParseStorage = require('../core/storage').ParseStorage
const Configurator = require('./configurator')
const configurator = new Configurator()
const path = require('path')
const PlatformConfiguration = require('lowdb')(
    path.resolve(__dirname, '../conf.json')
)

class WorldService extends CommonAktor {
    // Constructor
    // No configuration is needed. World is a self-configuring aktor
    constructor() {
        var id = 'service/world',
            result = configurator.getWorldConf(),
            err = result[0],
            conf = result[1],
            path = 'interfaces.local.listen'

        if (! _.isNil(err) ){
            console.log('Fatal: Can NOT initialize WorldService. Reason: ' + err)
            console.log('Terminating now...')
            process.exit(-1)
        }

        // register handlers
        _.set(conf, path, {
            // 'action/service/world/manifest': 'onManifest',
            // 'action/service/world/register': 'onRegister',
            // 'action/service/world/unregister': 'onUnregister',
            // 'action/service/world/query': 'onQuery',
            // 'action/service/world/enum': 'onEnum',
            // 'action/service/world/clean': 'onClean',
            // 'action/service/world/restart': 'onRestart',
            'action/service/world/aktor_start' : 'onAktorStart',
            'action/service/world/aktor_stop' : 'onAktorStop',
            'action/service/world/aktor_destroy': 'onAktorDestroy',
            'action/service/world/aktor_list': 'onAktorList',

            'event/service/container/aktor_faulty': 'onAktorFaulty',
            'event/service/container/container_started': 'onContainerStarted',
            'event/service/container/container_stopped': 'onContainerStopped',
            'event/service/container/aktor_started': 'onAktorStarted',
            'event/service/container/aktor_stopped': 'onAktorStopped'
        })
        super(conf)

        this._distributor = new RoundDistributor()
    }

    // [overriden] Check if a message is authorized (that means: it's from `world`, `system`)
    // @return Returns true if it's authorized. Otherwise, returns false.
    _isAuthorized(message) {
        var from = _.get( message, 'header.from')
        if( !_.isString(from) ){
            return false
        }

        var allowed = [ 'system', this.getId() ]
        return allowed.indexOf( from ) != -1
    }

    onAktorStarted(message){
        var aktor = _.get( message, 'params.id' )
        var container = _.get( message, 'header.from' )
        if(_.isNil(aktor)){
            this.logWarn(`Invalid event aktor_started from container ${container}`)
            return
        }
        this._distributor.addDistribution(aktor, container)
    }

    onAktorStopped(message){
        // no need to process this kind of events
    }

    onAktorFaulty(message){
        var id = _.get( message, 'params.id' )
        if(_.isNil(id)){
            var from = _.get( message, 'header.from' )
            this.logWarn(`Invalid event aktor_faulty from container ${from}`)
            return
        }
        var params = _.get( message, 'params' )
        this._distributor.remove(id) // should remove it, coz the container deleted the associated record.
        this.tell('event/service/world/aktor_faulty', {params}) // propagate the event
    }

    // Dynamically update available containers
    onContainerStarted(message){
        var from = _.get(message, 'header.from')
        this.logInfo(`Container ${from} is started`)
        this._distributor.addContainer(from)
    }

    // Dynamically remove unsuable containers
    onContainerStopped(message){
        var from = _.get(message, 'header.from')
        this.logWarn(`Container ${from} is stopped`)
        this._distributor.removeContainer(from)
    }

    onAktorStart(message) {
        var self = this

        var from = _.get( message, 'header.from' )
        var params = _.get( message, 'params')

        var id = _.get(params, 'id')
        if( _.isNil(id) ){ // field `id` is required
            return self.replyInvalidId(message)
        }
        if( !_.has( params, 'script') ){ // field `script` is required
            return self.replyFailure( message, 'status.failure.invalid_script' )
        }

        if( !_.has( params, 'token' ) ){ // if no token provided, we use a random one
            _.set( params, 'token', util.getRandomId() )
        }

        // secretly add field `_parent` into `params`
        _.set( params, '_parent', from )

        var container = this._distributor.get(id)
        var endpoint = util.pathJoin('action', container, 'aktor_start')

        this.logInfo(`Asking container ${container} to start aktor with id = ${id} ...`)
        var timeout = this.getForwardTimeout(message) // calculate the forward timeout

        var done = function(err, msg) {
            if (err) {
                return self.replyFailure(message, err)
            }
            var response = _.get(msg, 'response')
            self.replySuccess(message, response)
        }
        this.ask(endpoint, { params }, { timeout }, done)
    }

    onAktorStop(message) {
        var self = this
        var params = _.get(message, 'params', { })

        // secretly add a field into the params
        var from = _.get( message, 'header.from' )
        _.set( params, '_from', from )

        var id = _.get(params, 'id')
        if( _.isNil(id) ){
            id = from
            _.set( params, 'id', id )
        }

        if( !this._distributor.exist(id) ){
            return this.replyFailure( message, 'status.failure.no_such_aktor_running')
        }

        var container = this._distributor.get(id)
        var endpoint = util.pathJoin('action', container, 'aktor_stop')

        var done = (err, msg) => {
            if (err) {
                return self.replyFailure(message, err)
            }
            var response = _.get(msg, 'response')
            self.replySuccess(message, response)
        }

        this.logInfo(`Asking container ${container} to stop aktor with id = ${id} ...`)
        var timeout = this.getForwardTimeout(message) // calculate the forward timeout
        this.ask(endpoint, { params }, { timeout }, done)
    }

    onAktorDestroy(message){
        var self = this
        var params = _.get(message, 'params', {})

        // secretly add a field into the params
        var from = _.get( message, 'header.from' )
        _.set( params, '_from', from )

        var id = _.get(params, 'id')
        if( _.isNil(id) ){
            id = from
            _.set( params, 'id', id )
        }

        if( !this._distributor.exist(id) ){
            return this.replyFailure( message, 'status.failure.no_such_aktor_running')
        }

        var container = this._distributor.get(id)
        this._distributor.remove(id) // remove it first
        var endpoint = util.pathJoin('action', container, 'aktor_destroy')

        var done = function(err, msg) {
            if (err) {
                return self.replyFailure(message, err)
            }
            var response = _.get(msg, 'response')
            self.replySuccess(message, response)
        }

        this.logInfo('Asking container ' + container + ' to destroy aktor with id = ' + id + ' ...')
        var timeout = this.getForwardTimeout(message) // calculate the forward timeout
        this.ask(endpoint, { params }, { timeout }, done)
    }

    onAktorList(message) {
        var self = this,
            params = _.get(message, 'params'),
            id = _.get(params, 'id'),
            containers, aktors = []

        if( !_.isNil(id) ){
            if( !this._distributor.exist(id) ){
                return this.replyFailure( message, 'status.failure.no_such_aktor_running')
            }
            var container = this._distributor.get(id)
            this.logInfo(`Asking container ${container} to get information about aktor with id = ${id} ...`)
            containers = [ container ]
        } else {
            containers = this._distributor.getContainers()
        }
        var timeout = this.getForwardTimeout(message) // calculate the forward timeout


        var get = function(container, callback){
            var endpoint = util.pathJoin('action', container, 'aktor_list')
            var done = function(err, msg) {
                if (err) {
                    return callback && callback(err)
                }
                var actors = _.get(msg, 'response.aktors')

                for(var i = 0; i< actors.length; i++){
                    actors[i]._container = container // add information about the hosting container
                }
                aktors = _.concat( aktors, actors )
                callback && callback()
            }

            self.ask(endpoint, { params }, { timeout }, done)
        }

        var done = function(err){
            if(err){
                return self.replyFailure( message, err )
            }
            self.replySuccess(message, { aktors })
        }

        async.each( containers, get, done )
    }

    // Initialize the World by launching containers
    // @callback: [Function] function `(err, result)` to process results
    _init(callback) {
        var self = this
        var containers = _.get(self._conf, 'containers', [])
        var prefix = _.get(self._conf, 'containerPrefix', 'aktor-container')
        var containerLaunched = 0, containerTotal = containers.length

        self.logInfo(`Number of container(s) to launch ${containerTotal}`)

        var launchContainer = function(conf, cb) {
            var id = _.get(conf, 'id')
            if (_.isNil(id)) {
                self.logError(`Invalid id in container configuration. This error is ignored to continue to start other container.`)
                return cb && cb() // 'status.failure.invalid_id'
            }

            var name = _.get( conf, 'name' )
            if( _.isNil(name) ){
                self.logWarn(`No name is provided to launch the container. Use name = ${id} instead`)
                name = id
            }
            self.logInfo(`Launching container named ${name} ...`)

            // TODO: Anyway to use `executors.exec()` ?
            // install the status listener
            var startError = null
            var listenDone = function(message) {
                if ( !_.isNil(startError) ) {
                    self.logError(`Can NOT ask PM2 to start container named ${name}. This error is ignored to continue to start other container.`)
                    return cb && cb()
                }
                // err will always be null
                var result = _.get(message, 'params.status', 'status.offline')
                if (result === 'status.online') {
                    self.logInfo(`Successfully launched container named ${name}`)
                    containerLaunched += 1
                    self._distributor.addContainer(name)
                    return cb && cb()
                }

                // kill the child :(
                var stopDone = (err) => cb() // dismiss any error may have
                pm2.stop(name, stopDone) // kill via the name
            }
            self.listenOnce(util.pathJoin('event/service/world/manifest', id), listenDone)

            var args = "--conf '" + JSON.stringify(conf) + "'"
            var script = util.pathJoin(__dirname, 'start-container.js')
            var killTimeout = self.getTimeout() * 3
            var options = {
                script,
                name,
                args,
                kill_timeout: killTimeout
            }
            var startDone = function(err) {
                if (_.isNil(err)) {
                    return // we invoke the callback later in `listenDone`
                }
                startError = 'status.failure.pm2_start'
                self.logError(`Failed to launch container named ${name} . Reason: ${err}`)
            }
            pm2.start(options, startDone)
        }

        var launchContainers = function(cb) {
            self.logInfo(`Launching ${containerTotal} container(s)....`)
            async.eachSeries(containers, launchContainer, cb)
        }

        var done = function(err) {
            pm2.disconnect() // still we need to disconnect
            if (err) {
                self.logError('Failed to initialize World. Reason: ' + err)
            } else {
                self.logInfo(`Number of container(s) launched: ${containerLaunched}`)
                self.logInfo('Started successfully')
            }
            callback(err)
        }

        var pm2connect = function(cb) {
            pm2.connect((err) => cb(err))
        }

        async.waterfall(
            [pm2connect, launchContainers],
            done
        )

    }

    // Stop this World
    // @callback: [Function] function `(err, result)` to process results
    stop(callback) {
        var self = this

        var stopContainer = function(name, cb) {
            self.logInfo(`Stopping container named ${name} ...`)
            var stopDone = (err) => {
                if(err){
                    self.logError(`Error when stopping the container. Reason: ${JSON.stringify(err)}. This error is ignored to stop other containers.`)
                }
                cb()
            }
            // contains will stop gracefully
            return pm2.stop(name, stopDone)
        }

        var stopContainers = function(cb) {
            var stopDone = (err) => cb() // Discard any error
            var containers = self._distributor.getContainers()
            async.eachSeries( containers, stopContainer, stopDone )
        }

        var pm2connect = function(cb) {
            pm2.connect((err) => cb(err))
        }

        var stopDone = function(err) {
            configurator.dispose()
            if (err) {
                self.logError(`Failed to stop. Reason: ${JSON.stringify(err)}` )
            } else {
                self.logInfo('Stopped successfully')
            }

            // TODO: Review this decision, coz all processes monitored by PM2 will be terminated as well
            pm2.delete('all', (err) => {
                pm2.disconnect() // still we need to disconnect
                callback && callback(err)
            })
        }

        async.waterfall(
            [
                pm2connect, stopContainers, super.stop.bind(this)
            ],
            stopDone
        )
    }

}

WorldService.path = __filename

module.exports = WorldService

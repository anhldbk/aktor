'use strict'

var crypto = require('crypto')
var ALGORITHM = 'aes-256-ctr'

class Cipher{
    static encrypt(text, password){
      var cipher = crypto.createCipher( ALGORITHM,password )
      var crypted = cipher.update(text,'utf8','hex')
      crypted += cipher.final('hex');
      return crypted;
    }

    static decrypt(text, password){
      var decipher = crypto.createDecipher( ALGORITHM,password )
      var dec = decipher.update(text,'hex','utf8')
      dec += decipher.final('utf8');
      return dec;
    }
}

module.exports = Cipher

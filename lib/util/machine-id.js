'use strict'

const os = require('os')
const crypto = require( 'crypto' )
const _ = require('lodash')

// Get MAC addresses of public interfaces
// @return: List of addresses
function getPublicInterfaces(){
        // get network interfaces
        var interfaces = os.networkInterfaces()
        interfaces =  _.values(interfaces)
        var mapOp = function(iface){
                // only get public interfaces
                if( _.isNil(iface.internal) || iface.internal == true){
                        return null
                }
                return iface.mac
        }
        interfaces = _.flatten(interfaces)
        interfaces = _.map(interfaces, mapOp)

        var filterOp = function(iface){
                return iface !== null
        }
        interfaces = _.filter(interfaces, filterOp)
        return interfaces
}

// Get models of CPU cores
// @return: List of models
function getCpuModels(){
        var cpus = os.cpus()
        var mapOp = function(cpu){
                return cpu.model
        }
        cpus = _.map(cpus, mapOp)
        return cpus
}

// Get Machine Id
// @return: Unique Id string for the machine
function getMachineId(){
        var interfaces = getPublicInterfaces()
        var cpus = getCpuModels()
        var uid = _.join(interfaces, '#') + _.join(cpus, '#')
        uid = crypto.createHash( 'sha1' ).update( uid ).digest( 'HEX' )
        return uid
}

module.exports = getMachineId

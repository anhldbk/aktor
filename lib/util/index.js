'use strict'
const _ = require('lodash')
const async = require('async')
const crypto = require('crypto');
const uuid = require('uuid')
const net = require('net')
const util = require('util')
const path = require('path')
const calls = require('./calls')
const callout = calls.callout
const callsync = calls.callsync
const Cipher = require('./cipher')
const Binder = require('./binder')
const getMachineId = require('./machine-id')
const moment = require('moment')

// Check if a port is opened
// @param port: The port to check
// @param callback: callback function
function isPortOpened(port, callback) {
  var server = net.createServer(function(socket) {
      socket.write('Echo server\r\n')
      socket.pipe(socket)
  })

  server.listen(port, '127.0.0.1')
  server.on('error', function(e) {
      callback(null, true)
  })
  server.on('listening', function(e) {
      server.close()
      callback(null, false)
  })
}

// Check if a port is opened synchronously
// @param port: The port to check
// @return: Returns true if the port's opened. Otherwise, returns false.
function isPortOpenedSync(port){
  return callsync( isPortOpened, null, [ port ] )[1]
}

// Check if any field of an object is nil
// @param obj: The object
// @param fields: List of files. For example: ['owner.id', 'owner.token']
// @return Returns true if there's an undefined field. Otherwise, returns false.
function isAnyNil(obj, fields){
  if( _.isNil(obj) ){
    return true
  }

  if( ! _.isObject(obj) || ! _.isArray(fields) ){
    return false
  }

  var value
  for(var i = 0; i < fields.length; i++){
    value = _.get( obj, fields[i], null)
    if ( _.isNil( value ) ){
      return true
    }
  }
  return false
}

function getSha256Hash(input) {
    return crypto.createHash('sha256')
        .update(input)
        .digest('hex');
}

// Parse an input into a JSON object
// @param input: a JSON-encoded string
// @return: If success, returns the object. Otherwise, returns null
function parseJson(input){
  try{
    return JSON.parse(input);
  } catch(e){
    // malformed header detected
    return null;
  }
}

// Rename a field of an object
// @param obj: The object
// @param oldField: The field to rename
// @param newField: Name of the new field
// @return The new object
function renameField(obj, oldField, newField){
  if( ! _.isObject(obj) ){
    return obj
  }

  var value = _.get(obj, oldField, null)
  _.unset(obj, oldField)
  _.set(obj, newField, value)

  return obj
}

// Get a random id
// @return [String] the id
function getRandomId(){
  return uuid.v4()
}

// Get the current time in Unix time epoch
// @return [Integer] The time
function getCurrentTime(){
  return (new Date).getTime()
}

// Get the log time (suitable for logging)
// @return [String] the time

function getLogTime() {
  return moment().format('HH:mm:ss DD/MM/Y')
}

// Get the faulty module from an exception
// @param e: [Exception] An Exception
// @return: [Array of Strings] Returns an array of the absolute path of the faulty one & the associated line index if success. Otherwise, returns null.
function getFaultyModule(e){
    if(!e || !e.stack){
      return null
    }
    var faulty = e.stack.split('\n')[1]
    var start = faulty.indexOf('/')
    faulty = faulty.substring(start, faulty.length)
    faulty = faulty.split(':')
    faulty.pop()
    var line =faulty.pop()
    return [ faulty.join(':'), line]
}

var bindThis = Binder.bind

module.exports = {
    isPortOpened,
    isPortOpenedSync,
    isAnyNil,
    stringFormat: util.format,
    pathJoin: path.join,
    parseJson,
    callsync,
    callout,
    renameField,
    getSha256Hash,
    getRandomId,
    getCurrentTime,
    getFaultyModule,
    getMachineId,
    getLogTime,
    Cipher,
    Binder,
    bindThis
}

'use strict'
const _ = require('lodash')
const deasync = require('deasync')
// Module from an unmaintained project: https://github.com/jmar777/cb
// Callout = Callback with timeout
// @param callback: [Function] The original callback
// @param ms: [Integer, optional, default = 10000] timeout in milliseconds
function callout(callback, ms) {
  !ms && (ms = 10000) // default timeout: 10s
  if(ms < 0){
    throw 'Invalid timeout'
  }

  var callout = function() {
    if (timedout) return
    tid && clearTimeout(tid)

    var args = Array.prototype.slice.call(arguments)
    process.nextTick(function() {
      return callback.apply(this, args)
    })
  }

  var watchdog = function() {
    callout('status.failure.timeout')
    timedout = true
  }

  var timedout = false

  var tid = setTimeout(watchdog, ms)

  return callout
}

// Make an asynchronous function to be synchronous
// @param fnAsync: A function `(arg1, arg2, ..., argn, callback)`
// @parm context: Context for the function to bind to
// @param args: [Optional] List of arguments excluding the callback, [arg1, arg2..., argn]
// @return: Returns [err, result] which are parameters passed to the callback
function callsync(fnAsync, context, args){
  if(! _.isFunction(fnAsync)){
      throw new Error('Invalid function')
  }
  if( ! _.isNil(args) ){
      if( ! _.isArray(args) ) {
          throw new Error('Invalid arguments')
      }
  } else {
      args = []
  }
  var result = null
  var loopStep = function(){
      return result === null
  }
  var done = function(err, res){
      result = [err, res]
  }
  args.push(done)
  fnAsync.apply(context, args)
  deasync.loopWhile(loopStep)
  return result
}

module.exports = {
  callout,
  callsync
}

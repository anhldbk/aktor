state = {
    owner: {
        id: 'user/id',
        name,
        activatedTime : "time at which the system is activated, string",

        location: [
            {
                id: 'home', // always have
                name
                //TODO: image?
            },
            {
                // users can add more location
            }
        ],

        history: [
            {
                id,
                type: 'action.*', // or 'event.*'
                location: {
                    id,
                    name
                },
                message : '',
                time: 'unix timestamp'
            }
        ],

        home: {
            next: { // next scheduled action
                type: 'action.stage.start_scene',
                time,
                params: {
                    scene: 'home.arm'
                }
            },
            devices: [
                // array of installed devices
                // describing via descriptors
            ],
            scenes: [
                // scenes in the same groups are mutually exclusive
                {   // scene descriptor
                    id: 'home.arm',
                    // home scene can not be deleted
                    location: 'home', // id of location

                    schedule: [
                        {
                            start: 'cron-like strings which supported by later.js',
                            stop: 'cron-like strings which supported by later.js',
                            // you may use package `cron-parser` to parse the strings
                            // TODO: exception?
                        }
                    ],

                    actions: {
                        main: {
                            // user actions put here
                        },
                        start: {
                            // system actions for notifying
                        },
                        stop: {
                            // system actions for notifying
                        }
                    },

                    triggers: [
                        {
                            select: {
                                // sift.js query from event streams
                            },
                            if: `a js condition`,
                            then: [ // a serie of actions to take
                                {
                                    id, // id may be omitted
                                    type, // if type is unique
                                    params
                                }
                            ]
                        }
                    ]

                },
                {
                    id: 'home.disarm',
                    location: 'home'
                },
                {
                    id: 'home.indoor',
                    location: 'home'
                },
                {
                    id: 'home.sleep',
                    location: 'home'
                }
            ]
        },
    },

    system: {
        id: 'system/id',

        model: {
            name: "Evolas One",
            code: "EVO68",
            link: "http://evolas.vn/products/evo68"
        },

        platform: {
            name: "Evolas OS",
            version: "1.0.0",
            link: "http://evolas.vn/products/evos"
        },

        "upTime": "up time, string",
        "currentTime": "the current system time, string",

        "storage": {
            "total": "memory size, string",
            "used": "used memory, string",
            "available": "available memory, string",
            "use" : "percent of used memory, number"
        },
        "memory": {
            "total": "memory size, string",
            "used": "used memory, string",
            "available": "available memory, string",
            "use" : "percent of used memory, number"
        },
        "power": {
            "blackout": "true or false. true if there's a electric power outage",
            "durationTime": "time in ms that the system can work by using battery",
            "elapsedTime": "elapsed time in ms since there's a blackout"
        },
        "gsm" : {
            "state": "state.{connected, disconnected}",
            "network": "viettel",
            "signal": "signal.{poor, fair, good, excellent}",
            "phoneNumber": "0987xyz",
            "balance": "1000 VND"
        },
        "wifi": {
            "state": "state.{connected, disconnected, broadcasting}",
            "network": "xyz.com",
            "ip": "127.0.0.1"
        },
        "knowledgebase": {
            "devices": [
                // array of knowledgeable device descriptors
            ]
        }
    },
}

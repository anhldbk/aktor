var scene = {
    name, // name of the scene, unique

    schedule: [
        {
            start: 'cron-like strings which supported by later.js',
            stop: 'cron-like strings which supported by later.js'
            // you may use package `cron-parser` to parse the strings
        }
    ],

    actions: {
        start: [ // registers actions to start
            {
                endpoint: 'service/speaker/:request/beep',
                params: {
                    beep: 3
                }
            }
        ],

        started: [

        ],

        stop: [

        ],

        stopped: [

        ]

    },

    triggers: [
        { // trigger
            // using json5 to use unquoted keys
            select: `{
                eventName: { sift-query-here }, // json string, using sift.js for query
                // eventName then can be used by "if" clause
                // for example
                temperatureEvent: { event.type : { $eq: 'event.sensor.fire.on'}}
            }`,

            if: `!eventName1 && temperatureEvent.data.temperature  > 30`,

            // event can be a string which is a function to evaluate
            // (event, callback) => { callback(null, true | false ) }. false to reject events
            then: `[
                {
                    action: 'service/speaker/:request/beep',
                    params: {
                        time: 3
                    }
                },
                {
                    action: 'service/sms/:request/sms',
                    params: {
                        to: '09xxx',
                        message: 'Hey Jude'
                    }
                },
                {
                    action: 'service/stage/:request/start_scene',
                    params: {
                        name: 'arm'
                    }
                }
            ]`

        }
    ]

}

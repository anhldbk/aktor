'use strict'
const Logger = require('../lib/platform/core/logger')
const SimpleLogger = Logger.SimpleLogger
const AdvancedLogger = Logger.AdvancedLogger
const Loggable = Logger.Loggable
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect

class Aktor extends Loggable {
    constructor(conf){
        super(conf)
    }
}

describe('Tests for Logger', function(){
    it('should allow users to log with AdvancedLogger', function(done){
        const logger = new AdvancedLogger()
        var logHandler = sinon.stub(logger, "log")

        var aktor = new Aktor()
        aktor.onLogWarn( logger.warn )
        aktor.onLogDebug( logger.debug )
        aktor.onLogInfo( logger.info )
        aktor.onLogError( logger.error )
        aktor.onLogFatal( logger.fatal )

        aktor.logWarn('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'warn', 'bonjour' )

        aktor.logDebug('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'debug', 'bonjour' )

        aktor.logInfo('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'info', 'bonjour' )

        aktor.logError('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'error', 'bonjour' )

        aktor.logFatal('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'fatal', 'bonjour' )

        done()
    })

    it('should allow users to log with SimpleLogger', function(done){
        const logger = new SimpleLogger()
        var logHandler = sinon.stub(logger, "log")

        var aktor = new Aktor()
        aktor.onLogWarn( logger.warn )
        aktor.onLogDebug( logger.debug )
        aktor.onLogInfo( logger.info )
        aktor.onLogError( logger.error )
        aktor.onLogFatal( logger.fatal )

        aktor.logWarn('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'warn', 'bonjour' )

        aktor.logDebug('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'debug', 'bonjour' )

        aktor.logInfo('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'info', 'bonjour' )

        aktor.logError('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'error', 'bonjour' )

        aktor.logFatal('bonjour')
        sinon.assert.calledWithExactly(logHandler, 'fatal', 'bonjour' )

        done()
    })
})

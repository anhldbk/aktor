'use strict'
const Trigger = require('../../lib/services/stage/trigger')
const SimpleLogger = require('../../lib/platform/core/logger/simple')
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const sinon = require('sinon')
const _ = require('lodash')
const Rx = require('rx')

var stream = Rx.Observable.create( (observer) => {
    observer.onNext({
        type: 'event.sensor.fire',
        data: {
            fire: 1
        },
        origin: {
            from: 'device/uuid',
            endpoint: 'device/uuid/:event/update'
        }
    })

    observer.onNext({
        type: 'event.sensor.motion',
        data: {
            motion: 1
        },
        origin: {
            from: 'device/uuid1',
            endpoint: 'device/uuid1/:event/update'
        }
    })

    observer.onCompleted()

})

const  conf = {
    stream, // [Required] A Rx Observable object

    select: `{
        eventFire: {
            type: { $eq: 'event.sensor.fire'}
        },
        eventMotion: {
            type: { $eq: 'event.sensor.motion' }
        }
    }`,

    if: `eventFire.fire === 1 && eventMotion.motion === 1`,

    then: `[
        {
            id: 'service/speaker',
            type: 'action.beep',
            params: {
                time: 3
            }
        },
        {
            id: 'service/sms',
            type: 'action.send_sms',
            params: {
                to: '09xxx',
                message: 'Hey Jude'
            }
        },
        {
            id: 'service/stage',
            type: 'action.start_scene',
            params: {
                name: 'arm'
            }
        }
    ]`,

    loggers: [ // optional
        {
            script: SimpleLogger.path,
            name: 'trigger/1'
        }
    ]
}

var trigger = null

describe('Tests for triggers', function(){
    before(function(done){
        trigger = new Trigger(conf)
        done()
    })

    it('should allow users to start triggers', function(done){
        var handler = sinon.spy()
        trigger.onAction( handler )
        trigger.start( function(err){
            should.not.exist(err)
            sinon.assert.calledThrice(handler)
            done()
        } )
    })

    it('should allow users to stop triggers', function(done){
        trigger.stop( function(err){
            should.not.exist(err)
            done()
        })
    })
})

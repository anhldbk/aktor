'use strict'

const sinon = require('sinon')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect
const ParseStorage = require('../../lib/platform/core/storage').ParseStorage

var conf = {
    username: 'bot',
    password: '123456'
}

describe('Tests for ParseStorage', function(){
    var storage = new ParseStorage(conf)

    before(function(done){
        // initialize first
        storage.init(function(err, result){
            done(err)
        })
    })

    it('should allow users to store key-value data', function(done){
        storage.set('platform', 'windows', function(err, result){
            done(err)
        })
    })

    it('should NOT accept invalid keys', function(done){
        storage.set('platform.os', 'windows', function(err, result){
            if(err){
                return done() // yeah
            }
            done()
        })
    })

    it('should allow users to store data synchronously', function(done){
        var res = storage.setSync('updated', true) // res = [err, result]
        done(res[0])
    })

    it('should allow users to get pre-defined data via keys', function(done){
        storage.get('platform', function(err, result){
            should.not.exist(err)
            expect(result).to.equal('windows')
            return done()
        })
    })

    it('for undefined keys, should return undefined values', function(done){
        storage.get('price', function(err, result){
            should.not.exist(err)
            should.not.exist(result)
            return done()
        })
    })

    it('should allow users to get data synchronously', function(done){
        var res = storage.getSync('updated')
        expect(res[1]).to.equal(true)
        done(res[0])
    })

    it('should allow users to unset data via keys', function(done){
        storage.unset('platform', function(err, result){
            done(err)
        })
    })

    it('should allow users to unset data via keys synchronously', function(done){
        var res = storage.unsetSync('updated')
        done(res[0])
    })

    it('should allow users to add items into an array', function(done){
        var key = 'amount'
        var err = storage.addSync(key, 1 )[0]
        should.not.exist(err)
        err = storage.addSync(key, 2 )[0]
        should.not.exist(err)

        // recheck
        var res = storage.getSync(key)
        should.not.exist( res[0] )
        res = res[1]
        expect(res).to.deep.equal([1,2])

        done()
    })

    it('should allow users to remove items of an array', function(done){
        var key = 'amount'
        var err = storage.removeSync(key, 1 )[0]
        should.not.exist(err)

        // recheck
        var res = storage.getSync(key)
        should.not.exist( res[0] )
        res = res[1]
        expect(res).to.deep.equal([2])

        res = storage.removeSync('non-existing-key', 1 )
        err = res[0]
        should.not.exist(err)

        done()
    })

    // TODO: Tests for offline processing

    it('should allow users to destroy the whole storage', function(done){
        var res = storage.destroySync()
        done(res[0])
    })

    after(function(done){
        storage.dispose(function(){
            done()
        })
    })
})

'use strict'

const sinon = require('sinon')
const chai = require('chai')
const should = chai.should
const expect = chai.expect
const TimeSeriesStorage = require('../../lib/platform/core/storage').TimeSeriesStorage

var conf = {
    username: 'device/123456',
    password: '123456'
}

describe('Tests for TimeSeriesStorage', function(){
    var storage = new TimeSeriesStorage(conf)

    before(function(done){
        // initialize first
        storage.init(function(err, result){
            done(err)
        })
    })

    it('should allow users to store key-value data', function(done){
        storage.set('temperature', 1, function(err, result){
            done(err)
        })
    })

    it('should allow users to store data synchronously', function(done){
        var res = storage.setSync('temperature', 0) // res = [err, result]
        done(res[0])
    })

    it('should allow users to get pre-defined data via keys', function(done){
        storage.get('temperature', function(err, result){
            if(err){
                return done(err)
            }
            expect(result[0].value).to.equal(1)
            expect(result[1].value).to.equal(0)
            return done()
        })
    })

    it('for undefined keys, should return blank arrays []', function(done){
        storage.get('price', function(err, res){
            if(err){
                return done(err)
            }
            expect(res.length).to.equal(0)
            return done()
        })
    })

    it('should allow users to get data synchronously', function(done){
        var res = storage.getSync('temperature')
        expect(res[1].length).to.equal(2)
        done(res[0])
    })

    it('should allow users to unset data via keys', function(done){
        storage.unset('temperature', function(err, result){
            // try to verify
            var res = storage.getSync('temperature')
            expect(res[1].length).to.equal(0)
            done(res[0])
        })
    })

    after(function(done){
        storage.dispose(function(){
            done()
        })
    })
})

package main

import (
    "fmt"
    "os"
    "os/signal"
    "github.com/Jeffail/gabs"

    "github.com/yosssi/gmq/mqtt"
    "github.com/yosssi/gmq/mqtt/client"
    "github.com/jessevdk/go-flags"
)

type Options struct {
    ID string `long:"id" description:"ID" required:"true"`
    Token string `long:"token" description:"Token" required:"true"`
    Host string `long:"host" description:"Broker's host" optional:"yes" default:"localhost"`
    Port string `long:"port" description:"Broker's port" optional:"yes" default:"1883"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
    if _, err := parser.Parse(); err != nil {
		os.Exit(1)
	}

    // Set up channel on which to send signal notifications.
    sigc := make(chan os.Signal, 1)
    signal.Notify(sigc, os.Interrupt, os.Kill)

    // Create an MQTT Client.
    cli := client.New(&client.Options{
        // Define the processing of the error handler.
        ErrorHandler: func(err error) {
            fmt.Println(err)
        },
    })

    // Terminate the Client.
    defer cli.Terminate()
    fmt.Println(options.Host + ":" + options.Port)
    // Connect to the MQTT Server.
    err := cli.Connect(&client.ConnectOptions{
        Network:  "tcp",
        Address:  options.Host + ":" + options.Port,
        UserName:        []byte( options.ID ),
        Password:        []byte(  options.Token ),
        ClientID: []byte(  options.ID ),
    })
    if err != nil {
        panic(err)
    }
    fmt.Println("Connected")

    // Publish a message.
    fmt.Println("Publishing to event/service/world/manifest/" + options.ID + "...")
    err = cli.Publish(&client.PublishOptions{
        QoS:       mqtt.QoS0,
        TopicName: []byte( "event/service/world/manifest/" + options.ID ),
        Message:   []byte("{\"params\":{\"status\":\"status.online\"}}"),
    })

    fmt.Println("Subscribing to " + "action/" + options.ID +  "/bonjour ..." )
    err = cli.Subscribe(&client.SubscribeOptions{
        SubReqs: []*client.SubReq{
            &client.SubReq{
                TopicFilter: []byte( "action/" + options.ID +  "/bonjour"),
                QoS:         mqtt.QoS0,
                // Define the processing of the message handler.
                Handler: func(topicName, message []byte) {
                    i := 0
                    for{
                        if message[i] == '\n' {
                            break
                        }
                        i += 1
                    }

                    headerParsed, err := gabs.ParseJSON(message[:i])
                    if err != nil {
                        // handle the error
                        return
                    }
                    bodyParsed, err := gabs.ParseJSON(message[i+1:])
                    if err != nil {
                        // handle the error
                        return
                    }

                    from := headerParsed.Path("from").Data().(string)
                    id := bodyParsed.Path("header.id").Data().(string)

                    to := from
                    response := "{\"request\": {\"header\": { \"from\": \"Fake to bypass\", \"id\": \"" + id + "\" }},\"response\":{\"message\":\"bonjour\"}}"
                    cli.Publish(&client.PublishOptions{
                        QoS:       mqtt.QoS0,
                        TopicName: []byte( to ),
                        Message:   []byte(response),
                    })
                },
            },
        },
    })
    if err != nil {
        panic(err)
    }

    fmt.Println("Travelling...")
    if err != nil {
        panic(err)
    }

    // Wait for receiving a signal.
    fmt.Println("Waiting...")
    <-sigc
    // // Disconnect the Network Connection.
    // if err := cli.Disconnect(); err != nil {
    //     panic(err)
    // }
}

// package main
// import (
//     "fmt"
//     "github.com/Jeffail/gabs"
// )
// func main()  {
//     message:= "{\"from\":\"ping\",\"timestamp\":1471409799770,\"ip\":\"127.0.0.1\",\"port\":60044}\n{\"message\":\"hi\"}"
//     messageByte := []byte(message)
//     i := 0
//     for{
//         if messageByte[i] == '\n' {
//             break
//         }
//         i += 1
//     }
//     headerParsed, err := gabs.ParseJSON(messageByte[:i])
//     fmt.Println("Data is here", err, headerParsed.Path("from").Data().(string))
// }

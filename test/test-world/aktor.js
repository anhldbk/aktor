'use strict'
const CommonAktor = require('../../lib/platform/core/aktor').CommonAktor
const _ = require('lodash')

class Aktor extends CommonAktor{
    constructor(conf){
        super(conf)
    }

    _init(callback){
        var options = {
            'action/bonjour': 'onBonjour',
            'action/error': 'onError'
        }
        this.listenBundle(options, callback)
    }

    onBonjour(message){
        this.logInfo( 'Receiving a message of ' + JSON.stringify(message) )
        this.reply(message, { message: 'bonjour' })
    }

    onError(message){
        // just stimulate an unhandled exception
        throw new Error('Hello there')
    }
}

module.exports = Aktor

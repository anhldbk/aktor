'use strict'

const sinon = require('sinon')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect
const WorldService = require('../../lib/platform/world')
const async = require('async')
const _ = require('lodash')
const path = require('path')

// IMPORTANT!!!!!!!!!!!!!!!!!!!!!!
// Configure `mocha.opts` with `--timeout 40000`
describe('Tests for World', function(){
    var world = null

    before(function(done){
        world = new WorldService()
        world.start(done)
    })

    it('should allow users to launch a NodeJS aktor', function(done){

        var params = { // no token is provided
            id: 'ping',
            maxRestarts: 2,
            script: path.join(__dirname, 'aktor')
        }

        world.ask( 'action/service/world/aktor_start', {params}, function(err, result){
            should.not.exist( err )

            // just check to see if the aktor's actually running.
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.not.exist(err)
                result =  _.get(result, 'response.message')
                expect( result ).to.equal('bonjour')
                done()
            })

        })
    })

    it('should allow users to stop a launched NodeJS aktor', function(done){
        var params = {
            id: 'ping'
        }

        world.ask( 'action/service/world/aktor_stop', {params}, {timeout: 15000}, function(err, result){
            should.not.exist( err )

            // just check to see if the aktor's actually running.
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.exist(err)
                done()
            })

        })
    })

    it('should allow users to destroy an already-stopped aktor', function(done){
        var params = {
            id: 'ping'
        }
        world.ask( 'action/service/world/aktor_destroy', {params}, function(err, result){
            should.not.exist( err )

            // just check to see if the aktor's actually running.
            world.ask( 'action/service/world/aktor_list', function(err, result){
                should.not.exist( err )
                var aktors = result.response.aktors
                var index = _.findIndex(aktors, function(aktor) { return aktor.id == 'ping' })
                expect(index).equal(-1)
                done()
            })

        })
    })

    it('should allow users to launch a non-Nodejs aktor', function(done){

        var params = {
            id: 'pong',
            token: '123456',
            script: path.join(__dirname, 'go/aktor')
        }

        world.ask( 'action/service/world/aktor_start', {params}, function(err, result){
            should.not.exist( err )

            // just check to see if the aktor's actually running.
            world.ask( 'action/pong/bonjour', { message: 'hi'}, function(err, result){
                should.not.exist(err)
                result =  _.get(result, 'response.message')
                expect( result ).to.equal('bonjour')
                done()
            })

        })
    })


    it('should allow users to get running aktors', function(done){
        world.ask( 'action/service/world/aktor_list', function(err, result){
            should.not.exist( err )
            var aktors = result.response.aktors
            var index = _.findIndex(aktors, function(aktor) { return aktor.id == 'pong' })
            expect(index).not.equal(-1)
            done()
        })
    })

    it('again, should allow users to launch a NodeJS aktor', function(done){

        var params = {
            id: 'ping',
            token: '123456',
            maxRestarts: 2,
            script: path.join(__dirname, 'aktor')
        }

        world.ask( 'action/service/world/aktor_start', {params}, function(err, result){
            should.not.exist( err )

            // just check to see if the aktor's actually running.
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.not.exist(err)
                result =  _.get(result, 'response.message')
                expect( result ).to.equal('bonjour')
                done()
            })

        })
    })

    it('if encounter a bad aktor (with unhandled exceptions), containers should restart itself, increase faulty counts', function(done){
        world.ask( 'action/error', { message: 'hi'}, {timeout: 25000}, function(err, result){
            should.exist(err)

            // just check to see if the aktor's actually running after being restarted
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.not.exist(err)
                result =  _.get(result, 'response.message')
                expect( result ).to.equal('bonjour')
                done()
            })

        })
    })

    it(' 2nd time, if encounter a bad aktor (with unhandled exceptions), containers should restart itself, increase faulty counts', function(done){
        world.ask( 'action/error', { message: 'hi'}, {timeout: 25000}, function(err, result){
            should.exist(err)

            // just check to see if the aktor's actually running after being restarted
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.not.exist(err)
                result =  _.get(result, 'response.message')
                expect( result ).to.equal('bonjour')
                done()
            })

        })
    })

    it('should allow users to get running aktors, check for its faulty count', function(done){
        world.ask( 'action/service/world/aktor_list', function(err, result){
            should.not.exist( err )
            var aktors = result.response.aktors
            var index = _.findIndex(aktors, function(aktor) { return aktor.id == 'ping' })
            expect(index).not.equal(-1)
            expect(aktors[index].faulty).to.equal(2)
            done()
        })
    })

    it('for the 3rd time, the bad aktor should be prohibited from starting', function(done){
        var handler = sinon.spy()
        world.listen('event/service/world/aktor_faulty', handler)
        world.ask( 'action/error', { message: 'hi'}, {timeout: 25000}, function(err, result){
            should.exist(err)
            sinon.assert.calledOnce(handler)

            // just check to see if the aktor's actually running after being restarted
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.exist(err)
                done()
            })

        })
    })

    it('should allow users to stop a launched non-NodeJS aktor', function(done){
        var params = {
            id: 'pong'
        }

        world.ask( 'action/service/world/aktor_stop', {params}, {timeout: 25000}, function(err, result){
            should.not.exist( err )

            // just check to see if the aktor's actually running.
            world.ask( 'action/bonjour', { message: 'hi'}, function(err, result){
                should.exist(err)
                done()
            })

        })
    })

    after(function(done){
        var end = (err) => {
            setTimeout( function(){
                done(err) // wait for other aktors to terminate fully
            }, 5000)
        }
        world.stop(end)
    })
})

'use strict'
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const _ = require('lodash')

const RoundDistributor = require('../../lib/platform/environ/distributors/round-distributor')

describe('Tests for RoundDistributor', function(){
    var containers = [
        'container-1',
        'container-2',
        'container-3'
    ]
    var distributor = new RoundDistributor(containers)

    it('should allow users to get assigned containers in round-robin fashion', function(done){
        var container = distributor.get('aktor-1')
        expect(container).to.equal('container-1')

        container = distributor.get('aktor-2')
        expect(container).to.equal('container-2')

        container = distributor.get('aktor-3')
        expect(container).to.equal('container-3')

        container = distributor.get('aktor-4')
        expect(container).to.equal('container-1')

        done()
    })

    it('should return the assined container for an assigned aktor', function(done){
        var container = distributor.get('aktor-1')
        expect(container).to.equal('container-1')
        done()
    })

    it('should allow users to check if an aktor is already assigned', function(done){
        var assigned = distributor.exist('aktor-1')
        expect(assigned).to.equal(true)
        assigned = distributor.exist('aktor-11')
        expect(assigned).to.equal(false)
        done()
    })

    it('should allow users to remove an assigned aktor', function(done){
        var assigned = distributor.remove('aktor-1')
        expect(assigned).to.equal(true)
        assigned = distributor.remove('aktor-11')
        expect(assigned).to.equal(false)
        done()
    })

    it('should allow users dynamically add new containers', function(done){
        var res = distributor.addContainer('container-4')
        expect(res).to.equal(true)
        done()
    })

    it('should allow users dynamically remove containers', function(done){
        var res = distributor.removeContainer('container-3')
        expect(res).to.equal(true)
        // aktor-3 is removed as well
        res = distributor.exist('aktor-3')
        expect(res).to.equal(false)
        done()
    })

    it('should allow users to add a distribution', function(done){
        var aktor = 'ping'
        var container = 'service/containter/1'
        var res = distributor.addDistribution(aktor, container)
        expect(res).to.equal(true)
        res = distributor.get(aktor)
        expect(res).to.equal(container )
        done()
    })

})

'use strict'

const sinon = require('sinon')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect
const configurator = require('../../lib/platform/environ/configurator')
const async = require('async')
const _ = require('lodash')
const path = require('path')

describe('Tests for Environ configurator ', function(){
    var environ = null

    it('should allow users to get configuration for Environ', function(done){
        var result = configurator.getEnvironConf(),
            err = result[0],
            conf = result[1]

        should.not.exist( err )
        should.exist( conf.id )
        done()
    })

    it('should allow users to get configuration for aktors', function(done){
        var result = configurator.getAktorConf('pong'),
            err = result[0]

        result = result[1]
        should.not.exist(err)
        should.exist( result )
        expect(result.id).to.equal('pong')
        done()
    })

})

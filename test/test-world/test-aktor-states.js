'use strict'

const sinon = require('sinon')
const chai = require('chai')
const should = chai.should()
const expect = chai.expect
const AktorStates = require('../../lib/platform/environ/container/aktor-states')
const async = require('async')
const _ = require('lodash')
const path = require('path')

describe('Tests for AktorStates', function(){
    var states = new AktorStates()
    var id1 = 'service/1'
    var id2 = 'service/2'
    it('should allow users to createStart', function(done){
        var res = states.createStart(id1)
        console.log(res)
        should.exist(res)
        res = states.started(id1)
        expect(res).to.equal(true)
        res = states.error(id1)
        expect(res).to.equal(true)
        done()
    })

    it('should allow users to createStart', function(done){
        var res = states.createStopped(id2)
        should.exist(res)
        res = states.stop(id2)
        expect(res).to.equal(false)
        console.log( states.get(id2) )
        done()
    })
})

'use strict'
const Loggable = require('../lib/platform/core/logger').Loggable
const logger = new Loggable()
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect

var handler = sinon.spy()

describe('Tests for Loggable', function(){
    it('should allow users to register handlers for a valid log level', function(done){
        var result = logger.onLog(handler)
        expect(result).to.equal(true)
        done()
    })

    it('should NOT allow users to register handlers for a INVALID log level', function(done){
        var result = logger.onLog('log/all', handler)
        expect(result).to.equal(false)
        done()
    })

    it('should allow users to write logs', function(done){
        logger.logInfo('hi')
        process.nextTick(function(){
            sinon.assert.calledWithExactly(handler, 'hi')
            done()
        })
    })
})

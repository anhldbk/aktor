var resources = require('../../lib/system/share/resources')
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect
const should = chai.should()

describe('Tests for system resources', function(){
    it('should allow users to get memory state', function(done){
        resources.getStorageState( function(err, result){
            should.not.exist(err)
            should.exist(result)
            should.exist(result.used)
            should.exist(result.use)
            should.exist(result.total)
            should.exist(result.available)
            done()
        })
    })

    it('should allow users to get total storage state', function(done){
        resources.getStorageState( function(err, result){
            should.not.exist(err)
            should.exist(result)
            should.exist(result.used)
            should.exist(result.use)
            should.exist(result.total)
            should.exist(result.available)
            done()
        } )
    })

    it('should allow users to get states about all resources', function(done){
        resources.getResourcesState( function(err, result){
            should.not.exist(err)
            should.exist(result)
            should.exist(result.memory)
            should.exist(result.storage)            
            done()
        } )
    })
})

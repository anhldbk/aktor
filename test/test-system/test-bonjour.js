'use strict'
const os = require('os')
const _ = require('lodash')
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const BonjourService = require('../../lib/system/bonjour')
const async = require('async')
const dgram = require('dgram')

function getIPs() {
    // get network interfaces
    var interfaces = os.networkInterfaces()
    interfaces = _.values(interfaces)
    var mapOp = function(iface) {
        // only get public interfaces
        if (_.isNil(iface.internal) || iface.internal == true) {
            return null
        }
        return iface.address
    }
    interfaces = _.flatten(interfaces)
    interfaces = _.map(interfaces, mapOp)

    var filterOp = function(iface) {
        // not null and IPv6
        return (iface !== null) && (iface.indexOf(':') == -1)
    }
    interfaces = _.filter(interfaces, filterOp)
    return interfaces
}

function getBroadcastIP(){
    var ip = getIPs()[0] // get the first one. Bonjour by default listening on all interfaces.
    var octets = ip.split('.')
    octets[3] = '255'
    ip = octets.join('.')
    return ip
}

function getBonjourClient(callback) {
    var client = dgram.createSocket("udp4")
    client.bind()

    client.on('listening', function() {
        client.setBroadcast(true)
        callback && callback(null, client)
    })
}

describe('Tests for Bonjour service', function() {
    var bonjourd = new BonjourService()
    var broadcastIP = getBroadcastIP()

    it('should allow users to start Bonjour service', function(done) {
        var discover = function(err, client){
            should.not.exist(err)

            // register handler
            client.on('message', function(message, client) {
                should.exist(message)
                message = JSON.parse(message.toString())
                should.exist( message.state )
                done()
            })

            var message = JSON.stringify({ tell: 'bonjour' })
            client.send(message, 0, message.length, 8886, broadcastIP)
        }

        async.waterfall(
            [
                bonjourd.start,
                getBonjourClient
            ],
            discover
        )
    })

    it('should allow users to stop the service', function(done){
        bonjourd.stop(function(err){
            should.not.exist(err)
            done()
        })
    })
})

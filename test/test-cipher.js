'use strict'
const Cipher = require('../lib/util').Cipher
const chai = require('chai')
const expect = chai.expect
const should = chai.should()

describe('Tests for Cipher', function(){
    var password = 'password'
    var plain = 'data'
    var output = ''

    it('should allow users encrypt data', function(done){
        output = Cipher.encrypt( plain, password )
        expect(output).not.to.equal( plain )
        done()
    })

    it('should allow users decrypt data', function(done){
        output = Cipher.decrypt( output, password )
        expect(output).to.equal( plain )
        done()
    })

    it('should return blank string for un-encrypted input', function(done){
        output = Cipher.decrypt( 'test', password )
        expect(output).to.equal( '' )
        done()
    })

})

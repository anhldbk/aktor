'use strict'

const PlatformIdentity = require('../../lib/platform/identity').PlatformIdentity
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const sinon = require('sinon')
const _ = require('lodash')
const util = require('../../lib/util')

const identity = new PlatformIdentity()
const username = 'bigsonata'
const password = 'hey hey hey'
const newPassword = 'yo yo yo'
var permissions = { publish: ['#'] }
describe('Tests for PlatformIdentity', function(){

    before(function(done){
        var err = identity.initSync()[0]
        should.not.exist(err)
        done()
    })

    it('should allow users to create/update', function(done){
        identity.upsert(username, permissions , function(err, result){
            should.not.exist(err)
            should.exist(result)
            expect(result.id).to.equal(username)

            // using the synchronous version of `create`
            // we can create without using passwords
            result = identity.createSync(username, password)
            should.not.exist(result[0]) // still successful and get configurations back
            result = result[1]
            expect( result.id ).to.equal(username)

            // upsert
            result = identity.upsertSync(username, null)
            should.not.exist(result[0])
            done()
        })
    })


    it('should allow users to check if a user exists', function(done){
        identity.exist(username, function(err, result){
            should.not.exist(err)
            expect(result).to.equal(true)

            result = identity.existSync('John Doe')
            should.not.exist(result[0])
            expect(result[1]).to.equal(false)
            done()
        })
    })

    it('should allow users to update information about users', function(done){
        // update password
        var err = identity.updateSync(username, newPassword)[0]
        should.not.exist(err)
        done()
    })

    it('should allow users to get information about users', function(done){
        var result = identity.readSync(username)
        should.not.exist(result[0])
        result = result[1]
        should.exist( result.mqtt )
        expect( result.mqtt.token ).to.equal( util.getSha256Hash(newPassword) )
        should.exist( result.parse )
        should.exist( result.influx )
        done()
    })

    it('should allow users to get all registered users', function(done){
        var result  = identity.readAllSync()
        should.not.exist(result[0])
        result = result[1]
        expect(result).to.be.instanceof(Array)
        done()
    })

    it('should allow users to delete a specific user', function(done){
        var err = identity.deleteSync(username)[0]
        should.not.exist(err)

        // try to delete non-existing users
        err = identity.deleteSync(username)[0]
        should.exist(err)
        done()
    })

    after(function(done){
        identity.delete(username, function(err, result){
            // we don't care about any error (we've already deleted it above )
            err = identity.disposeSync()[0]
            // should.not.exist(err)
            done()
        })
    })
})

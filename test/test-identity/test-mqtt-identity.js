'use strict'
const MqttIdentity = require('../../lib/platform/identity').MqttIdentity
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const sinon = require('sinon')
const _ = require('lodash')

const identity = new MqttIdentity()
const username = 'bigsonata'
var permissions = {
    pubsub: ['#']
}

describe('Tests for MqttIdentity', function(){
    before(function(done){
        var err = identity.initSync()[0]
        should.not.exist(err)
        done()
    })

    it('should allow users to create/update', function(done){
        identity.upsert(username, '123456', function(err, result){
            should.not.exist(err)

            // using the synchronous version of `create
            err = identity.createSync(username, '123456')[0]
            should.exist(err)

            // upsert
            err = identity.upsertSync(username, '123456')[0]
            should.not.exist(err)

            done()
        })
    })

    it('should allow users to check if a user exists', function(done){
        identity.exist(username, function(err, result){
            should.not.exist(err)
            expect(result).to.equal(true)

            result = identity.existSync('John Doe')
            should.not.exist(result[0])
            expect(result[1]).to.equal(false)
            done()
        })
    })

    it('should allow users to update information about users', function(done){
        // identity.update(username, permissions, function(err, result){
        //     should.not.exist(err)
        //     done()
        // })
        var err =identity.updateSync(username, permissions)[0]
        should.not.exist(err)
        done()
    })

    it('should allow users to get information about users', function(done){
        var result = identity.readSync(username)
        should.not.exist(result[0])
        result = result[1]
        expect(result.id).to.equal(username)
        expect(result.permissions.pubsub).to.deep.equal(['#'])
        done()

    })

    it('should allow users to get all registered users', function(done){
        var result = identity.readAllSync()
        should.not.exist(result[0])
        result = result[1]
        result = _.findIndex(result, function(o) { return o.id == username })
        expect(result).not.to.equal(-1)
        done()
    })

    it('should allow users to delete a specific user', function(done){
        var err = identity.deleteSync(username)[0]
        should.not.exist(err)

        // try to delete non-existing users
        err = identity.deleteSync(username)[0]
        should.exist(err)
        done()

    })

    after(function(done){
        identity.delete(username, function(err, result){
            // we don't care about any error (we've already deleted it above )
            err= identity.disposeSync()[0]
            should.not.exist(err)
            done()
        })
    })
})

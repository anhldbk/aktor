'use strict'

require('./test-mqtt-identity')
require('./test-parse-identity')
require('./test-influx-identity')
require('./test-platform-identity')

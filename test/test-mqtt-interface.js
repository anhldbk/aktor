'use strict'

const MqttInterface = require('../lib/platform/core/interface').MqttInterface
const chai = require('chai')
const should = chai.should()
const expect = chai.expect
const sinon = require('sinon')

describe('Tests for Mqtt interface', function(){
    var conf = {
        username: 'ping',
        password: '123456',
        host: '127.0.0.1',
        port: 1883
    }
    var iface = new MqttInterface(conf)
    var letter = { message: 'Hello world' }

    before(function(done){
        // initialize first
        iface.connect(done)
    })

    it('should allow users to get network information', function(done){
        var info = iface.getNetworkInformation()
        expect(info.isOnline).to.equal(true)
        done()
    })

    it('should allow users to listen for messages', function(done){
        var endpoint = 'ping/:request/hi'
        var handler = (message) => {}
        iface.listen(endpoint, handler, function(err){
            should.not.exist(err)
            done()
        })
    })

    it('should allow users to send messages', function(done){
        var endpoint = 'ping/:request/bonjour'
        var handler = function(msg){
            expect(msg.header.endpoint).to.equal(endpoint)
            // console.log('Received a bonjour message')
            // done && done()
        }
        iface.listen(endpoint, handler, function(err){
                should.not.exist(err)

            iface.send(endpoint, letter, function(error){
                should.not.exist(error)
                done()
            })

        })
    })


    it('should allow users to unlisten', function(done){
        var endpoint = 'ping/:request/holla'
        iface.unlisten(endpoint, function(err){
            should.not.exist(err)
            // try to send a message to see if there's any handler registered
            iface.send(endpoint, letter, function(err){
                should.exist(err)
                done()
            })
        })
    })

    it('should allow users to disconnect', function(done){
        iface.disconnect(function(err, result){
            if(err){
                return done(err)
            }

            // try to send
            var endpoint = 'ping/:request/holla'
            iface.send(endpoint, letter, function(err, result){
                expect(err).to.not.be.undefined

                // check the info
                var info = iface.getNetworkInformation()
                expect(info.isOnline).to.equal(false)

                done()
            })
        })
    })

    after(function(done){
        iface.dispose(done)
    })

})

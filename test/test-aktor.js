'use strict'
const CommonAktor = require('../lib/platform/core/aktor').CommonAktor
const SimpleLogger = require('../lib/platform/core/logger').SimpleLogger
const MqttInterface = require('../lib/platform/core/interface').MqttInterface
const Storage = require('../lib/platform/core/storage')
const ParseStorage = Storage.ParseStorage
const StorageBase = Storage.StorageBase
const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const _ = require('lodash')

class SampleAktor extends CommonAktor {
    constructor(conf){
        _.set( conf, 'interfaces',  {
            local: {
                script: MqttInterface.path,
                listen: {
                    'action/bonjour': 'onBonjour'
                }
            }
        })
        super(conf)
    }

    onBonjour(message){
        console.log('Bonjour...')
        this.replySuccess(message)
    }

    onStop(message){
        console.log('Stopping...')
        this.replySuccess(message)
    }
}

class AnotherAktor extends CommonAktor {
    constructor(conf){
        super(conf)
    }

    _init(callback){
        callback && callback()
    }
}

describe('Tests for CommonAktor', function(){
    var conf = {
        id: 'ping',
        token: '123456'
    }
    var aktor = new SampleAktor(conf)

    before(function(done){
        // initialize first
        aktor.start(done)
    })

    it('should allow users to add loggers', function(done){
        var loggerAddedHandler = sinon.spy()
        aktor.onLoggerAdded( loggerAddedHandler )

        var config = {
            script: SimpleLogger.path,
            name: 'new' // can be omitted
        }
        aktor.addLogger( config, function(err, result){
            sinon.assert.calledOnce(loggerAddedHandler)
            // CommonAktors by default have one logger pre-installed
            expect(aktor._loggers.length).to.equal(2)
            done()
        })
    })

    var handler = sinon.spy()
    it('should allow users to listen for messages', function(done){
        aktor.listen('action/non_responsive', handler, function(err){
            should.not.exist(err)
            done()
        })
    })

    it('should allow users to ask', function(done){
        aktor.ask('action/bonjour', {'message': 'hi'}, function(err, result){
            should.not.exist(err)
            done()
        })
    })

    it('should allow users to ask with option timeout', function(done){
        aktor.ask('action/non_responsive', {'message': 'hi'}, {timeout: 1000}, function(err, result){
            should.exist(err)
            sinon.assert.calledOnce( handler )
            done()
        })
    })

    it('should allow users to add storages', function(done){
        var storage = {
            script: ParseStorage.path,
            name: 'local', // should have a name
            username: 'bot',
            password: '123456',
            loggers: [
                {
                    script: SimpleLogger.path,
                    name: 'storage@local'
                }
            ]
        }
        aktor.addStorage(storage, function(err, result){
            should.not.exist(err)
            done()
        })
    })

    it('should allow users to store data into the added storage', function(done){
        var storage = aktor.getStorage('local')
        should.exist(storage)
        expect(storage).to.be.an.instanceof(StorageBase)
        storage.set('platform', 'windows', function(err, result){
            should.not.exist(err)

            storage.get('platform', function(err, result){
                should.not.exist(err)
                expect(result).to.equal('windows')

                storage.unset('platform', function(err){
                    should.not.exist(err)
                    done()
                })

            })

        })
    })

    it('should allow users to add interfaces', function(done){
        var iface = {
            script: MqttInterface.path,
            name: 'remote', // should have a name
            username: 'pong',
            password: '123456'
        }
        aktor.addInterface(iface, function(err, result){
            should.not.exist(err)
            done()
        })
    })

    it('should allow users to construct an Aktor with interface `local` predefined', function(done){
        var another = new AnotherAktor({
            id: 'bot',
            token: '123456'
        })
        another.start(function(err){
            should.not.exist(err)

            // Stop this aktor via 2 methods

            // Method #1: directly invoke function `stop()`
            // another.stop(function(error){
            //     should.not.exist(error)
            //     done()
            // })

            // Method #2: typed message
            var message = {
                type: 'action/stop'
            }
            aktor.tell( another.getId(), message )
            done()
        })
    })

    after(function(done){
        var end = (err) => {
            setTimeout( function(){
                done(err) // wait for other aktors to terminate fully
            }, 5000)
        }
        aktor.stop(end)
    })

})

// called with --topic <topic to clean retained message>
const mqtt    = require('mqtt');
const argv = require('optimist').argv
const _ = require('lodash')

if(_.isNil(argv.topic)){
  throw new Error('Invalid topic')
}

console.log('Cleaning topic: ' + argv.topic + ' ...')

var options = {
  username: 'ping',
  password: '123456',
  host: '127.0.0.1',
  port: 1883,
}

var client  = mqtt.connect(options);
client.on('connect', function () {
  console.log('Connected')
  client.publish(argv.topic, null, {retain: true}, function(err){
    if(err){
      throw new Error('Error in publishing: ', err)
    }
    console.log('Done')
    client.end()
  })
});

client.on('error', function(e){
  console.log('Error: ', e)
})

client.on('offline', function () {
  console.log('Offline')
});

InfluxDb
====

### 1. Install Go

Follow the [link](http://dave.cheney.net/2015/09/04/building-go-1-5-on-the-raspberry-pi) to build Go

When run as (for example)
```sh
$ GOOS=linux GOARCH=arm GOARM=7 ./bootstrap.bash
```

bootstrap.bash cross-compiles a toolchain for that GOOS/GOARCH combination, leaving the resulting tree in ../../go-${GOOS}-${GOARCH}-bootstrap. That tree can be copied to a machine of the given target type and used as GOROOT_BOOTSTRAP to bootstrap a local build.

```sh
$ GOROOT_BOOTSTRAP=<your bootstrap dir> ./make.bash
```

### 2. Install InfluxDb

Follow the [link](https://github.com/influxdata/influxdb/blob/master/CONTRIBUTING.md) to compile InfluxDb

On Ubuntu you may have to install several packages:

```sh
sudo apt-get install ruby-dev gcc make
sudo gem install fpm
sudo apt-get install rpm
# You may need to patch function go_get_version()
GOPATH=/home/anhld/.go python build.py --package
```

### 3. Configuration
We should:

- Disable reporting
- Remove the admin interface (Web)
- Enable authentication by setting `auth-enabled` option to `true` in the `[http]` section of the configuration file:

via configure following fields:

```sh
# The configuration: /etc/influxdb/influxdb.conf
reporting-disabled = false

# Remove admin interface by commenting out the below lines
#[admin]
#  enabled = true
#  bind-address = ":8083"
#  https-enabled = false
#  https-certificate = "/etc/ssl/influxdb.pem"


[http]  
enabled = true  
bind-address = ":8086"  
auth-enabled = true # ✨
log-enabled = true  
write-tracing = false  
pprof-enabled = false  
https-enabled = false  
https-certificate = "/etc/ssl/influxdb.pem"  
```

Fluentd
====

### 1. Installation

For Debian Jessie, we use:

**Install gcc 4.9.2:**
https://community.thinger.io/t/starting-with-the-raspberry-pi/36

**Install Ruby 2.2.5 from source:**
https://www.ruby-lang.org/en/documentation/installation/#building-from-source

```sh
$ git clone https://github.com/fluent/fluentd.git
$ cd fluentd
$ git checkout -b v0.12 origin/v0.12


$ sudo gem install bundle
$ sudo gem install cool.io -v '1.4.4'
$ bundle install
$ bundle exec rake build
# fluentd xxx built to pkg/fluentd-xxx.gem.
$ gem install pkg/fluentd-xxx.gem
# install plugin mongo
$ fluent-gem install fluent-plugin-mongo
$ sudo gem install bson_ext
# make an auto run file registered with `sudo crontab -u pi -e`
$ cat /home/pi/platform/fluentd/run.sh
/usr/local/bin/fluentd -c /home/pi/platform/fluentd/fluentd.conf &
```


### 2. Configuration
Two things to mention:

- Configure use plugins `in_forward` and `out_mongo`:
- Listen on local interfaces only

Here is the configuration used by our platform:

```text
<source>
  @type forward
  port 24224
  bind 127.0.0.1
</source>

<match mqtt.**>
	@type copy

	<store>
	  @type stdout
	</store>

	# Single MongoDB
	<store>
	  @type mongo
	  host localhost
	  port 27017
	  database mqtt
	  collection label

	  # Set 'tag_mapped' if you want to use tag mapped mode.
	  tag_mapped
	  remove_tag_prefix mqtt.

	  # for capped collection
	  capped
	  capped_size 256m

	  # key name of timestamp
	  time_key timestamp

	  # flush
	  flush_interval 10s
	</store>
</match>
```

Mongodb
======


### 1. Installation

To install `mongodb`

```sh
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get install mongodb-server
$ sudo mkdir /data
$ sudo mkdir /data/db
$ sudo service mongodb start
```

### 2. Database & collections

There are 3 collections:

**mqtt_entity**
For storing key-value data of user (required by eMQTT)

```js
{
    id: "user",
    password: "password hash",
    is_superuser: boolean (true, false),
    created: "datetime"
}
```

**mqtt_acl**:
For storing ACLs (required eMQTT)

```js
{
    id: "id",
    publish: ["topic1", "topic2", ],
    subscribe: ["subtop1", "subtop2", ],
    pubsub: ["topic/#", "topic1", ]
}
```

**mqtt_data**
Storing latest data received

```js
{
    id: "id",
    motion: 1,
    battery: 2
}
```

### 3. Configuration

```sh
$ sudo vi /etc/mongodb.conf
```

#### 3.1 Journaling

**Disable Journal (worse)**
```text
nojournal=true
journal=false
```

**Using small journal**
```text
smallfiles=true
```

### 3.2 Listen only on local interface

```text
$ sudo nano /etc/mongod.conf

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1

```

### 4. Configuration

```sh
$ sudo chown -R mongodb:mongodb /var/log/mongodb/
$ sudo chown -R mongodb:mongodb /var/lib/mongodb/
$ sudo chown -R mongodb:mongodb /data/db
$ sudo -H -u mongodb mongod --config /etc/mongod.conf --dbpath /data/db
```

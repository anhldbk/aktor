World
======================

### 1. Overview

- id: `service/world`

- Has containers to start/stop aktors securely. `Securely` means:
	+ if there's an uncaught exception, restart the affected container (also hosted aktors)
	+ can identify bad aktors (causing to much exceptions)
	+ exclude such aktors from starting

- Containers are implemented as independent processes, can host aktors written in any programming languages

- World starts by:
	+ starting itself
	+ activating containers
	+ starting aktor `system`

- World records every events specified by [descriptors](./descriptor.md)

### 2. Typed messages

#### 2.1. action/service/world/manifest

Let the world know what descriptors a thing can have.

*message*

```js
{
	header,
	type: 'action/service/world/manifest',
	params: {
		status: 'status.{online,offline}',
		descriptors: [ // array of descriptors a thing can register
			{
				meta, // describe id & class
				event, // describe events
				state, // describe state
				action // describe actions
			}
		]
	}
}
```

*response*

```js
{
	header,
	request,
	response: {
		status: 'status.{success, failure}'
	}
}
```

World will return `status.failure` if any of following conditions is not satisfied.

- `meta.class` must be unique
- `event.type` must be unique (for every events)
- `action.type` must be unique (for every actions)

#### 2.2. action/service/world/register

Register an instance described by a descriptor

*messsage*

```js
{
	header,
	type: 'action/service/world/register',
	params: {
		meta: {
			id,
			class
		},
		state: { // initial state if have

		}
	}
}
```

*response*

```js
{
	header,
	request,
	response: {
		status: 'status.{success, failure}'
	}
}
```

World will return `status.failure` if `meta.id` is NOT unique.

#### 2.3. action/service/world/unregister

Unregister an instance

*message*

```js
{
	header,
	type: 'action/service/world/unregister',
	params: {
		meta: {
			id
		}
	}
}
```

*response*

```js
{
	header,
	request,
	response: {
		status: 'status.{success, failure}'
	}
}
```

World will return `status.failure` if `meta.id` is NOT unique.

#### 2.4. action/service/world/query
Allows others to query descriptors about events, actions, instances (devices, services...)

#### 2.5. action/service/world/enum
#### 2.6. action/service/world/restart
#### 2.7. action/service/world/aktor_start

Start an aktor. It supports nodejs and non-nodejs aktor

**endpoint** `:request/aktor_start`

**message**:

```js
{
  header, // added by our custom broker
  params: {
    script, // required, absolute path to nodejs aktor scripts or other binaries
    maxRestarts ,  // optional, default 3, the maximum number of times in a row a script will be restarted      
    id, // required
    timeout, // time (in ms) to wait for aktors to start. Optional, defaul 10000 ms (10s)
    token, // token may be omitted. If none's provided, an random one will be used.
    //... other options      
  }

}
```

**response**

```js
{
  header,
  request,
  response: {
    status: 'status.{failure, success}'
  }
}
```

Returns `failure` if:
- Aktor can NOT start successfully (exceptions or timeouts)
- Any aktor with the same id running

If there's  an aktor running with the same id , returns `status.failure.already_running`


**note**
- Aktors invoking this request will be marked as parents of the newly started aktor (via field `_parent`)
- Aktors must conform specification Aktor System v2.0

#### 2.8. action/service/world/aktor_stop

Stop an aktor but not remove it from the world.

**endpoint** `action/service/world/aktor_stop`

**message**:

```js
{
  header, // added by our custom broker
  params: { // optional
    id, // id of the aktor to stop
  }
}
```

If no id is provided, one from the header will be used.

Environ will fulfill the request by:
- Ask the aktor gracefully by invoking `:request/stop` (with a `forward timeout`)
- Ask PM2 to `delete` the associated process (if have, for non-NodeJs aktor) after a specific timeout (2000 ms)

**response**

```js
{
  header,
  request,
  response: {
    status: 'status.{failure, success}'
  }
}
```

Only parent aktors, World, the aktor itself may terminate a specific aktor.

If invokers are not authorized, returns `status.failure.unauthorized`

If no such aktor's running, returns `status.failure.no_such_aktor`

#### 2.9. action/service/world/aktor_destroy

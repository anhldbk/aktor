Run
==================

### Parse service

To enable Aktor using ParseStorage:
```shell
$ mongod &
$ parse-server --appId aktor-parse-data --masterKey OUR-SECRET-KEY --databaseURI mongodb://localhost/aktor &
```

To use the dashboard:

```
parse-dashboard --appId aktor-parse-data --masterKey OUR-SECRET-KEY --serverURL "http://localhost:1337/parse" --appName Aktor
```

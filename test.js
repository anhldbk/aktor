'use strict'
const CommonAktor = require('./lib/platform/core/aktor').CommonAktor
const SimpleLogger = require('./lib/platform/core/logger').SimpleLogger
const MqttInterface = require('./lib/platform/core/interface').MqttInterface
const Storage = require('./lib/platform/core/storage')
const ParseStorage = Storage.ParseStorage
const StorageBase = Storage.StorageBase
const _ = require('lodash')

class SampleAktor extends CommonAktor {
    constructor(conf){
        _.set( conf, 'interfaces',  {
            local: {
                script: MqttInterface.path,
                listen: {
                    'action/sample/bonjour': 'onBonjour'
                }
            }
        })
        super(conf)
    }

    onBonjour(message){
        console.log('Bonjour...')
        this.replySuccess(message)
    }

}

function run2(){
    world.ask('action/ping/bonjour', {}, (err, result) => {
        console.log('*****************************');
        console.log(err);
        console.log(result);
        console.log('*****************************>');

    })
}

var world = new CommonAktor({
    id: 'service/world',
    token: '123456'
})

function run(){
    var conf = {
        id: 'ping',
        token: '123456'
    }

    var aktor = new SampleAktor(conf)
    aktor.start((err) => {
        console.log('Started with err = ', err)

        var message = {
            type: 'action/stop'
        }
        world.ask( aktor.getId(), message, (err, result) => {
            console.log('*****************************');
            console.log(err);
            console.log(result);
            console.log('*****************************>');
        })
    })

}

world.start((err) => {
    world.listen('event/service/world/manifest/#', (message) => {
        console.log('manifest: ', message);
    })
    run()
})
